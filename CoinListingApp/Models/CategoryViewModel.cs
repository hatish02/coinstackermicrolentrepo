﻿using System.Collections.Generic;

namespace CoinListingApp.Models
{
    public class CategoryViewModel
    {
        public Category Category { get; set; }
        public List<Category> SubCategories { get; set; }
    }
}
