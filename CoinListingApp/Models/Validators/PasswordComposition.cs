﻿using System.ComponentModel.DataAnnotations;
using CoinListingApp.Services.Validation;

namespace CoinListingApp.Models.Validators
{
    public class PasswordComposition : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            this.ErrorMessage = PasswordValidationService.GetErrorMessage(value);

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
    }
}