﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoinListingApp.Models.Validators
{
    public class MustBeTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return Convert.ToBoolean(value);
        }
    }
}