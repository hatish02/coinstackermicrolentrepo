﻿using System.ComponentModel.DataAnnotations;

namespace CoinListingApp.Models
{
    public class ReportSellerViewModel
    {
        [StringLength(300)]
        public string Report { get; set; }

        public int SellerId { get; set; }

        public string SellerMaskedName { get; set; }
    }
}
