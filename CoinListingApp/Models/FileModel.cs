﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoinListingApp.Models
{
    public class FileModel
    {
        [Required(ErrorMessage = "Please select files.")]
        [FileExtensions(Extensions = "JPEG|PNG|JPG|PDF|jpeg|jpg|png|pdf")]
        [Display(Name = "Browse Files")]
        public HttpPostedFileBase[] files { get; set; }
    }
}