﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinListingApp.Models
{
    public class UserManagementViewModel
    {
        public User User { get; set; }
        public String UserRole { get; set; }
        public Credit Credits { get; set; }
    }

    public class HomeViewModel
    {
        public String MessageOfTheDay { get; set; }
        public List<Category> Categories { get; set; }
    }

    public class IndividualCredits
    {
        public double CreditAmount { get; set; }

        public List<CreditHistory> CreditHistory { get; set; }

        public bool IsSubscribed { get; set; }
        public string SubName { get; set; }
        public DateTime SubExpiry { get; set; }
    }
}