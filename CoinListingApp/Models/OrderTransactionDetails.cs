﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinListingApp.Models
{
    public class OrderTransactionDetails
    {
        public DateTime OrderDate { get; set; }
        public int OrderNumber { get; set; }
        public List<CustomItem> Item { get; set; }
        public string BuyerName { get; set; }
        public double TotalPrice { get; set; }
        public string Status { get; set; }
    }

    public class CustomItem
    {
        public string SellerName { get; set; }
        public string ItemName { get; set; }
        public Double Price { get; set; }
        public Double PostagePrice { get; set; }
        public Double? ExtraCharge { get; set; }
    }

    public class CustomListingModel
    {
        public int ListingID { get; set; }
        public int ListingViewCount { get; set; }
    }


}