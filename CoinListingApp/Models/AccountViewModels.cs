﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using CoinListingApp.Models.Validators;

namespace CoinListingApp.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public partial class PostageListing
    {
        public bool Selected { get; set; }
    }

    public class RegisterViewModel
    {
        [MustBeTrue(ErrorMessage = "You must accept terms & conditions to register.")]
        public bool CheckTerms { get; set; }

        //[MustBeTrue(ErrorMessage = "You must accept terms & conditions to register.")]
        public bool CheckNewsletter { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [PasswordComposition]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Code { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [PasswordComposition]
        public string Password { get; set; }
    }

    public class SendCodeViewModel
    {
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
        public string SelectedProvider { get; set; }
    }

    public class UpdateProfileViewModel
    {
        public string[] Countries { get; set; }

        [Required]
        [Display(Name = "Delivery Address Line 1")]
        public string DeliveryAddressLine1 { get; set; }

        [Display(Name = "Delivery Address Line 2")]
        public string DeliveryAddressLine2 { get; set; }

        [Required]
        [Display(Name = "City")]
        public string DeliveryCity { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string DeliveryCountry { get; set; }

        public int DeliveryId { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string DeliveryPostalCode { get; set; }

        [Required]
        [Display(Name = "State or Province or Region")]
        public string DeliveryStateProvince { get; set; }

        [Required]
        [Display(Name = "Home Address Line 1")]
        public string HomeAddressLine1 { get; set; }

        [Display(Name = "Home Address Line 2")]
        public string HomeAddressLine2 { get; set; }

        [Required]
        [Display(Name = "City")]
        public string HomeCity { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string HomeCountry { get; set; }

        public int HomeId { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string HomePostalCode { get; set; }

        [Required]
        [Display(Name = "State or Province or Region")]
        public string HomeStateProvince { get; set; }

        public string MaskedName { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Phone Number")]
        public string Phone { get; set; }



        [Display(Name = "Email address")]
        public string Email { get; set; }
        public bool IsReceiveNewsletter { get; set; }

        [Required]
        [Display(Name = "CoinStackerName")]
        public string CoinStackerName { get; set; }



        [Display(Name = "Profile Picture")]
        public HttpPostedFileBase ProfilePicture { get; set; }

        [Display(Name = "Delivery Address is the same")]
        public bool SameAddresses { get; set; }

        [Display(Name = "Same as Home address")]
        public bool SameAsHomeAddress { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        public string Provider { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}