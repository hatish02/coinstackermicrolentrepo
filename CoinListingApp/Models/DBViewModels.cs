﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace CoinListingApp.Models
{
    // ==============================================================================

    public class AllOrdersViewModel
    {
        public int CurrentUserId { get; set; }
        public List<OrderViewModel> OrdersAsBuyer { get; set; }
        public List<OrderViewModel> OrdersAsSeller { get; set; }
    }

    public class CartOrderViewModel
    {
        public List<Order> Orders { get; set; }

        public string RatingReason { get; set; }
        public double RatingValue { get; set; }
        public int UserID { get; set; }
    }

    public class ChatViewModel
    {
        public List<ConversationReply> Chats { get; set; }

        public Conversation Conversation { get; set; }

        public string CurrentUserFullName { get; set; }
        public int CurrentUserId { get; set; }

        public int OtherUserId { get; set; }

        public string OtherUserMaskedName { get; set; }
    }

    public class CheckoutListing
    {
        public Listing Listing { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public SellerViewModel SellerViewModel { get; set; }
    }

    public class CheckoutPage
    {
        public Address Address { get; set; }
        public int Amount { get; set; }
        public Listing Listing { get; set; }

        //public List<string> PaymentMethods { get; set; }
        public string PaymentMethod { get; set; }
    }

    // ==============================================================================
    public class CheckoutPageV2
    {
        public Address Address { get; set; }
        public List<CheckoutListing> CheckoutListings { get; set; }

        //public List<string> PaymentMethods { get; set; }
        public string PaymentMethod { get; set; }

        public List<PostageListing> PostageListings { get; set; }

        public PostageListing SelectedPostageListing { get; set; }
    }

    public class CreateListing
    {
        [Required]
        [Display(Name = "Allow Offers")]
        public bool AllowOffers { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int CategoryID { get; set; }

        public int CoverImageIndex { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(800)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Expires After")]
        public int Expires { get; set; }

        [Display(Name = "Allow EFT / Paypal Friends & Family payments for this listing")]
        public string paymentEFT { get; set; }

        //public Payment Payment { get; set; }
        public List<Payment> PaymentList { get; set; }

        [Display(Name = "Allow Paypal Goods & Services payments for this listing")]
        public string paymentPaypal { get; set; }

        [Display(Name = "Payment Option")]
        public byte PaymentSelectedId { get; set; }

        [Display(Name = "Browse Photos")]
        public HttpPostedFileBase[] Photos { get; set; }

        public List<PostageListingCreate> PostageList { get; set; }

        [Display(Name = "Free Postage")]
        public bool PostagePriceIsIncluded { get; set; }

        [Required]
        [Range(0.01f, float.MaxValue, ErrorMessage = "Price must be higher than zero")]
        [Display(Name = "Price")]
        public float Price { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a quantity bigger than 1")]
        [Display(Name = "Quantity Available")]
        public int QTY { get; set; }

        [Required]
        [Display(Name = "Subcategory")]
        public int SubcategoryID { get; set; }

        [Required]
        [Display(Name = "Title")]
        [StringLength(40)]
        public string Title { get; set; } 
        public bool preOrderItem { get; set; } 
        public bool shipInTwotoFourWeek { get; set; }   
        public bool shipInFourtoEightWeek { get; set; }
    }

    public class CreditPackageViewModel
    {
        public string Description { get; set; }
        public double Discount { get; set; }
        public string Name { get; set; }
        public int PackageID { get; set; }
        public double Price { get; set; }
        public double QTY { get; set; }
    }

    public class DetailsListing
    {
        [Required]
        [Display(Name = "Amount to buy")]
        public int Amount { get; set; }

        [Display(Name = "Seller's rating")]
        public double AverageRating { get; set; }

        public bool CanRate { get; set; }
        public bool Expired { get; set; }
        public bool Favourite { get; set; }
        public Listing Listing { get; set; }
        public int NoViews { get; set; }
        public bool OfferMade { get; set; }
        public string PaymentMethod { get; set; }
        public List<string> PaymentMethods { get; set; }
        public List<string> Photos { get; set; }
        public bool PostagePriceIsIncluded { get; set; }
        public bool Rated { get; set; }

        [Display(Name = "Seller rating")]
        public double Rating { get; set; }

        [Display(Name = "Report seller")]
        public string Report { get; set; } 
        public bool Reported { get; set; }
        public string SellerNameMasked { get; set; }
        public int UserID { get; set; }
        public bool shipInTwotoFourWeek { get; set; }
        public bool shipInFourtoEightWeek { get; set; }
        public WatchlistButtonViewModel WatchlistButtonViewModel { get; set; }
    }

    public class EditListing
    {
        public string Description { get; set; }

        public Listing Listing { get; set; }

        public int ListingID { get; set; }
        public List<PostageListingCreate> PostageList { get; set; }

        [Display(Name = "Free Postage")]
        public bool PostagePriceIsIncluded { get; set; }

        public double Price { get; set; }

        [Display(Name = "Quantity")]
        public int QTY { get; set; }

        public string Title { get; set; }

        public bool preOrderItem { get; set; }
        public bool shipInTwotoFourWeek { get; set; }
        public bool shipInFourtoEightWeek { get; set; }
    }

    public class FavouriteViewModel
    {
        public int FavouriteID { get; set; }

        public string MaskedSellerName { get; set; }

        public bool Notify { get; set; }

        public User Seller { get; set; }
    }

    public class FullListing
    {
        public Nullable<int> ActiveDays { get; set; }
        public byte AllowOffers { get; set; }
        public double AverageRating { get; set; }
        public Nullable<System.DateTime> BumpExpires { get; set; }
        public virtual Category Category { get; set; }
        public int CategoryID { get; set; }
        public string CoverImage { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.DateTime> DeactivatedDate { get; set; }
        public string Description { get; set; }
        public System.DateTime Expires { get; set; }
        public byte isActive { get; set; }
        public byte isBump { get; set; }
        public byte isHighlight { get; set; }
        public int ListingID { get; set; }

        public bool shipInTwotoFourWeek { get; set; }
        public bool shipInFourtoEightWeek { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ListingView> ListingViews { get; set; }
            
        public int NoViews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Offer> Offers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        public string Photos { get; set; }
        public virtual PostageListing PostageListing { get; set; }
        public double Price { get; set; }
        public double QTY { get; set; }
        public int SellerD { get; set; }
        public string SellerNameMasked { get; set; }
        public string Title { get; set; }
        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Watching> Watchings { get; set; }

        public WatchlistButtonViewModel WatchlistButtonViewModel { get; internal set; }
    }

    public class IndividualConversation
    {
        public int ConversationID { get; set; }

        public DateTime? LastMessageTimeStamp { get; set; }
        public string SellerMaskedName { get; set; }

        public int SellerUserId { get; set; }
    }

    public class ListingBuyNow
    {
        public int ListingID { get; set; }

        public double ListingPrice { get; set; }
        public double ListingQTY { get; set; }
        public int SellerID { get; set; }
    }

    public class ListingBuyNowDetail
    {
        public Listing Listing { get; set; }
        public double Price { get; set; }
        public double Quantity { get; set; }
        public double TotalPrice => this.Price * this.Quantity;
    }

    public class ListingBuyNowV2
    {
        public Address Address { get; set; }
        public List<ListingBuyNowDetail> Listings { get; set; }

        //public List<string> PaymentMethods { get; set; }
        public string PaymentMethod { get; set; }

        public PostageListing PostageListing { get; set; }
        public int SellerID { get; set; }

        public double ShippingPrice { get; set; }
        public double TotalListingsPrice { get; set; }
        public double TotalPrice => TotalListingsPrice + ShippingPrice;
        public double TotalQTY { get; set; }
    }

    public class ListingsPhotoViewModel
    {
        public Listing Listing { get; set; }
        public List<string> Photos { get; set; }
    }

    public class ListingsViewsViewModel
    {
        public Listing Listing { get; set; }

        [Display(Name = "Views")]
        public int Views { get; set; }
    }

    public class ListingViewModel
    {
        public virtual Category Category { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int ListingID { get; set; }
        public double Price { get; set; }
        public double Rating { get; set; }
        public int SellerD { get; set; }
        public string Title { get; set; }
        public int Views { get; set; }
        public WatchlistButtonViewModel WatchlistButtonViewModel { get; set; }
    }

    public class ManageListing
    {
        [Required]
        [Display(Name = "Activate listing")]
        public byte Active { get; set; }

        [Required]
        [Display(Name = "Bump listing")]
        public byte isBump { get; set; }

        [Required]
        [Display(Name = "Highlight listing")]
        public byte isHighlight { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ListingID { get; set; }

        [Required]
        [Display(Name = "Renew listing")]
        public int Renew { get; set; }
    }

    public class MessagesIndexViewModel
    {
        public List<IndividualConversation> CurrentUserConversations { get; set; }

        public List<SellerViewModel> Sellers { get; set; }
    }

    public class OrdersHistoryViewModel
    {
        public bool AsBuyer { get; set; }
        public List<OrderViewModel> Orders { get; set; }
    }

    public class OrderViewModel
    {
        public string BuyerFullName { get; set; }
        public int BuyerID { get; set; }
        public Address BuyerShippingAddress { get; set; }
        public DateTime? CreatedDate { get; set; }
        public List<CheckoutListing> Listings { get; set; }
        public string MaskedBuyerName { get; set; }
        public int OrderID { get; set; }
        public string PaymentType { get; set; }
        public string Postage { get; set; }
        public double Price { get; set; }
        public int QTY { get; set; }
        public SellerViewModel Seller { get; set; }
        public Status Status { get; set; }
        public Tracking Tracking { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class Packages
    {
        public List<BillingPlan> BillingPlans { get; set; }
        public List<CreditPackage> CreditPackages { get; set; }
    }

    public class Payments
    {
        [Display(Name = "Notify Automatically")]
        public byte AutomaticNotifications { get; set; }

        [Display(Name = "Message")]
        public string InstructionsMessage { get; set; }

        public int OrderID { get; set; }

        //[Display(Name = "Paypal G&s")]
        //public int OptionTwoSelected { get; set; }
        //[Display(Name = "Apply this payment method to new listings")]
        //public byte OptionOneApplyToAll { get; set; }
        [Display(Name = "Paypal Email")]
        [EmailAddress]
        public string PaypalEmail { get; set; }

        public double TotalPrice { get; set; }

        [Required]
        [Display(Name = "UserID")]
        public int UserID { get; set; }

        //[Display(Name = "EFT & Paypal Friends and Family")]
        //public int OptionOneSelected { get; set; }
        //[Display(Name = "Apply this payment method to new listings")]
        //public byte OptionTwoApplyToAll { get; set; }
    }

    // ==============================================================================
    public class PostageListingCreate : PostageListing
    {
        public bool Selected { get; set; }
    }

    public class SellerVerifyViewModel
    {
        [Required]
        [Display(Name = "ID or Password")]
        public HttpPostedFileBase ID { get; set; }

        [Required]
        [Display(Name = "Proof of Residence")]
        public HttpPostedFileBase PoR { get; set; }
    }

    public class SellerViewModel
    {
        [Display(Name = "Seller's rating")]
        public double AverageRating { get; set; }

        [Display(Name = "Location")]
        public string CityAndCountry { get; set; }

        public int CurrentUserId { get; set; }

        [Display(Name = "Seller's rating")]
        public double CurrentUserRating { get; set; }

        public bool Favourite { get; set; }

        [Display(Name = "Member since")]
        public string FormattedSignUpDate { get; set; }

        [Display(Name = "Name")]
        public string MaskedSellerName { get; set; }

        public IPagedList<FullListing> PagedFullListings { get; set; }
        public string Report { get; set; }

        public User Seller { get; set; }
        public int SellerId { get; set; }
    }

    public class SendMessageViewModel
    {
        public int? ConversationId { get; set; }

        public string Message { get; set; }

        public int ReceiverUserId { get; set; }
    }

    public class SystemNotificationViewModel
    {
        [AllowHtml]
        public string Content { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }
    }

    public class UpdateListing
    {
        public int CoverImageIndex { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(800)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Extend expiration with")]
        public int Expires { get; set; }

        [Required]
        [Display(Name = "ListingID")]
        public int ListingID { get; set; }

        public HttpPostedFileBase[] Photos { get; set; }

        public List<PostageListingCreate> PostageList { get; set; }

        public bool PostagePriceIsIncluded { get; set; }

        [Required]
        [Range(0.01f, float.MaxValue, ErrorMessage = "Price must be higher than zero")]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a quantity bigger than 1")]
        [Display(Name = "Quantity")]
        public int QTY { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        public bool shipInTwotoFourWeek { get; set; }
        public bool shipInFourtoEightWeek { get; set; }
    }
}