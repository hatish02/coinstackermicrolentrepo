﻿using System.Collections.Generic;

namespace CoinListingApp.Models
{
    public class OffersViewModel
    {
        public List<OfferViewModel> ClosedOffersAsBuyer { get; set; }
        public List<OfferViewModel> ClosedOffersAsSeller { get; set; }
        public int MaxBuyerPossibleActions { get; set; }
        public int MaxCounterOffers { get; set; }
        public int MaxSellerPossibleActions { get; set; }
        public List<OfferViewModel> OffersAsBuyer { get; set; }
        public List<OfferViewModel> OffersAsSeller { get; set; }
        public int UserId { get; set; }
    }

    public class OfferViewModel
    {
        public int Amount { get; set; }
        public bool BuyerActionPending { get; set; }
        public int BuyerID { get; set; }
        public string BuyerMaskedName { get; set; }
        public int Counters { get; set; }
        public Listing Listing { get; set; }
        public int OfferID { get; set; }
        public double Price { get; set; }
        public bool SellerActionPending { get; set; }
        public string SellerMaskedName { get; set; }
        public Status Status { get; set; }
    }
}
