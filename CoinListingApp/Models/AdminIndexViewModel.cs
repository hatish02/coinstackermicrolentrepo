﻿namespace CoinListingApp.Models
{
    public class AdminIndexViewModel
    {
        public int AuthenticatedUsers { get; set; }
        public int LiveListings { get; set; }
        public int OnlineUsers { get; set; }
        public int RegisertedUsers { get; set; }

    }
}