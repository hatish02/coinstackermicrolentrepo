﻿using System.Collections.Generic;

namespace CoinListingApp.Models
{
    public class MostPopularViewModel
    {
        public List<Listing> Listings { get; set; }
        public HashSet<int> UserWatchingListingsIds { get; internal set; }
    }
}
