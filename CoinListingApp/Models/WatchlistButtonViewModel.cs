﻿namespace CoinListingApp.Models
{
    public class WatchlistButtonViewModel
    {
        public int ListingId { get; set; }

        public bool Watching { get; set; }
    }
}
