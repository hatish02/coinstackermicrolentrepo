﻿using System.Collections.Generic;

namespace CoinListingApp.Utils
{
    public class CountryCodes
    {
        public Dictionary<string,string> countryCodes = new Dictionary<string, string>();

        public string[] countries = new string[] {
            "ALBANIA",
            "ALGERIA",
            "ANDORRA",
            "ANGOLA",
            "ANGUILLA",
            "ANTIGUA & BARBUDA",
            "ARGENTINA",
            "ARMENIA",
            "ARUBA",
            "AUSTRALIA",
            "AUSTRIA",
            "AZERBAIJAN",
            "BAHAMAS",
            "BAHRAIN",
            "BARBADOS",
            "BELARUS",
            "BELGIUM",
            "BELIZE",
            "BENIN",
            "BERMUDA",
            "BHUTAN",
            "BOLIVIA",
            "BOSNIA & HERZEGOVINA",
            "BOTSWANA",
            "BRAZIL",
            "BRITISH VIRGIN ISLANDS",
            "BRUNEI",
            "BULGARIA",
            "BURKINA FASO",
            "BURUNDI",
            "CAMBODIA",
            "CAMEROON",
            "CANADA",
            "CAPE VERDE",
            "CAYMAN ISLANDS",
            "CHAD",
            "CHILE",
            "CHINA",
            "COLOMBIA",
            "COMOROS",
            "CONGO - BRAZZAVILLE",
            "CONGO - KINSHASA",
            "COOK ISLANDS",
            "COSTA RICA",
            "CÔTE D’IVOIRE",
            "CROATIA",
            "CYPRUS",
            "CZECH REPUBLIC",
            "DENMARK",
            "DJIBOUTI",
            "DOMINICA",
            "DOMINICAN REPUBLIC",
            "ECUADOR",
            "EGYPT",
            "EL SALVADOR",
            "ERITREA",
            "ESTONIA",
            "ETHIOPIA",
            "FALKLAND ISLANDS",
            "FAROE ISLANDS",
            "FIJI",
            "FINLAND",
            "FRANCE",
            "FRENCH GUIANA",
            "FRENCH POLYNESIA",
            "GABON",
            "GAMBIA",
            "GEORGIA",
            "GERMANY",
            "GIBRALTAR",
            "GREECE",
            "GREENLAND",
            "GRENADA",
            "GUADELOUPE",
            "GUATEMALA",
            "GUINEA",
            "GUINEA-BISSAU",
            "GUYANA",
            "HONDURAS",
            "HONG KONG SAR CHINA",
            "HUNGARY",
            "ICELAND",
            "INDIA",
            "INDONESIA",
            "IRELAND",
            "ISRAEL",
            "ITALY",
            "JAMAICA",
            "JAPAN",
            "JORDAN",
            "KAZAKHSTAN",
            "KENYA",
            "KIRIBATI",
            "KUWAIT",
            "KYRGYZSTAN",
            "LAOS",
            "LATVIA",
            "LESOTHO",
            "LIECHTENSTEIN",
            "LITHUANIA",
            "LUXEMBOURG",
            "MACEDONIA",
            "MADAGASCAR",
            "MALAWI",
            "MALAYSIA",
            "MALDIVES",
            "MALI",
            "MALTA",
            "MARSHALL ISLANDS",
            "MARTINIQUE",
            "MAURITANIA",
            "MAURITIUS",
            "MAYOTTE",
            "MEXICO",
            "MICRONESIA",
            "MOLDOVA",
            "MONACO",
            "MONGOLIA",
            "MONTENEGRO",
            "MONTSERRAT",
            "MOROCCO",
            "MOZAMBIQUE",
            "NAMIBIA",
            "NAURU",
            "NEPAL",
            "NETHERLANDS",
            "NEW CALEDONIA",
            "NEW ZEALAND",
            "NICARAGUA",
            "NIGER",
            "NIGERIA",
            "NIUE",
            "NORFOLK ISLAND",
            "NORWAY",
            "OMAN",
            "PALAU",
            "PANAMA",
            "PAPUA NEW GUINEA",
            "PARAGUAY",
            "PERU",
            "PHILIPPINES",
            "PITCAIRN ISLANDS",
            "POLAND",
            "PORTUGAL",
            "QATAR",
            "RÉUNION",
            "ROMANIA",
            "RUSSIA",
            "RWANDA",
            "SAMOA",
            "SAN MARINO",
            "SÃO TOMÉ & PRÍNCIPE",
            "SAUDI ARABIA",
            "SENEGAL",
            "SERBIA",
            "SEYCHELLES",
            "SIERRA LEONE",
            "SINGAPORE",
            "SLOVAKIA",
            "SLOVENIA",
            "SOLOMON ISLANDS",
            "SOMALIA",
            "SOUTH AFRICA",
            "SOUTH KOREA",
            "SPAIN",
            "SRI LANKA",
            "ST. HELENA",
            "ST. KITTS & NEVIS",
            "ST. LUCIA",
            "ST. PIERRE & MIQUELON",
            "ST. VINCENT & GRENADINES",
            "SURINAME",
            "SVALBARD & JAN MAYEN",
            "SWAZILAND",
            "SWEDEN",
            "SWITZERLAND",
            "TAIWAN",
            "TAJIKISTAN",
            "TANZANIA",
            "THAILAND",
            "TOGO",
            "TONGA",
            "TRINIDAD & TOBAGO",
            "TUNISIA",
            "TURKMENISTAN",
            "TURKS & CAICOS ISLANDS",
            "TUVALU",
            "UGANDA",
            "UKRAINE",
            "UNITED ARAB EMIRATES",
            "UNITED KINGDOM",
            "UNITED STATES",
            "URUGUAY",
            "VANUATU",
            "VATICAN CITY",
            "VENEZUELA",
            "VIETNAM",
            "WALLIS & FUTUNA",
            "YEMEN",
            "ZAMBIA",
            "ZIMBABWE"
        };

        public CountryCodes()
        {
            countryCodes.Add("ALBANIA", "AL");
            countryCodes.Add("ALGERIA", "DZ");
            countryCodes.Add("ANDORRA", "AD");
            countryCodes.Add("ANGOLA", "AO");
            countryCodes.Add("ANGUILLA", "AI");
            countryCodes.Add("ANTIGUA & BARBUDA", "AG");
            countryCodes.Add("ARGENTINA", "AR");
            countryCodes.Add("ARMENIA", "AM");
            countryCodes.Add("ARUBA", "AW");
            countryCodes.Add("AUSTRALIA", "AU");
            countryCodes.Add("AUSTRIA", "AT");
            countryCodes.Add("AZERBAIJAN", "AZ");
            countryCodes.Add("BAHAMAS", "BS");
            countryCodes.Add("BAHRAIN", "BH");
            countryCodes.Add("BARBADOS", "BB");
            countryCodes.Add("BELARUS", "BY");
            countryCodes.Add("BELGIUM", "BE");
            countryCodes.Add("BELIZE", "BZ");
            countryCodes.Add("BENIN", "BJ");
            countryCodes.Add("BERMUDA", "BM");
            countryCodes.Add("BHUTAN", "BT");
            countryCodes.Add("BOLIVIA", "BO");
            countryCodes.Add("BOSNIA & HERZEGOVINA", "BA");
            countryCodes.Add("BOTSWANA", "BW");
            countryCodes.Add("BRAZIL", "BR");
            countryCodes.Add("BRITISH VIRGIN ISLANDS", "VG");
            countryCodes.Add("BRUNEI", "BN");
            countryCodes.Add("BULGARIA", "BG");
            countryCodes.Add("BURKINA FASO", "BF");
            countryCodes.Add("BURUNDI", "BI");
            countryCodes.Add("CAMBODIA", "KH");
            countryCodes.Add("CAMEROON", "CM");
            countryCodes.Add("CANADA", "CA");
            countryCodes.Add("CAPE VERDE", "CV");
            countryCodes.Add("CAYMAN ISLANDS", "KY");
            countryCodes.Add("CHAD", "TD");
            countryCodes.Add("CHILE", "CL");
            countryCodes.Add("CHINA", "C2");
            countryCodes.Add("COLOMBIA", "CO");
            countryCodes.Add("COMOROS", "KM");
            countryCodes.Add("CONGO - BRAZZAVILLE", "CG");
            countryCodes.Add("CONGO - KINSHASA", "CD");
            countryCodes.Add("COOK ISLANDS", "CK");
            countryCodes.Add("COSTA RICA", "CR");
            countryCodes.Add("CÔTE D’IVOIRE", "CI");
            countryCodes.Add("CROATIA", "HR");
            countryCodes.Add("CYPRUS", "CY");
            countryCodes.Add("CZECH REPUBLIC", "CZ");
            countryCodes.Add("DENMARK", "DK");
            countryCodes.Add("DJIBOUTI", "DJ");
            countryCodes.Add("DOMINICA", "DM");
            countryCodes.Add("DOMINICAN REPUBLIC", "DO");
            countryCodes.Add("ECUADOR", "EC");
            countryCodes.Add("EGYPT", "EG");
            countryCodes.Add("EL SALVADOR", "SV");
            countryCodes.Add("ERITREA", "ER");
            countryCodes.Add("ESTONIA", "EE");
            countryCodes.Add("ETHIOPIA", "ET");
            countryCodes.Add("FALKLAND ISLANDS", "FK");
            countryCodes.Add("FAROE ISLANDS", "FO");
            countryCodes.Add("FIJI", "FJ");
            countryCodes.Add("FINLAND", "FI");
            countryCodes.Add("FRANCE", "FR");
            countryCodes.Add("FRENCH GUIANA", "GF");
            countryCodes.Add("FRENCH POLYNESIA", "PF");
            countryCodes.Add("GABON", "GA");
            countryCodes.Add("GAMBIA", "GM");
            countryCodes.Add("GEORGIA", "GE");
            countryCodes.Add("GERMANY", "DE");
            countryCodes.Add("GIBRALTAR", "GI");
            countryCodes.Add("GREECE", "GR");
            countryCodes.Add("GREENLAND", "GL");
            countryCodes.Add("GRENADA", "GD");
            countryCodes.Add("GUADELOUPE", "GP");
            countryCodes.Add("GUATEMALA", "GT");
            countryCodes.Add("GUINEA", "GN");
            countryCodes.Add("GUINEA-BISSAU", "GW");
            countryCodes.Add("GUYANA", "GY");
            countryCodes.Add("HONDURAS", "HN");
            countryCodes.Add("HONG KONG SAR CHINA", "HK");
            countryCodes.Add("HUNGARY", "HU");
            countryCodes.Add("ICELAND", "IS");
            countryCodes.Add("INDIA", "IN");
            countryCodes.Add("INDONESIA", "ID");
            countryCodes.Add("IRELAND", "IE");
            countryCodes.Add("ISRAEL", "IL");
            countryCodes.Add("ITALY", "IT");
            countryCodes.Add("JAMAICA", "JM");
            countryCodes.Add("JAPAN", "JP");
            countryCodes.Add("JORDAN", "JO");
            countryCodes.Add("KAZAKHSTAN", "KZ");
            countryCodes.Add("KENYA", "KE");
            countryCodes.Add("KIRIBATI", "KI");
            countryCodes.Add("KUWAIT", "KW");
            countryCodes.Add("KYRGYZSTAN", "KG");
            countryCodes.Add("LAOS", "LA");
            countryCodes.Add("LATVIA", "LV");
            countryCodes.Add("LESOTHO", "LS");
            countryCodes.Add("LIECHTENSTEIN", "LI");
            countryCodes.Add("LITHUANIA", "LT");
            countryCodes.Add("LUXEMBOURG", "LU");
            countryCodes.Add("MACEDONIA", "MK");
            countryCodes.Add("MADAGASCAR", "MG");
            countryCodes.Add("MALAWI", "MW");
            countryCodes.Add("MALAYSIA", "MY");
            countryCodes.Add("MALDIVES", "MV");
            countryCodes.Add("MALI", "ML");
            countryCodes.Add("MALTA", "MT");
            countryCodes.Add("MARSHALL ISLANDS", "MH");
            countryCodes.Add("MARTINIQUE", "MQ");
            countryCodes.Add("MAURITANIA", "MR");
            countryCodes.Add("MAURITIUS", "MU");
            countryCodes.Add("MAYOTTE", "YT");
            countryCodes.Add("MEXICO", "MX");
            countryCodes.Add("MICRONESIA", "FM");
            countryCodes.Add("MOLDOVA", "MD");
            countryCodes.Add("MONACO", "MC");
            countryCodes.Add("MONGOLIA", "MN");
            countryCodes.Add("MONTENEGRO", "ME");
            countryCodes.Add("MONTSERRAT", "MS");
            countryCodes.Add("MOROCCO", "MA");
            countryCodes.Add("MOZAMBIQUE", "MZ");
            countryCodes.Add("NAMIBIA", "NA");
            countryCodes.Add("NAURU", "NR");
            countryCodes.Add("NEPAL", "NP");
            countryCodes.Add("NETHERLANDS", "NL");
            countryCodes.Add("NEW CALEDONIA", "NC");
            countryCodes.Add("NEW ZEALAND", "NZ");
            countryCodes.Add("NICARAGUA", "NI");
            countryCodes.Add("NIGER", "NE");
            countryCodes.Add("NIGERIA", "NG");
            countryCodes.Add("NIUE", "NU");
            countryCodes.Add("NORFOLK ISLAND", "NF");
            countryCodes.Add("NORWAY", "NO");
            countryCodes.Add("OMAN", "OM");
            countryCodes.Add("PALAU", "PW");
            countryCodes.Add("PANAMA", "PA");
            countryCodes.Add("PAPUA NEW GUINEA", "PG");
            countryCodes.Add("PARAGUAY", "PY");
            countryCodes.Add("PERU", "PE");
            countryCodes.Add("PHILIPPINES", "PH");
            countryCodes.Add("PITCAIRN ISLANDS", "PN");
            countryCodes.Add("POLAND", "PL");
            countryCodes.Add("PORTUGAL", "PT");
            countryCodes.Add("QATAR", "QA");
            countryCodes.Add("RÉUNION", "RE");
            countryCodes.Add("ROMANIA", "RO");
            countryCodes.Add("RUSSIA", "RU");
            countryCodes.Add("RWANDA", "RW");
            countryCodes.Add("SAMOA", "WS");
            countryCodes.Add("SAN MARINO", "SM");
            countryCodes.Add("SÃO TOMÉ & PRÍNCIPE", "ST");
            countryCodes.Add("SAUDI ARABIA", "SA");
            countryCodes.Add("SENEGAL", "SN");
            countryCodes.Add("SERBIA", "RS");
            countryCodes.Add("SEYCHELLES", "SC");
            countryCodes.Add("SIERRA LEONE", "SL");
            countryCodes.Add("SINGAPORE", "SG");
            countryCodes.Add("SLOVAKIA", "SK");
            countryCodes.Add("SLOVENIA", "SI");
            countryCodes.Add("SOLOMON ISLANDS", "SB");
            countryCodes.Add("SOMALIA", "SO");
            countryCodes.Add("SOUTH AFRICA", "ZA");
            countryCodes.Add("SOUTH KOREA", "KR");
            countryCodes.Add("SPAIN", "ES");
            countryCodes.Add("SRI LANKA", "LK");
            countryCodes.Add("ST. HELENA", "SH");
            countryCodes.Add("ST. KITTS & NEVIS", "KN");
            countryCodes.Add("ST. LUCIA", "LC");
            countryCodes.Add("ST. PIERRE & MIQUELON", "PM");
            countryCodes.Add("ST. VINCENT & GRENADINES", "VC");
            countryCodes.Add("SURINAME", "SR");
            countryCodes.Add("SVALBARD & JAN MAYEN", "SJ");
            countryCodes.Add("SWAZILAND", "SZ");
            countryCodes.Add("SWEDEN", "SE");
            countryCodes.Add("SWITZERLAND", "CH");
            countryCodes.Add("TAIWAN", "TW");
            countryCodes.Add("TAJIKISTAN", "TJ");
            countryCodes.Add("TANZANIA", "TZ");
            countryCodes.Add("THAILAND", "TH");
            countryCodes.Add("TOGO", "TG");
            countryCodes.Add("TONGA", "TO");
            countryCodes.Add("TRINIDAD & TOBAGO", "TT");
            countryCodes.Add("TUNISIA", "TN");
            countryCodes.Add("TURKMENISTAN", "TM");
            countryCodes.Add("TURKS & CAICOS ISLANDS", "TC");
            countryCodes.Add("TUVALU", "TV");
            countryCodes.Add("UGANDA", "UG");
            countryCodes.Add("UKRAINE", "UA");
            countryCodes.Add("UNITED ARAB EMIRATES", "AE");
            countryCodes.Add("UNITED KINGDOM", "GB");
            countryCodes.Add("UNITED STATES", "US");
            countryCodes.Add("URUGUAY", "UY");
            countryCodes.Add("VANUATU", "VU");
            countryCodes.Add("VATICAN CITY", "VA");
            countryCodes.Add("VENEZUELA", "VE");
            countryCodes.Add("VIETNAM", "VN");
            countryCodes.Add("WALLIS & FUTUNA", "WF");
            countryCodes.Add("YEMEN", "YE");
            countryCodes.Add("ZAMBIA", "ZM");
            countryCodes.Add("ZIMBABWE", "ZW");
        }

        public string GetCountryCode(string country)
        {
            return countryCodes[country];
        }
    }
}