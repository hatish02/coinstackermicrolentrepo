﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CoinListingApp.Models;
using CoinListingApp.Services;
using Quartz;
using Quartz.Impl;

namespace CoinListingApp.Utils
{
    public class Scheduler
    {
        public class DeleteInactiveUsers : IJob
        {
            private readonly Entities db = new Entities();
            private readonly UserService userService = new UserService();

            private List<User> users = new List<User>();

            async Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                // Find inactive users
                users = db.Users
                  .Where(u => DbFunctions.AddDays(u.LastLoggedIn.Value, 1825) < DbFunctions.TruncateTime(DateTime.Now))
                  .ToList();

                await Task.Run(() =>
                {
                    foreach (User user in users)
                    {
                        // Delete the user
                        userService.DeleteUser(user.UserID);
                    }

                    System.Diagnostics.Debug.WriteLine("Removed Users - Done");
                }
                );
            }
        }

        public class JobScheduler
        {
            public async void Start()
            {
                ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
                IScheduler scheduler = await schedulerFactory.GetScheduler();
                await scheduler.Start();

                IJobDetail job1 = JobBuilder.Create<UpdateListings>().Build();
                IJobDetail job2 = JobBuilder.Create<DeleteInactiveUsers>().Build();
                IJobDetail job3 = JobBuilder.Create<RemoveInactiveSubs>().Build();
                IJobDetail job4 = JobBuilder.Create<VerifySubs>().Build();
                IJobDetail job5 = JobBuilder.Create<NotifySubs>().Build();

                ITrigger trigger1 = TriggerBuilder.Create()
                .WithIdentity("UpdateListings", "UpdateGroup")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(300)
                .RepeatForever())
                .Build();

                ITrigger trigger2 = TriggerBuilder.Create()
                .WithIdentity("DeleteInactiveUsers", "UpdateGroup2")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                .RepeatForever())
                .Build();

                ITrigger trigger3 = TriggerBuilder.Create()
                .WithIdentity("RemoveInactiveSubs", "UpdateGroup3")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                .RepeatForever())
                .Build();

                ITrigger trigger4 = TriggerBuilder.Create()
                .WithIdentity("VerifySubs", "UpdateGroup4")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                .RepeatForever())
                .Build();

                ITrigger trigger5 = TriggerBuilder.Create()
                .WithIdentity("NotifySubs", "UpdateGroup5")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                .RepeatForever())
                .Build();

                await scheduler.ScheduleJob(job1, trigger1);
                await scheduler.ScheduleJob(job2, trigger2);
                await scheduler.ScheduleJob(job3, trigger3);
                await scheduler.ScheduleJob(job4, trigger4);
                await scheduler.ScheduleJob(job5, trigger5);
            }
        }

        public class NotifySubs : IJob
        {
            private readonly Entities db = new Entities();
            private readonly NotificationService notificationService = new NotificationService();
            private List<Subscription> subs = new List<Subscription>();

            async Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                // Remove expired active
                subs = db.Subscriptions
                  .Where(s => DbFunctions.DiffDays(s.DateExpires, DateTime.Now) == 10)
                  .ToList();

                await Task.Run(() =>
                {
                    foreach (Subscription sub in subs)
                    {
                        notificationService.SendSubscriptionEndingNotification(sub.User.Email, sub.User.Name, sub.DateExpires);
                    }

                    System.Diagnostics.Debug.WriteLine("Notify Subscribers - Done");
                }
                );
            }
        }

        public class RemoveInactiveSubs : IJob
        {
            private readonly Entities db = new Entities();
            private List<Subscription> subs = new List<Subscription>();

            async Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                // Remove expired active
                subs = db.Subscriptions
                  .Where(s => DbFunctions.TruncateTime(s.DateExpires) < DbFunctions.TruncateTime(DateTime.Now))
                  .ToList();

                await Task.Run(() =>
                {
                    foreach (Subscription sub in subs)
                    {
                        User user = db.Users.Find(sub.UserID);

                        user.isSubscribed = 0;

                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();

                        db.Subscriptions.Remove(sub);
                        db.SaveChanges();
                    }

                    System.Diagnostics.Debug.WriteLine("Removed Subscriptions - Done");
                }
                );
            }
        }

        public class UpdateListings : IJob
        {
            private readonly Entities db = new Entities();
            private List<Listing> listings = new List<Listing>();

            async Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                // Find expired listings
                listings = db.Listings.Where(l => l.isActive == 1 && l.Expires < DateTime.Now && l.ActiveDays == 0).ToList();

                await Task.Run(() =>
                    {
                        foreach (Listing list in listings)
                        {
                            // Soft delete listing
                            list.isActive = 0;

                            db.Entry(list).State = EntityState.Modified;
                            db.SaveChanges();

                            //Remove entries from view table
                            var views = db.ListingViews.Where(v => v.ListingID == list.ListingID).ToList();

                            views.RemoveAll((x) => x.ListingID == list.ListingID);
                            db.SaveChanges();
                        }

                        System.Diagnostics.Debug.WriteLine("Update Listings - Done");
                    }
                );
            }
        }

        public class VerifySubs : IJob
        {
            private readonly CreditService creditService = new CreditService();
            private readonly Entities db = new Entities();
            private List<Subscription> subs = new List<Subscription>();

            async Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                //Verify subscriptions are still active
                subs = db.Subscriptions
                  .Where(s => DbFunctions.TruncateTime(s.VerifyDate) == DbFunctions.TruncateTime(DateTime.Now))
                  .ToList();

                await Task.Run(() =>
                {
                    foreach (Subscription sub in subs)
                    {
                        if (PayPalService.GetAgreementState(sub.AgreementID).ToLower() != "active")
                        {
                            User user = db.Users.Find(sub.UserID);

                            user.isSubscribed = 0;

                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();

                            db.Subscriptions.Remove(sub);
                            db.SaveChanges();
                        }
                        else
                        {
                            creditService.CreateTransactionHistory(sub.BillingID, sub.UserID);
                        }
                    }

                    System.Diagnostics.Debug.WriteLine("Verify Subscriptions - Done");
                }
                );
            }
        }
    }
}