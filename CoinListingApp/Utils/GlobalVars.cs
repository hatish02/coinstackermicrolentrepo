﻿namespace CoinListingApp.Utils
{
    public static class GlobalVars
    {
        public const string CoinstackerEmail = "info@coinstacker.co.uk";

        public const string HostUrl = "http://www.coinstacker.co.uk";
    }
}