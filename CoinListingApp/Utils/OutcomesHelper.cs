﻿using Ether.Outcomes;
using Ether.Outcomes.Builder;

namespace CoinListingApp.Utils
{

    public enum FailureCode
    {
        Unknown = 0,

        NotEnoughCredits = 1
    }

    public static class OutcomesHelper
    {
        private const string FailureCodeKey = "failureCode";

        public const string CreditsKey = "credits";
        public const string NeededCreditsKey = "neededCredits";

        public static FailureCode GetFailureCode<T>(this IOutcome<T> outcome)
        {
            if (!outcome.Keys.ContainsKey(FailureCodeKey))
            {
                return default;
            }
    
            return (FailureCode)outcome.Keys[FailureCodeKey];
        }
    
        public static FailureCode GetFailureCode(this IOutcome outcome)
        {
            if (!outcome.Keys.ContainsKey(FailureCodeKey))
            {
                return default;
            }
    
            return (FailureCode)outcome.Keys[FailureCodeKey];
        }
    
        public static IFailureOutcomeBuilder<T> WithFailureCode<T>(this IFailureOutcomeBuilder<T> builder, FailureCode failureCode)
        {
            builder.WithKey(FailureCodeKey, failureCode);
    
            return builder;
        }
    
        
    }
}