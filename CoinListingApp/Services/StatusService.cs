﻿using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class StatusService
    {
        private readonly Entities db = new Entities();

        public Status CreateStatus(string state)
        {
            Status status = new Status
            {
                State = state
            };

            db.Status.Add(status);
            db.SaveChanges();
            db.Entry(status).State = EntityState.Detached;

            return status;
        }

        public bool DoesStatusExist(string state)
        {
            Status status = db.Status.Where(s => s.State == state).FirstOrDefault();

            if (status != null)
            {
                return true;
            }

            return false;
        }

        public Status FindStatus(string state)
        {
            return db.Status.Where(s => s.State == state).FirstOrDefault();
        }

        public Status GetOrCreateStatus(string state)
        {
            if (!this.DoesStatusExist(state))
            {
                return this.CreateStatus(state);
            }

            return this.FindStatus(state);
        }
    }
}
