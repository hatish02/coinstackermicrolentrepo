﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class PaymentService
    {
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();

        public bool CreatePreferredPayment(Payments payment)
        {
            var id = userService.GetCurrentID();

            Payment payments = db.Payments.Where(p => p.UserID == id).Where(p => p.Type == 1).FirstOrDefault();

            if (payments == null)
            {
                Payment pay = new Payment
                {
                    Type = 1,
                    Message = payment.InstructionsMessage,
                    AutoNotify = payment.AutomaticNotifications,
                    UserID = id,
                    //applyToAll = payment.OptionOneApplyToAll
                };

                db.Payments.Add(pay);
                db.SaveChanges();
            }

            if (payments != null)
            {
                Payment pay = payments;
                pay.Message = payment.InstructionsMessage;
                pay.AutoNotify = payment.AutomaticNotifications;

                db.Entry(pay).State = EntityState.Modified;
                db.SaveChanges();
            }

            payments = db.Payments.Where(p => p.UserID == id).Where(p => p.Type == 2).FirstOrDefault();

            if (payments == null)
            {
                Payment pay = new Payment
                {
                    Type = 2,
                    Email = payment.PaypalEmail,
                    UserID = id
                };

                db.Payments.Add(pay);
                db.SaveChanges();
            }

            if (payments != null)
            {
                Payment pay = payments;
                pay.Email = payment.PaypalEmail;
                pay.Message = payment.PaypalEmail;

                db.Entry(pay).State = EntityState.Modified;
                db.SaveChanges();
            }

            return true;
        }

        public bool HasPaypal(int listingID)
        {
            Listing listing = db.Listings.Find(listingID);

            User user = db.Users.Where(r => r.UserID == listing.SellerD).FirstOrDefault();

            Payments payment = RetrievePaymentInfoWithUserID(user.UserID);

            if (payment != null && !string.IsNullOrEmpty(payment.PaypalEmail))
            {
                return true;
            }

            return false;
        }

        public bool HasPreferredPayments()
        {
            var userid = userService.GetCurrentID();

            List<Payment> payments = db.Payments.Where(p => p.UserID == userid).ToList();

            if (payments == null || payments.Count == 0)
            {
                return false;
            }

            return true;
        }

        public Payments RetrievePaymentInfo()
        {
            var userID = userService.GetCurrentID();

            List<Payment> payments = db.Payments.Where(p => p.UserID == userID).ToList();
            Payments payment = new Payments();

            foreach (Payment pay in payments)
            {
                if (pay.Type == PaymentTypeProvider.BankTransferType)
                {
                    payment.InstructionsMessage = pay.Message;
                    payment.AutomaticNotifications = (byte)pay.AutoNotify;
                    //payment.OptionOneApplyToAll = (byte)(pay.applyToAll == null ? 0 : pay.applyToAll);
                }

                if (pay.Type == PaymentTypeProvider.PayPalType)
                {
                    payment.PaypalEmail = pay.Email;
                }
            }

            return payment;
        }

        public Payments RetrievePaymentInfoWithUserID(int userID)
        {
            List<Payment> payments = db.Payments.Where(p => p.UserID == userID).ToList();
            Payments payment = new Payments();

            foreach (Payment pay in payments)
            {
                if (pay.Type == PaymentTypeProvider.BankTransferType)
                {
                    payment.InstructionsMessage = pay.Message;
                    payment.AutomaticNotifications = (byte)pay.AutoNotify;
                }

                if (pay.Type == PaymentTypeProvider.PayPalType)
                {
                    payment.PaypalEmail = pay.Email;
                }
            }

            return payment;
        }
    }
}