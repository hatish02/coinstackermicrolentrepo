﻿using System.Collections.Generic;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class SellerService
    {
        private readonly Entities db;
        private readonly FavouriteService favouriteService;
        private readonly UserService userService;

        public SellerService()
        {
            this.userService = new UserService();
            this.favouriteService = new FavouriteService();
            this.db = new Entities();
        }

        public IEnumerable<SellerViewModel> CreateSellersViewModel(bool onlyWithActiveListings)
        {
            IQueryable<Seller> sellers = null;

            if (onlyWithActiveListings)
                sellers = db.Sellers.Where(s => s.User.isActive == 1 && s.User.Listings.Any(l => l.isActive == 1));
            else
                sellers = db.Sellers.Where(s => s.User.isActive == 1);

            var sellersViewModel = new List<SellerViewModel>();
            foreach (var seller in sellers)
            {
                sellersViewModel.Add(
                    new SellerViewModel
                    {
                        Seller = seller.User,
                        MaskedSellerName = this.userService.MaskUsername(seller.User),
                        AverageRating = this.userService.GetSellerAverageRating(seller.UserID),
                        FormattedSignUpDate = seller.User.SignUpDate is null ? string.Empty : seller.User.SignUpDate.Value.ToString("d MMMM yyyy"),
                        CityAndCountry = this.userService.GetSellerCityAndCountry(seller.User.Address),
                        Favourite = this.favouriteService.IsFavourite(seller.UserID),
                        CurrentUserId = userService.GetAnIdentifier()
                    });
            }

            return sellersViewModel;
        }
    }
}