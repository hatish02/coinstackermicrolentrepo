﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class MessageService
    {
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly SellerService sellerService = new SellerService();
        private readonly StatusService statusService = new StatusService();
        private readonly UserService userService = new UserService();

        public ChatViewModel GetChat(int conversationId)
        {
            var currentUserId = userService.GetCurrentID();

            if (currentUserId == -1)
            {
                return null;
            }

            //Auth check
            Conversation conversation = db.Conversations.Find(conversationId);

            if (conversation == null || conversation.User1 != currentUserId && conversation.User2 != currentUserId)
            {
                return null;
            }

            return this.GetMessages(currentUserId, conversation);
        }

        public List<IndividualConversation> GetChats()
        {
            var currentUserId = userService.GetCurrentID();

            var chats = db.Conversations.Where(c => c.User1 == currentUserId || c.User2 == currentUserId);

            List<IndividualConversation> individualChats = new List<IndividualConversation>();

            foreach (Conversation chat in chats)
            {
                var otherUser = chat.User1 == currentUserId ? chat.User3 : chat.User;

                DateTime? lastMessageTimeStamp = null;

                if (chat.ConversationReplies.Any())
                {
                    lastMessageTimeStamp = chat.ConversationReplies.Max(cr => cr.Timestamp);
                }

                IndividualConversation individualConversation = new IndividualConversation
                {
                    SellerUserId = otherUser.UserID,
                    SellerMaskedName = this.userService.MaskUsername(otherUser),
                    ConversationID = chat.ConversationID,
                    LastMessageTimeStamp = lastMessageTimeStamp
                };

                individualChats.Add(individualConversation);
            }

            return individualChats.OrderByDescending(ic => ic.LastMessageTimeStamp).ToList();
        }

        public MessagesIndexViewModel GetMessagesIndexViewModel()
        {
            var sellers = this.sellerService.CreateSellersViewModel(false).OrderBy(s => s.MaskedSellerName).ToList();

            sellers.RemoveAll(p => p.CurrentUserId == p.Seller.UserID);

            return new MessagesIndexViewModel
            {
                CurrentUserConversations = this.GetChats(),
                Sellers = sellers
            };
        }

        public bool SendMessage(SendMessageViewModel sendMessageViewModel)
        {
            var currentUserId = this.userService.GetCurrentID();

            Conversation conversation;

            if (sendMessageViewModel.ConversationId.HasValue)
            {
                //Auth check
                conversation = db.Conversations.Find(sendMessageViewModel.ConversationId.Value);

                if (conversation.User1 != currentUserId && conversation.User2 != currentUserId)
                {
                    return false;
                }
            }
            else
            {
                conversation = this.GetAndCreateConversationIfNeeded(currentUserId, sendMessageViewModel.ReceiverUserId);
            }

            var newMessage = new ConversationReply
            {
                ConversationID = conversation.ConversationID,
                Timestamp = DateTime.UtcNow.AddHours(1),
                StatusID = this.statusService.GetOrCreateStatus("SENT").StatusID,
                Message = sendMessageViewModel.Message,
                User1 = currentUserId,
            };

            db.ConversationReplies.Add(newMessage);
            db.SaveChanges();

            //Send email
            if (currentUserId == conversation.User1)
            {
                notificationService.SendChatNotification(conversation.User2, newMessage);
            }
            else
            {
                notificationService.SendChatNotification(conversation.User1, newMessage);
            }

            return true;
        }

        public int StartNewChat(int receiverUserId)
        {
            var currentUserId = this.userService.GetCurrentID();

            return this.GetAndCreateConversationIfNeeded(currentUserId, receiverUserId).ConversationID;
        }

        private Conversation GetAndCreateConversationIfNeeded(int currentUserId, int receiverUserId)
        {
            var conversations = db.Conversations.Where(c => (c.User1 == currentUserId && c.User2 == receiverUserId) || (c.User1 == receiverUserId && c.User2 == currentUserId));

            if (!conversations.Any())
            {
                var conversation = new Conversation
                {
                    User1 = currentUserId,
                    User2 = receiverUserId,
                    StatusID = this.statusService.GetOrCreateStatus("CREATED").StatusID
                };

                db.Conversations.Add(conversation);
                db.SaveChanges();

                return conversation;
            }

            return conversations.First();
        }

        private ChatViewModel GetMessages(int currentUserId, Conversation conversation)
        {
            List<ConversationReply> messages = db.ConversationReplies.Where(c => c.ConversationID == conversation.ConversationID).OrderBy(c => c.Timestamp).ToList();

            if (messages is null || messages.Count < 1)
            {
                messages = new List<ConversationReply>();
            }
            else
            {
                //Set status to read
                var statusID = this.statusService.GetOrCreateStatus("READ").StatusID;

                foreach (ConversationReply mess in messages)
                {
                    mess.StatusID = statusID;

                    db.Entry(mess).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            var otherUser = this.GetOtherUser(currentUserId, conversation);

            var chat = new ChatViewModel
            {
                Chats = messages,
                Conversation = conversation,
                CurrentUserId = currentUserId,
                CurrentUserFullName = this.userService.GetCurrentUsername(),
                OtherUserId = otherUser.UserID,
                OtherUserMaskedName = this.userService.MaskUsername(otherUser)
            };

            return chat;
        }

        private User GetOtherUser(int currentUserId, Conversation conversation)
        {
            if (currentUserId != conversation.User1)
            {
                return conversation.User;
            }

            return conversation.User3;
        }
    }
}