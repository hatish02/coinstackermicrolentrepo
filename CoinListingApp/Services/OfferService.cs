﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class OfferService
    {
        private const int MaxCounterOffers = 3;
        private static readonly string[] ClosedOfferStatus = new[] { "ACCEPTED", "DECLINED", "WITHDRAW" };
        private readonly Entities db = new Entities();
        private readonly ListingService listingService = new ListingService();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly OrderService orderService = new OrderService();
        private readonly StatusService statusService = new StatusService();
        private readonly UserService userService = new UserService();

        public int? AcceptOffer(int id)
        {
            Offer offer = db.Offers.Find(id);

            if (offer == null)
            {
                return null;
            }

            int offerAcceptedstatusID = this.statusService.GetOrCreateStatus("ACCEPTED").StatusID;

            offer.StatusID = offerAcceptedstatusID;
            offer.SellerUpdate = 1;
            offer.BuyerUpdate = 1;

            db.Entry(offer).State = EntityState.Modified;
            db.SaveChanges();

            int orderCreatedStatusId = this.statusService.GetOrCreateStatus("CREATED").StatusID;

            //TrackingID
            Tracking tracking = db.Trackings.Where(t => t.SellerID == offer.Listing.SellerD).FirstOrDefault();

            if (tracking == null)
            {
                return null;
            }

            Listing listing = db.Listings.Find(offer.ListingID);
            PostageListing cheapestPostage = null;

            foreach (var postageListing in listing.PostageListings)
            {
                if (cheapestPostage is null ||
                     this.listingService.CalculateTotalPricePostage(cheapestPostage, offer.QTY) > this.listingService.CalculateTotalPricePostage(postageListing, offer.QTY))
                {
                    cheapestPostage = postageListing;
                }
            }

            Order order = new Order
            {
                //ListingID = offer.ListingID,
                StatusID = orderCreatedStatusId,
                BuyerID = offer.BuyerID,
                TrackingID = tracking.TrackingID,
                QTY = offer.QTY,
                Price = offer.Price,
                CreatedDate = DateTime.Now,
                PaymentType = offer.Listing.PaymentType,
                PostageId = cheapestPostage?.PostageID
            };
            db.Orders.Add(order);

            db.SaveChanges();

            OrderListing orderListing = new OrderListing
            {
                ListingId = offer.ListingID,
                OrderId = order.OrderID,
                Price = offer.Price,
                Quantity = offer.QTY
            };

            db.OrderListings.Add(orderListing);
            db.SaveChanges();

            listing.QTY -= offer.QTY;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            if (listing.QTY == 0)
            {
                // Send notification
                notificationService.SendListingUnavailableNotification(listing.SellerD, listing.ListingID, "Sold out");

                listing.isActive = 0;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
            }

            if (order.PaymentType == PaymentTypeProvider.BankTransferType)
            {
                this.orderService.OrderNotPaypalPayment(order, listing.SellerD, true);
            }

            return order.OrderID;
        }

        public bool CounterOffer(int OfferID, int QTY, double Price)
        {
            Offer offer = db.Offers.Find(OfferID);

            if (offer == null)
            {
                return false;
            }

            if (offer.Counters >= MaxCounterOffers)
            {
                return false;
            }

            int statusID = this.statusService.GetOrCreateStatus("COUNTERED").StatusID;

            //Seller was the last to counter -> buyer is counter offering
            if (offer.SellerUpdate == 1)
            {
                offer.SellerUpdate = 0;
                offer.BuyerUpdate = 1;
            }
            else
            //Buyer was the last to counter -> seller is counter offering
            {
                offer.SellerUpdate = 1;
                offer.BuyerUpdate = 0;
            }

            offer.Counters += 1;
            offer.StatusID = statusID;

            offer.QTY = QTY;
            offer.Price = Price;

            db.Entry(offer).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public bool DeclineOffer(int id)
        {
            Offer offer = db.Offers.Find(id);

            if (offer == null)
            {
                return false;
            }

            int statusID = this.statusService.GetOrCreateStatus("DECLINED").StatusID;

            offer.StatusID = statusID;
            offer.SellerUpdate = 1;
            offer.BuyerUpdate = 1;

            db.Entry(offer).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public OffersViewModel GetOffers()
        {
            var userId = userService.GetCurrentID();

            var allOffersAsBuyer = db.Offers.Where(o => o.BuyerID == userId);
            var offersAsBuyer = this.AdaptToViewModel(allOffersAsBuyer.Where(o => !ClosedOfferStatus.Contains(o.Status.State)));
            var maxBuyerPossibleActions = this.GetMaxPossibleActions(offersAsBuyer, true);
            var closedOffersAsBuyer = this.AdaptToViewModel(allOffersAsBuyer.Where(o => ClosedOfferStatus.Contains(o.Status.State)));

            var sellerListingsIds = db.Listings.Where(l => l.SellerD == userId).Select(l => l.ListingID).ToList();
            var allOffersAsSeller = db.Offers.Where(o => sellerListingsIds.Contains(o.ListingID)).ToList();
            var offersAsSeller = this.AdaptToViewModel(allOffersAsSeller.Where(o => !ClosedOfferStatus.Contains(o.Status.State)));
            var maxSellerPossibleActions = this.GetMaxPossibleActions(offersAsSeller, false);
            var closedOffersAsSeller = this.AdaptToViewModel(allOffersAsSeller.Where(o => ClosedOfferStatus.Contains(o.Status.State)));

            return new OffersViewModel
            {
                UserId = userId,
                MaxCounterOffers = MaxCounterOffers,
                OffersAsBuyer = offersAsBuyer,
                MaxBuyerPossibleActions = maxBuyerPossibleActions,
                ClosedOffersAsBuyer = closedOffersAsBuyer,
                OffersAsSeller = offersAsSeller,
                MaxSellerPossibleActions = maxSellerPossibleActions,
                ClosedOffersAsSeller = closedOffersAsSeller
            };
        }

        public int MakeOffer(int listingID, int QTY, double Price)
        {
            var userId = userService.GetCurrentID();

            int offerID = 0;

            //Create offer
            Offer offer = db.Offers.Where(o => o.ListingID == listingID && o.BuyerID == userId).FirstOrDefault();

            if (offer is null)
            {
                //StatusID
                int statusID = this.statusService.GetOrCreateStatus("CREATED").StatusID;

                Offer newOffer = new Offer
                {
                    ListingID = listingID,
                    StatusID = statusID,
                    BuyerID = userId,
                    QTY = QTY,
                    Price = Price,
                    Counters = 0,
                    SellerUpdate = 0,
                    BuyerUpdate = 1
                };

                db.Offers.Add(newOffer);
                db.SaveChanges();

                offerID = newOffer.OfferID;
            }

            return offerID;
        }

        public bool OfferExists(int listingID)
        {
            var userId = userService.GetCurrentID();

            if (userId <= 0)
            {
                return false;
            }

            Offer offer = db.Offers.Where(o => o.ListingID == listingID && o.BuyerID == userId).FirstOrDefault();

            if (offer != null)
            {
                return true;
            }

            return false;
        }

        public bool WithdrawOffer(int id)
        {
            Offer offer = db.Offers.Find(id);

            if (offer == null)
            {
                return false;
            }

            int statusID = this.statusService.GetOrCreateStatus("WITHDRAW").StatusID;

            offer.StatusID = statusID;

            offer.SellerUpdate = 1;
            offer.BuyerUpdate = 1;

            db.Entry(offer).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        private List<OfferViewModel> AdaptToViewModel(IEnumerable<Offer> offers)
        {
            return offers.Select(o => new OfferViewModel
            {
                OfferID = o.OfferID,
                Listing = o.Listing,
                Price = o.Price,
                Amount = o.QTY,
                Counters = o.Counters,
                Status = o.Status,
                BuyerID = o.BuyerID,
                BuyerMaskedName = this.userService.MaskUsername(o.User),
                SellerMaskedName = this.userService.MaskUsername(o.Listing.User),
                BuyerActionPending = o.BuyerUpdate == 0,
                SellerActionPending = o.SellerUpdate == 0
            }).OrderByDescending(o => o.OfferID).ToList();
        }

        private int GetMaxPossibleActions(List<OfferViewModel> offers, bool buyerContext)
        {
            if (offers.Any(o =>
                (buyerContext ?
                    o.BuyerActionPending && !o.SellerActionPending : !o.BuyerActionPending && o.SellerActionPending)
                && o.Counters < MaxCounterOffers))
            {
                return 3; // counter, accept or withdraw/decline
            }
            else if (offers.Any(o =>
               buyerContext ?
                   o.BuyerActionPending && !o.SellerActionPending : !o.BuyerActionPending && o.SellerActionPending
              ))
            {
                return 2; // accept or withdraw/decline
            }

            return 1; // withdraw/decline
        }
    }
}