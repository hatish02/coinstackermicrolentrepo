﻿using CoinListingApp.Models;
using System.Collections.Generic;
using System.Linq;

namespace CoinListingApp.Services
{
    public class CategoryService
    {
        private readonly Entities db = new Entities();
        
        public bool DoesCategoryHaveListings(int id)
        {
            List<Listing> listings = db.Listings.Where(l => l.CategoryID == id).ToList();

            if (listings.Count > 0)
            {
                return true;
            }

            return false;
        }
    }
}