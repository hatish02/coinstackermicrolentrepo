﻿using System.Text.RegularExpressions;

namespace CoinListingApp.Services.Validation
{
    public static class StringExtensions
    {
        public static string GetValidFileName(this string fileName)
        {
            // remove any invalid character from the filename.
            string ret = Regex.Replace(fileName.Trim(), "[^A-Za-z0-9_. ]+", "");
            return ret.Replace(" ", "_");
        }
    }
}