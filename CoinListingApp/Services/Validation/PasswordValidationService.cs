﻿using System.Text.RegularExpressions;

namespace CoinListingApp.Services.Validation
{
    public static class PasswordValidationService
    {
        public static string GetErrorMessage(object value)
        {
            var errorMessage = string.Empty;

            if (value is null)
            {
                return errorMessage;
            }

            var password = value.ToString();

            if (password.Length < 6)
            {
                errorMessage += "Password must have at least 6 characters long.\n";
            }

            if (!Regex.IsMatch(password, @"[^a-zA-Z0-9]"))
            {
                errorMessage += "Passwords must have at least one non letter or digit character.\n";
            }

            if (!Regex.IsMatch(password, @"[a-z]"))
            {
                errorMessage += "Passwords must have at least one lowercase('a' - 'z').\n";
            }

            if (!Regex.IsMatch(password, @"[A-Z]"))
            {
                errorMessage += "Passwords must have at least one uppercase('A' - 'Z').\n";
            }

            return errorMessage;
        }
    }
}