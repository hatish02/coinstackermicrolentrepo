﻿using CoinListingApp.Utils;
using CoinListingApp.Models;
using CoinListingApp.Resources;
using Microsoft.Ajax.Utilities;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.WebPages.Scope;
using Address = CoinListingApp.Models.Address;

namespace CoinListingApp.Services
{
    public class PayPalService
    {
        private static CountryCodes countryCodes = new CountryCodes();
        
        public static PayPal.Api.Payment CreatePayment(string baseUrl)
        {
            // ### Api Context
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = PaypalConfiguration.GetAPIContext();

            // Payment Resource
            var payment = new PayPal.Api.Payment()
            {
                intent = "sale",    // `sale` or `authorize`
                payer = new Payer() { payment_method = "paypal" },
                transactions = GetTransactionsList(),
                redirect_urls = GetReturnUrls(baseUrl)
            };

            // Create a payment using a valid APIContext
            var createdPayment = payment.Create(apiContext);

            return createdPayment;
        }

        private static List<Transaction> GetTransactionsList()
        {
            // A transaction defines the contract of a payment
            // what is the payment for and who is fulfilling it. 
            var transactionList = new List<Transaction>();

            // The Payment creation API requires a list of Transaction; 
            // add the created Transaction to a List
            transactionList.Add(new Transaction()
            {
                description = "Transaction description.",
                invoice_number = "GenerateRandomInvoice()",
                amount = new Amount()
                {
                    currency = "USD",
                    total = "100.00",       // Total must be equal to sum of shipping, tax and subtotal.
                    details = new Details() // Details: Let's you specify details of a payment amount.
                    {
                        tax = "15",
                        shipping = "10",
                        subtotal = "75"
                    }
                },
                item_list = new ItemList()
                {
                    items = new List<Item>()
                    {
                        new Item()
                        {
                            name = "Item Name",
                            currency = "USD",
                            price = "15",
                            quantity = "5",
                            sku = "sku"
                        }
                    }
                }
            });
            return transactionList;
        }

        private static RedirectUrls GetReturnUrls(string baseUrl)
        {
            var returnUrl = "/Home/PaymentSuccessful";

            // Redirect URLS
            // These URLs will determine how the user is redirected from PayPal 
            // once they have either approved or canceled the payment.
            return new RedirectUrls()
            {
                //cancel_url = baseUrl + "/Home/PaymentCancelled",
                cancel_url = baseUrl + "&Cancel=true",
                return_url = baseUrl + returnUrl
            };
        }

        public static Item CreateItemObject(string name, string price, string quantity, string sku)
        {
            return new Item()
            {
                name = name,
                currency = "GBP",
                price = price,
                quantity = quantity,
                sku = sku
            };
        }

        public static Transaction CreateTransactionObject(string description, string invoice_number, Amount amount, ItemList items)
        {
            return new Transaction()
            {
                description = description,
                invoice_number = invoice_number,
                amount = amount,
                item_list = items
            };
        }

        public static Transaction CreateTransactionObjectWithPayee(string description, string invoice_number, Amount amount, ItemList items, string payee)
        {
            return new Transaction()
            {
                description = description,
                invoice_number = invoice_number,
                amount = amount,
                item_list = items,
                payee = new Payee
                {
                    email = payee
                }
            };
        }

        public static Amount CreateAmountObject(string total, Details details)
        {
            return new Amount()
            {
                currency = "GBP",
                total = total,
                details = details
            };
        }

        public static Details CreateDetailsObject(string tax, string shipping, string subtotal)
        {
            return new Details()
            {
                tax = tax,
                shipping = shipping,
                subtotal = subtotal
            };
        }

        public static PayPal.Api.Payment ExecutePayment(string paymentId, string payerId)
        {
            // ### Api Context
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = PaypalConfiguration.GetAPIContext();

            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            var payment = new PayPal.Api.Payment() { id = paymentId };

            // Execute the payment.
            var executedPayment = payment.Execute(apiContext, paymentExecution);

            return executedPayment;
        }

        //========================================================================================
        //Subscriptions

        private static Currency GetCurrency(string value)
        {
            return new Currency
            {
                currency = "GBP",
                value = value
            };
        }

        public static RedirectUrls GetReturnUrls(string baseUrl, string returnUrl)
        {
            //var returnUrl = "/Home/PaymentSuccessful";

            // Redirect URLS
            // These URLs will determine how the user is redirected from PayPal 
            // once they have either approved or canceled the payment.
            return new RedirectUrls()
            {
                //cancel_url = baseUrl + "/Home/PaymentCancelled",
                cancel_url = baseUrl + "&Cancel=true",
                return_url = baseUrl + returnUrl
            };
        }

        public static Plan CreateBillingPlan(string planName, string planDescription, string returnUrl, string cancelUrl,
            string frequency, int frequencyInterval, decimal planPrice, int duration,
            decimal shippingAmount = 0, decimal taxPercentage = 0, bool trial = false, int trialLength = 0, decimal trialPrice = 0)
        {
            // Define the plan and attach the payment definitions and merchant preferences.
            return new Plan
            {
                name = planName,
                description = planDescription,
                type = "fixed",

                // Define the merchant preferences.
                merchant_preferences = new MerchantPreferences()
                {
                    setup_fee = GetCurrency("0"),
                    return_url = returnUrl,
                    cancel_url = cancelUrl,
                    auto_bill_amount = "YES",
                    initial_fail_amount_action = "CONTINUE",
                    max_fail_attempts = "0"
                },
                payment_definitions = GetPaymentDefinitions(trial, trialLength, trialPrice, frequency, frequencyInterval, planPrice, shippingAmount, taxPercentage, duration)
            };
        }

        private static List<PaymentDefinition> GetPaymentDefinitions(bool trial, int trialLength, decimal trialPrice,
            string frequency, int frequencyInterval, decimal planPrice, decimal shippingAmount, decimal taxPercentage, int duration)
        {
            var paymentDefinitions = new List<PaymentDefinition>();

            if (trial)
            {
                // Define a trial plan that will charge 'trialPrice' for 'trialLength'
                // After that, the standard plan will take over.
                paymentDefinitions.Add(
                    new PaymentDefinition()
                    {
                        name = "Trial",
                        type = "TRIAL",
                        frequency = frequency,
                        frequency_interval = frequencyInterval.ToString(),
                        amount = GetCurrency(trialPrice.ToString()),
                        cycles = trialLength.ToString(),
                        charge_models = GetChargeModels(trialPrice, shippingAmount, taxPercentage)
                    });
            }

            // Define the standard payment plan. It will represent a 'frequency' (monthly, etc)
            // plan for 'planPrice' that charges 'planPrice' (once a month) for #cycles.
            var regularPayment = new PaymentDefinition
            {
                name = "Standard Plan",
                type = "REGULAR",
                frequency = frequency,
                frequency_interval = frequencyInterval.ToString(),
                amount = GetCurrency(planPrice.ToString()),
                cycles = duration.ToString(),
                charge_models = GetChargeModels(trialPrice, shippingAmount, taxPercentage)
            };
            paymentDefinitions.Add(regularPayment);

            return paymentDefinitions;
        }

        private static List<ChargeModel> GetChargeModels(decimal planPrice, decimal shippingAmount, decimal taxPercentage)
        {
            // Create the Billing Plan
            var chargeModels = new List<ChargeModel>();
            if (shippingAmount > 0)
            {
                chargeModels.Add(new ChargeModel()
                {
                    type = "SHIPPING",
                    amount = GetCurrency(shippingAmount.ToString())
                });
            }
            if (taxPercentage > 0)
            {
                chargeModels.Add(new ChargeModel()
                {
                    type = "TAX",
                    amount = GetCurrency(String.Format("{0:f2}", planPrice * taxPercentage / 100))
                });
            }

            return chargeModels;
        }

        public static string ActivateBillingPlan(Plan plan)
        {
            // PayPal Authentication tokens
            var apiContext = PaypalConfiguration.GetAPIContext();

            // create plan first
            var createdPlan = plan.Create(apiContext);

            // Activate the plan
            var patchRequest = new PatchRequest()
            {
                new Patch()
                {
                    op = "replace",
                    path = "/",
                    value = new Plan {state = "ACTIVE"}
                }
            };
            createdPlan.Update(apiContext, patchRequest);

            return createdPlan.id;
        }

        public static void DeactivateBillingPlan(string planId)
        {
            // PayPal Authentication tokens
            var apiContext = PaypalConfiguration.GetAPIContext();

            // Retrieve Plan
            var plan = Plan.Get(apiContext, planId);

            // Activate the plan
            var patchRequest = new PatchRequest()
            {
                new Patch()
                {
                    op = "replace",
                    path = "/",
                    value = new Plan {state = "INACTIVE"}
                }
            };
            plan.Update(apiContext, patchRequest);
        }

        public static Agreement CreateBillingAgreement(string planId, ShippingAddress shippingAddress,
            string name, string description, DateTime startDate)
        {
            // PayPal Authentication tokens
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = new Agreement()
            {
                name = name,
                description = description,
                start_date = startDate.ToString("yyyy-MM-ddTHH:mm:ss") + "Z",
                payer = new Payer() { payment_method = "paypal" },
                plan = new Plan() { id = planId },
                shipping_address = shippingAddress
            };

            var createdAgreement = agreement.Create(apiContext);
            return createdAgreement;
        }
        
        //Activate subscription
        public static string ExecuteBillingAgreement(string token)
        {
            // PayPal Authentication tokens
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = new Agreement() { token = token };
            var executedAgreement = agreement.Execute(apiContext);

            return executedAgreement.id;
        }

        //Suspend subscription
        public static void SuspendBillingAgreement(string agreementId)
        {
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = new Agreement() { id = agreementId };
            agreement.Suspend(apiContext, new AgreementStateDescriptor()
            { note = "Suspending the agreement" });
        }

        //Reactivate subscription
        public static void ReactivateBillingAgreement(string agreementId)
        {
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = new Agreement() { id = agreementId };
            agreement.ReActivate(apiContext, new AgreementStateDescriptor()
            { note = "Reactivating the agreement" });
        }

        //Cancel subscription
        public static void CancelBillingAgreement(string agreementId)
        {
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = new Agreement() { id = agreementId };
            agreement.Cancel(apiContext, new AgreementStateDescriptor()
            { note = "Cancelling the agreement" });
        }

        public static string GetAgreementState(string agreementid)
        {
            var apiContext = PaypalConfiguration.GetAPIContext();

            var agreement = Agreement.Get(apiContext, agreementid);

            return agreement.state;
        }

        // ========================================================================== Helpers

        public static ShippingAddress CreateShippingAddress(Address address)
        {
            return new ShippingAddress() {
                city = address.City,
                line1 = address.Address_line1,
                postal_code = address.PostalCode,
                //country_code = countryCodes.GetCountryCode(address.Country)
                country_code="GB"
            };
        }
    }
}