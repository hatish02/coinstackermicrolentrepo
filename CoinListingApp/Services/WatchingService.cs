﻿using System.Collections.Generic;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class WatchingService
    {
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();

        public bool DeleteWatching(int id)
        {
            Listing listing = db.Listings.Find(id);

            if (listing == null)
            {
                return false;
            }

            int userID = userService.GetCurrentID();

            Watching watch = db.Watchings.Where(f => f.UserID == userID && f.ListingID == id).FirstOrDefault();

            if (watch == null)
            {
                return false;
            }

            db.Watchings.Remove(watch);
            db.SaveChanges();

            return true;
        }

        public HashSet<int> GetUserWatchingListingsIds()
        {
            var userId = this.userService.GetCurrentID();

            var watchings = db.Watchings.Where(f => f.UserID == userId);

            var result = new HashSet<int>();

            if (watchings is null)
            {
                return result;
            }

            foreach (var w in watchings)
            {
                result.Add(w.ListingID);
            }

            return result;
        }

        public bool IsWatching(int id)
        {
            Listing listing = db.Listings.Find(id);

            if (listing == null)
            {
                return false;
            }

            int userID = userService.GetAnIdentifier();

            Watching watch = db.Watchings.Where(f => f.UserID == userID && f.ListingID == id).FirstOrDefault();

            if (watch == null)
            {
                return false;
            }

            return true;
        }
    }
}
