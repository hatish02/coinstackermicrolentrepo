﻿using System.Collections.Generic;

namespace CoinListingApp.Services
{
    public static class PaymentTypeProvider
    {
        public const string BankTransferNickName = "EFT";

        public const string PayPalNickName = "PayPal";

        private static readonly List<Models.Payment> paymentTypes = new List<Models.Payment>
        {
            new Models.Payment(){ ID = BankTransferType, Message = "Bank Transfer or PayPal options" },
            new Models.Payment(){ ID = PayPalType, Message = "PayPal" }
        };

        public static byte BankTransferType => 1;

        public static byte PayPalType => 2;

        public static List<Models.Payment> GetPaymentTypes() => paymentTypes;
    }
}