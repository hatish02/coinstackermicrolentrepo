﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CoinListingApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CoinListingApp.Services
{
    public class UserService
    {
        private readonly ApplicationDbContext aspdb = new ApplicationDbContext();
        private readonly Entities db = new Entities();
        private readonly StatusService statusService = new StatusService();

        public bool CanRate()
        {
            var loggedIn = IsLoggedIn();

            if (loggedIn == false)
            {
                return false;
            }

            return true;
        }

        public void DeleteUser(int userid)
        {
            User user = db.Users.Find(userid);

            // Delete AspNet entry
            AspNetUser aspnetuser = db.AspNetUsers.Where(u => u.Email == user.Email).FirstOrDefault();

            if (aspnetuser is object)
            {
                // Delete AspNetRole entry
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(aspdb));
                string[] roles = UserManager.GetRoles(aspnetuser.Id).ToArray();

                UserManager.RemoveFromRoles(aspnetuser.Id, roles);

                db.AspNetUsers.Remove(aspnetuser);
                db.SaveChanges();
            }

            // Delete Addresses
            if (user.Home_Address != null)
            {
                user.Home_Address = null;
                if (user.Address1 is object)
                    db.Addresses.Remove(user.Address1);
                db.SaveChanges();
            }

            if (user.Delivery_Address != null)
            {
                user.Delivery_Address = null;
                if (user.Address is object)
                    db.Addresses.Remove(user.Address);
                db.SaveChanges();
            }

            // Delete Files
            if (user.ProofOfResidence != null)
            {
                user.ProofOfResidence = null;
                if (user.File1 is object)
                    db.Files.Remove(user.File1);
                db.SaveChanges();
            }

            if (user.CopyOfID != null)
            {
                user.CopyOfID = null;
                if (user.File is object)
                    db.Files.Remove(user.File);
                db.SaveChanges();
            }

            // Remove personal data
            user.Name = "Name";
            user.Surname = "Surname";
            user.Email = "name.surname@email.com";
            user.ProfilePicture = null;
            user.isActive = 0;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            int id = GetCurrentID();

            List<User> users = db.Users.Where(u => u.UserID != id).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public int GetAnIdentifier()
        {
            var id = this.GetCurrentAspNetUserId();

            if (id is null)
            {
                return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }

            var aspUser = db.AspNetUsers.Find(id);

            if (aspUser == null)
            {
                return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }

            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (user == null)
            {
                return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }

            return user.UserID;
        }

        // Home Address
        public Address GetBusinessAddress(User user = null)
        {
            if (user is null)
            {
                user = GetCurrentUser();
            }

            return user.Address1;
        }

        public string GetCurrentChatName(int id)
        {
            var userid = GetCurrentID();

            Conversation c = db.Conversations.Find(id);

            if (c.User.UserID != userid)
            {
                return this.MaskUsername(c.User);
            }

            return this.MaskUsername(c.User3);
        }

        public double GetCurrentCredit(User user = null)
        {
            int userId = 0;
            if (user is null)
            {
                userId = this.GetCurrentID();
            }
            else
            {
                userId = user.UserID;
            }

            var credits = db.Credits.Include(c => c.User).Where(c => c.UserID == userId).FirstOrDefault();

            if (credits == null)
            {
                return -1;
            }

            return credits.Amount;
        }

        public int GetCurrentID()
        {
            var id = this.GetCurrentAspNetUserId();

            if (id is null)
            {
                return -1;
            }

            var aspUser = db.AspNetUsers.Find(id);

            if (aspUser is null)
            {
                return -1;
            }

            var email = aspUser.Email;
            try
            {
                User user = db.Users.Where(r => r.Email == email)?.FirstOrDefault();
                if (user is null)
                {
                    return -1;
                }
                return user.UserID;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public User GetCurrentUser()
        {
            var id = GetCurrentAspNetUserId();

            if (id is null)
            {
                return null;
            }

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            return db.Users.Where(r => r.Email == email).FirstOrDefault();
        }

        public string GetCurrentUsername()
        {
            var id = this.GetCurrentAspNetUserId();

            if (id is null)
            {
                return null;
            }

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            return user.Name + " " + user.Surname;
        }

        // Shipping Address
        public Address GetDeliveryAddress()
        {
            var user = GetCurrentUser();

            return user.Address;
        }

        public string GetIDToVerify(int userid)
        {
            User user = db.Users.Find(userid);

            var aspUser = db.AspNetUsers.Where(a => a.Email == user.Email).FirstOrDefault();

            return aspUser.Id;
        }

        public string GetMaskUsername(int userId)
        {
            User user = db.Users.Find(userId);

            if (user is null)
            {
                return string.Empty;
            }

            return this.MaskUsername(user);
        }
        public string GetCoinstackerName(int userId)
        {
            User user = db.Users.Find(userId);
            if (user is null)
            {
                return string.Empty;
            }
            else if (user?.CoinStackerName != null)
            {
                return user.CoinStackerName;
            }
            return this.MaskUsername(user);
        }

        public bool GetShowListingRulesForCurrentUser()
        {
            var currentUser = this.GetCurrentUser();

            if (currentUser.ShowListingsRules == 0)
            {
                return false;
            }

            return true;
        }

        public bool GetUserFreeAds(int userID)
        {
            return db.Users.Find(userID).hasFreeAds == 0 ? false : true;
        }

        public string GetUserFullName(int userId)
        {
            var user = this.db.Users.Find(userId);

            if (user is null)
            {
                return string.Empty;
            }

            return user.Name + " " + user.Surname;
        }

        public Address GetUserShippingAddress(int userId)
        {
            var user = this.db.Users.Find(userId);

            if (user is null)
            {
                return null;
            }

            return user.Address ?? user.Address1; // Address is the delivery address, Address1 is the home address
        }

        public bool IsCurrentUserSubscribed(User user = null)
        {
            if (user is null)
            {
                var id = HttpContext.Current.User.Identity.GetUserId();

                var aspUser = db.AspNetUsers.Find(id);
                var email = aspUser.Email;

                user = db.Users.Where(r => r.Email == email).FirstOrDefault();
            }

            return user.isSubscribed == 0 ? false : true;
        }

        public bool IsEmailConfirmed(string userEmail)
        {
            AspNetUser aspnetuser = db.AspNetUsers.FirstOrDefault(u => u.Email == userEmail);

            if (aspnetuser is null)
            {
                return false;
            }

            return aspnetuser.EmailConfirmed;
        }

        public bool IsLoggedIn()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public bool IsSignedUp()
        {
            var identity = HttpContext.Current?.User?.Identity;

            if (identity is null)
            {
                return false;
            }

            var id = identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);

            if (aspUser == null)
                return false;

            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (user == null)
            {
                return false;
            }

            return true;
        }

        public string MaskUsername(User user)
        {
            if (string.IsNullOrEmpty(user.Surname))
            {
                return user.Name;
            }

            return user.Name + " " + user.Surname.Substring(0, 1) + ".";
        }

        public bool RateListing(int listingID, string message, double rating)
        {
            int userID = GetCurrentID();

            //SellerID
            Listing listing = db.Listings.Find(listingID);

            if (listing == null || listing.isArchived == 1)
            {
                return false;
            }

            int sellerID = listing.SellerD;

            Rating rate = new Rating
            {
                UserID = userID,
                SellerID = sellerID,
                Reason = message,
                Date = DateTime.Today,
                RatingValue = rating
            };

            if (rate == null)
            {
                return false;
            }

            db.Ratings.Add(rate);
            db.SaveChanges();

            return true;
        }

        public void RateSeller(int sellerId, double rating)
        {
            int currentUserId = GetCurrentID();

            var currentRating = this.db.Ratings.FirstOrDefault(r => r.SellerID == sellerId && r.UserID == currentUserId);

            if (currentRating is null)
            {
                Rating rate = new Rating
                {
                    UserID = currentUserId,
                    SellerID = sellerId,
                    Date = DateTime.Today,
                    RatingValue = rating
                };

                db.Ratings.Add(rate);
            }
            else
            {
                currentRating.RatingValue = rating;
                currentRating.Date = DateTime.Today;
            }

            db.SaveChanges();
        }

        public void TurnOffShowListingRulesForCurrentUser()
        {
            var currentUser = this.GetCurrentUser();

            if (currentUser.ShowListingsRules == 0)
            {
                return;
            }

            currentUser.ShowListingsRules = 0;
            db.SaveChanges();
        }

        private string GetCurrentAspNetUserId()
        {
            var identity = HttpContext.Current?.User?.Identity;

            if (identity is null)
            {
                return null;
            }

            return identity.GetUserId();
        }

        #region Sellers

        public List<KeyValuePair<int, int>> FrequentSellers()
        {
            List<Seller> sellers = db.Sellers.ToList();

            List<KeyValuePair<int, int>> sellerList = new List<KeyValuePair<int, int>>();

            foreach (Seller s in sellers)
            {
                int count = db.Listings.Where(l => l.SellerD == s.UserID && l.isActive > 0 && l.isArchived == 0).Count();

                if (count > 0)
                    sellerList.Add(new KeyValuePair<int, int>(s.UserID, count));
            }

            sellerList.Sort(CompareSellers);

            return sellerList;
        }

        public double GetSellerAverageRating(int sellerId)
        {
            var ratings = this.db.Ratings.Where(l => l.SellerID == sellerId && l.UserID != sellerId).ToList();
            if (ratings is null || !ratings.Any())
            {
                return 0.0;
            }

            var averageRating = ratings.Average(r => r.RatingValue);

            var averageRatingFloor = Math.Floor(averageRating);

            if (averageRating - averageRatingFloor >= 0.5)
            {
                return averageRatingFloor + 0.5;
            }

            return averageRatingFloor;
        }

        public string GetSellerCityAndCountry(Address sellerAddress)
        {
            var result = string.Empty;

            if (sellerAddress is null)
            {
                return result;
            }

            if (!string.IsNullOrEmpty(sellerAddress.City))
            {
                result = sellerAddress.City;
            }

            if (!string.IsNullOrEmpty(sellerAddress.Country))
            {
                if (!string.IsNullOrEmpty(result))
                {
                    result += ", ";
                }

                result += sellerAddress.Country;
            }

            return result;
        }

        public List<Order> GetSellerOrders()
        {
            var id = HttpContext.Current.User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            List<Order> orders = new List<Order>();
            List<Listing> listings = db.Listings.Where(l => l.SellerD == user.UserID).ToList();

            foreach (Listing listing in listings)
            {
                orders.AddRange(db.Orders.Where(o => o.OrderListings.Any(l => l.ListingId == listing.ListingID)).Include(o => o.OrderListings).Include("OrderListings.Listing").Include(o => o.User).Include(o => o.Status).Include(o => o.Tracking).ToList());
            }

            System.Diagnostics.Debug.WriteLine(orders);

            if (orders != null)
            {
                return orders;
            }

            return null;
        }

        public double GetSellerRatingFromCurrentUser(int selledId)
        {
            var currentUserId = this.GetCurrentID();

            var rating = this.db.Ratings.FirstOrDefault(r => r.SellerID == selledId && r.UserID == currentUserId);

            if (rating is null)
            {
                return 0.0;
            }

            return rating.RatingValue;
        }

        public List<User> GetSellers(List<KeyValuePair<int, int>> sellerIDs)
        {
            List<User> sellers = new List<User>();

            foreach (var item in sellerIDs)
            {
                try
                {
                    var lSellerTypeUser = db.Users.Find(item.Key);
                    if (lSellerTypeUser != null)
                    {
                        sellers.Add(lSellerTypeUser);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //sellers.Add(db.Users.Find(item.Key));
            }

            return sellers;
        }

        public bool IsRated(int sellerID)
        {
            var loggedIn = IsLoggedIn();

            if (loggedIn == false)
            {
                return false;
            }

            var userID = GetCurrentID();

            Rating rating = db.Ratings.Where(r => r.SellerID == sellerID && r.UserID == userID).FirstOrDefault();

            if (rating == null)
            {
                return false;
            }

            return true;
        }

        public bool IsReported(int sellerID)
        {
            int userID = GetAnIdentifier();

            ReportUser reported = db.ReportUsers.Where(r => r.SellerID == sellerID && r.UserID == userID).FirstOrDefault();

            if (reported == null)
            {
                return false;
            }

            return true;
        }

        public bool ReportSeller(ReportSellerViewModel report)
        {
            int userID = GetCurrentID();

            ReportUser reported = new ReportUser
            {
                UserID = userID,
                SellerID = report.SellerId,
                Reason = report.Report,
                Date = DateTime.Today
            };

            if (reported == null)
            {
                return false;
            }

            db.ReportUsers.Add(reported);
            db.SaveChanges();

            return true;
        }

        // Seller page
        public void SellerDetails(int userid)
        {
            User user = db.Users.Find(userid);
        }

        public List<Listing> SellerListings(int userid)
        {
            List<Listing> listings = db.Listings.Where(l => l.SellerD == userid && l.isActive > 1 && l.isArchived == 0).ToList();

            return listings;
        }

        public List<Rating> SellerRating(int userid)
        {
            List<Rating> ratings = db.Ratings.Where(r => r.SellerID == userid).ToList();

            return ratings;
        }

        public List<Seller> Sellers()
        {
            List<Seller> sellers = db.Sellers.OrderBy(u => u.User.Name).ToList();

            return sellers;
        }

        private static int CompareSellers(KeyValuePair<int, int> a, KeyValuePair<int, int> b)
        {
            return b.Value.CompareTo(a.Value);
        }

        #endregion Sellers

        #region Buyers

        public List<Order> GetBuyerOrders()
        {
            var id = HttpContext.Current.User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            List<Order> orders = db.Orders.Where(o => o.BuyerID == user.UserID).Include(o => o.OrderListings).Include("OrderListings.Listing").Include(o => o.User).Include(o => o.Status).Include(o => o.Tracking).ToList();

            System.Diagnostics.Debug.WriteLine(orders);

            if (orders != null)
            {
                return orders;
            }

            return null;
        }

        #endregion Buyers
    }
}