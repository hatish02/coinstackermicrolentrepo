﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinListingApp.Controllers;
using CoinListingApp.Models;
using CoinListingApp.Services.Keywords;
using CoinListingApp.Utils;

namespace CoinListingApp.Services
{
    public enum NotificationType
    {
        Unknown = 0,

        WelcomeNewUser = 1,
        PasswordReset = 2,
        OfferNotification = 3,
        SaleNotificationSellerPaypal = 4,
        ItemDispatched = 5,
        ItemReceived = 6,
        PaymentReceived = 7,
        ChatNotification = 8,
        FavouriteSellerListing = 9,
        PaymentInstructions = 10,
        ListingUnavailable = 11,
        AccountVerified = 12,
        Listing = 13,
        EmailVerification = 1011,
        SubscriptionEnding = 2010,
        OfferReceivedNotification = 2011,
        SaleNotificationSellerEFT = 2012,
        SaleNotificationBuyerPaypal = 2013,
        SaleNotificationBuyerEFT = 2014,
        AcceptedOfferToBuyer = 3012,
        SellerVerificationProcess = 3013
    }

    public class NotificationService
    {
        private readonly Entities db = new Entities();
        private readonly KeywordResolver keywordResolver = new KeywordResolver();
        private readonly UserService userService = new UserService();

        public void SendAcceptedOfferToBuyerNotification(int buyerID, int offerId, int orderId)
        {
            User user = db.Users.Find(buyerID);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.AcceptedOfferToBuyer);

            Offer offer = db.Offers.Find(offerId);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithLink(GlobalVars.HostUrl + "/Orders/Payment/" + orderId)
                    .WithOffer(offer)
                    .WithListing(offer.Listing, this.GetListingImagePath(offer.Listing));

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendAccountNotification(int userId, string action)
        {
            User user = db.Users.Find(userId);

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.AccountVerified);

            var keywordContext = new KeywordResolverContext()
                .WithUserName(user.Name);

            var emailBody = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            var emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailBody, emailSubject);
        }

        public void SendChatNotification(int userId, ConversationReply chat)
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.ChatNotification);

            var senderUser = db.Users.Find(chat.User1);

            var keywordContext = new KeywordResolverContext()
                .WithUserName(user.Name)
                .WithSenderName(senderUser?.Name)
                .WithLink(GlobalVars.HostUrl + "/Messages/Chat/" + chat.ConversationID)
                .WithChatMessage(chat.Message)
                .WithChatTimestamp(chat.Timestamp.ToString("HH:mm - MMMM d"));

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendEmailSellerVerificationProcessNotification(User user)
        {
            var notification = db.SystemNotifications.Find((int)NotificationType.SellerVerificationProcess);

            var userManagementInAdminDashboardLink = GlobalVars.HostUrl + "/users/Details/" + user.UserID;

            var keywordContext = new KeywordResolverContext()
               .WithUserName(this.userService.MaskUsername(user))
               .WithLink(userManagementInAdminDashboardLink);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(GlobalVars.CoinstackerEmail, emailMsg, emailSubject);
        }

        public void SendEmailVerificationNotification(string emailAddress, string name, string callback)
        {
            var notification = db.SystemNotifications.Find((int)NotificationType.EmailVerification);

            var keywordContext = new KeywordResolverContext()
               .WithUserName(name)
               .WithLink(callback);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(emailAddress, emailMsg, emailSubject);
        }

        public void SendFavouriteSellerNotification(int sellerId, int listingId, string action)
        {
            var users = db.Favourites.Where(f => f.SellerID == sellerId && f.Notify == 1).Select(f => f.User1);

            var notification = db.SystemNotifications.Find((int)NotificationType.FavouriteSellerListing);

            //var sellerMaskedName = this.userService.GetMaskUsername(sellerId);
            var sellerMaskedName = this.userService.GetCoinstackerName(sellerId);
            Listing listing = db.Listings.Find(listingId);

            foreach (var user in users)
            {
                if (user == null || user.AcceptEmails == 0)
                {
                    return;
                }

                var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithSenderName(sellerMaskedName)
                    .WithListing(listing, this.GetListingImagePath(listing));

                string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
                string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

                _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
            }
        }

        public void SendListingUnavailableNotification(int sellerId, int listingId, string action)
        {
            var users = db.Favourites.Where(f => f.SellerID == sellerId && f.Notify == 1).Select(f => f.User).Distinct().ToList();

            var sellerUser = db.Users.Find(sellerId);

            users.Add(sellerUser);

            var notification = this.db.SystemNotifications.Find((int)NotificationType.ListingUnavailable);

            Listing listing = db.Listings.Find(listingId);
            foreach (var user in users.Distinct())
            {
                if (user == null || user.AcceptEmails == 0)
                {
                    return;
                }

                var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithAction(action)
                    .WithListing(listing, this.GetListingImagePath(listing));

                string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
                string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

                _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
            }
        }

        public void SendListingUpdateNotification(int userId, int listingId, string action, string reason = "")
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.Listing);

            Listing listing = db.Listings.Find(listingId);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithAction(action)
                    .WithReason(reason)
                    .WithListing(listing, this.GetListingImagePath(listing));

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendOfferNotification(int userId, int offerId, string action)
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.OfferNotification);

            Offer offer = db.Offers.Find(offerId);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithAction(action)
                    .WithOffer(offer)
                    .WithListing(offer.Listing, this.GetListingImagePath(offer.Listing));

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendOfferReceivedNotification(int sellerUserId, int offerId)
        {
            User sellerUser = db.Users.Find(sellerUserId);

            if (sellerUser == null || sellerUser.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.OfferReceivedNotification);

            Offer offer = db.Offers.Find(offerId);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(sellerUser.Name)
                    .WithOffer(offer)
                    .WithListing(offer.Listing, this.GetListingImagePath(offer.Listing));

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(sellerUser.Email, emailMsg, emailSubject);
        }

        public void SendPasswordChangeNotification(string userEmail, string callback)
        {
            User user = db.Users.Where(u => u.Email == userEmail).FirstOrDefault();

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.PasswordReset);

            var keywordContext = new KeywordResolverContext()
                     .WithUserName(user.Name)
                     .WithLink(callback);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public async Task SendPasswordChangeNotificationAsync(string userEmail, string callback)
        {
            User user = db.Users.Where(u => u.Email == userEmail).FirstOrDefault();

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.PasswordReset);

            var keywordContext = new KeywordResolverContext()
                     .WithUserName(user.Name)
                     .WithLink(callback);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            var ctrl = new EmailController();

            await ctrl.NotifyWithException(user.Email, emailMsg, emailSubject);
        }

        public void SendPaymentInstructionsNotification(int userid, int listingId)
        {
            User user = db.Users.Find(userid);

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.PaymentInstructions);

            Listing listing = db.Listings.Find(listingId);

            var paymentInstructions = this.GetPaymentInstructions(listing);

            var keywordContext = new KeywordResolverContext()
                     .WithUserName(user.Name)
                     .WithListing(listing, this.GetListingImagePath(listing))
                     .WithPaymentInstructions(paymentInstructions);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendPaymentReceivedNotification(int userid, int orderId)
        {
            User user = db.Users.Find(userid);

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.PaymentReceived);

            Order order = db.Orders.Find(orderId);

            var orderListingsText = this.GetOrderListingsText(order);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithOrder(order, orderListingsText);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendProductDispatchedNotification(int userId, int orderId, string trackingNumber)
        {
            User user = db.Users.Find(userId);

            if (user == null)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.ItemDispatched);

            Order order = db.Orders.Find(orderId);

            var sellerUser = this.GetSellerFromOrder(order);

            var orderListingsText = this.GetOrderListingsText(order);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithSenderName(sellerUser?.Name)
                    .WithTrackingNumberInfo(trackingNumber)
                    .WithOrder(order, orderListingsText);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendProductReceivedNotification(int userId, int orderId)
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.ItemReceived);

            Order order = db.Orders.Find(orderId);

            var orderListingsText = this.GetOrderListingsText(order);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    //.WithBuyerName(this.userService.GetMaskUsername(order.BuyerID))
                    .WithBuyerName(this.userService.GetCoinstackerName(order.BuyerID))
                    .WithOrder(order, orderListingsText);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendSaleNotification(int userId, int orderId, string action)
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            Order order = db.Orders.Find(orderId);
            Listing firstListing = db.Listings.Find(order.OrderListings.First().ListingId);

            var paymentInstructions = string.Empty;

            var notificationType = NotificationType.Unknown;

            if (action == "bought")
            {
                if (firstListing.PaymentType == PaymentTypeProvider.PayPalType)
                {
                    notificationType = NotificationType.SaleNotificationBuyerPaypal;
                }
                else if (firstListing.PaymentType == PaymentTypeProvider.BankTransferType)
                {
                    notificationType = NotificationType.SaleNotificationBuyerEFT;
                    paymentInstructions = this.GetPaymentInstructions(firstListing);
                }
            }
            else if (action == "sold")
            {
                if (firstListing.PaymentType == PaymentTypeProvider.PayPalType)
                {
                    notificationType = NotificationType.SaleNotificationSellerPaypal;
                }
                else if (firstListing.PaymentType == PaymentTypeProvider.BankTransferType)
                {
                    notificationType = NotificationType.SaleNotificationSellerEFT;
                }
            }

            var notification = db.SystemNotifications.Find((int)notificationType);

            var orderListingsText = this.GetOrderListingsText(order);

            var keywordContext = new KeywordResolverContext()
                    .WithUserName(user.Name)
                    .WithAction(action)
                    .WithOrder(order, orderListingsText)
                    .WithPaymentInstructions(paymentInstructions);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        public void SendSubscriptionEndingNotification(string emailAddress, string name, DateTime date)
        {
            var notification = db.SystemNotifications.Find((int)NotificationType.SubscriptionEnding);

            var keywordContext = new KeywordResolverContext()
                   .WithUserName(name)
                   .WithDate(date);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(emailAddress, emailMsg, emailSubject);
        }

        public void SendWelcomeMessageNotification(int userId)
        {
            User user = db.Users.Find(userId);

            if (user == null || user.AcceptEmails == 0)
            {
                return;
            }

            var notification = db.SystemNotifications.Find((int)NotificationType.WelcomeNewUser);

            var keywordContext = new KeywordResolverContext()
                  .WithUserName(user.Name);

            string emailMsg = this.keywordResolver.ResolveKeywords(notification.Content, keywordContext);
            string emailSubject = this.keywordResolver.ResolveKeywords(notification.Subject, keywordContext);

            _ = new EmailController().Notify(user.Email, emailMsg, emailSubject);
        }

        private string GetListingImagePath(Listing listing)
        {
            if (!string.IsNullOrEmpty(listing.CoverImage) && System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + listing.CoverImage))
            {
                //byte[] imageArray = System.IO.File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + listing.CoverImage);
                //return "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                return GlobalVars.HostUrl + listing.CoverImage;
            }

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory + "/Images/" + listing.ListingID + "/";

            if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
            {
                var firstFile = Directory.GetFiles(baseDirectory)[0];

                //byte[] imageArray = System.IO.File.ReadAllBytes(firstFile);
                //return "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);

                return GlobalVars.HostUrl + "/Images/" + listing.ListingID + "/" + Path.GetFileName(firstFile);
            }

            return string.Empty;
        }

        private string GetOrderListingsText(Order order)
        {
            var orderListingsText = new StringBuilder("<hr>");

            var listings = order.OrderListings.Select(ol => ol.Listing);

            foreach (var listing in listings)
            {
                orderListingsText.Append($"<h3>{listing.Title}</h3><img style=\"max-width: 200px;\" src=\"{ this.GetListingImagePath(listing) }\"/><p><i>{ listing.Description }</i></p><p><a href=\"{GlobalVars.HostUrl}/listings/details/{ listing.ListingID }\">Go to the listing</a></p><hr>");
            }

            return orderListingsText.ToString();
        }

        private string GetPaymentInstructions(Listing listing)
        {
            var paymentInstructions = string.Empty;

            if (listing.PaymentType == PaymentTypeProvider.PayPalType)
            {
                return paymentInstructions;
            }

            var payment = db.Payments.FirstOrDefault(p => p.UserID == listing.SellerD && p.Type == listing.PaymentType);

            if (payment is object && payment.AutoNotify.HasValue && payment.AutoNotify.Value == 1 && !string.IsNullOrEmpty(payment.Message))
            {
                var type = PaymentTypeProvider.GetPaymentTypes().FirstOrDefault(p => p.Type == listing.PaymentType)?.Message;
                paymentInstructions = $"<h3>Payment Instructions</h3><p><b>{type ?? string.Empty}</b></p><p>{payment.Message}</p>";
            }

            return paymentInstructions;
        }

        private User GetSellerFromOrder(Order order)
        {
            var sellerId = order.OrderListings.FirstOrDefault()?.Listing?.SellerD;

            if (sellerId.HasValue)
            {
                return db.Users.Find(sellerId);
            }
            return null;
        }
    }
}