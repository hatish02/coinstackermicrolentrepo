﻿using CoinListingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinListingApp.Services
{
    public class SystemSettingsService
    {
        private readonly Entities db = new Entities();
        
        public double GetSystemCreditPrice()
        {
            return db.SystemSettings.Find(1).CreditPrice;
        }

        public double GetSystemDiscount()
        {
            return db.SystemSettings.Find(1).Discount;
        }

        public bool FreeAds()
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            return ss.hasFreeAds > 0;
        }
    }
}