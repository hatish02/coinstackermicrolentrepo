﻿using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class FavouriteService
    {
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();

        public void AddFavourite(int currentUserID, int sellerId)
        {
            Favourite fav = new Favourite
            {
                SellerID = sellerId,
                UserID = currentUserID,
                Notify = 1
            };

            db.Favourites.Add(fav);
            db.SaveChanges();
        }

        public bool DeleteFavourite(int sellerId)
        {
            int currentUserID = userService.GetCurrentID();

            if (sellerId == currentUserID)
            {
                return false;
            }

            Favourite fav = db.Favourites.Where(f => f.UserID == currentUserID && f.SellerID == sellerId).FirstOrDefault();

            if (fav == null)
            {
                return false;
            }

            db.Favourites.Remove(fav);
            db.SaveChanges();

            return true;
        }

        public bool DeleteFavouriteById(int favouriteId)
        {
            Favourite fav = db.Favourites.Find(favouriteId);

            if (fav == null)
            {
                return false;
            }

            db.Favourites.Remove(fav);
            db.SaveChanges();

            return true;
        }

        public bool IsFavourite(int sellerId)
        {
            int currentUserID = userService.GetAnIdentifier();

            if (sellerId == currentUserID)
            {
                return false;
            }

            Favourite fav = db.Favourites.Where(f => f.UserID == currentUserID && f.SellerID == sellerId).FirstOrDefault();

            if (fav == null)
            {
                return false;
            }

            return true;
        }

        public void ToggleNotify(int favouriteId, bool notify)
        {
            Favourite fav = db.Favourites.Find(favouriteId);

            if (fav is null)
            {
                return;
            }

            fav.Notify = notify ? (byte)1 : (byte)0;

            db.SaveChanges();
        }
    }
}
