﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class OrderService
    {
        private static readonly int[] ClosedOrderStatuses = new int[]
        {
           8, // COMPLETED
           9, //  SHIPPED
           2004, // PAYMENT MADE
           2005, // PAYMENT RECEIVED
           2007, // CANCELLED
           2011, // PAYMENT PENDING
        };

        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly StatusService statusService = new StatusService();
        private readonly UserService userService = new UserService();

        public Order BuyNow(ListingBuyNow _list)
        {
            if (OpenOrderExists(_list.ListingID))
            {
                return null;
            }

            var id = userService.GetCurrentID();

            //BuyerID
            int buyerID = id;

            //TrackingID
            Tracking tracking = db.Trackings.Where(t => t.SellerID == _list.SellerID).FirstOrDefault();

            if (tracking == null)
            {
                return null;
            }

            //QTY
            int qty = (int)_list.ListingQTY;
            double price = _list.ListingPrice;

            //StatusID
            int statusID;

            System.Diagnostics.Debug.WriteLine(statusService.DoesStatusExist("PAYMENT MADE"));

            if (statusService.DoesStatusExist("PAYMENT MADE") == false)
            {
                statusID = statusService.CreateStatus("PAYMENT MADE").StatusID;
            }
            else
            {
                statusID = statusService.FindStatus("PAYMENT MADE").StatusID;
            }
            var listing = db.Listings.Find(_list.ListingID);
            Order order = new Order
            {
                StatusID = statusID,
                TrackingID = tracking.TrackingID,
                BuyerID = buyerID,
                QTY = qty,
                Price = _list.ListingPrice,
                CreatedDate = DateTime.Now,
                PaymentType = listing.PaymentType
            };

            db.Orders.Add(order);
            db.SaveChanges();

            //TODO: add many here... not just 1
            OrderListing orderListing = new OrderListing
            {
                OrderId = order.OrderID,
                ListingId = _list.ListingID,
                Price = _list.ListingPrice,
                Quantity = (int)_list.ListingQTY
            };

            db.OrderListings.Add(orderListing);
            db.SaveChanges();

            listing.QTY -= _list.ListingQTY;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            if (listing.QTY == 0)
            {
                // Send notification
                notificationService.SendListingUnavailableNotification(listing.SellerD, listing.ListingID, "Sold out");

                listing.isActive = 0;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
            }

            // Send sale notifications
            notificationService.SendSaleNotification(order.BuyerID, order.OrderID, "bought");
            notificationService.SendSaleNotification(listing.SellerD, order.OrderID, "sold");

            return order;
        }

        public Order BuyNowV2(ListingBuyNowV2 _list)
        {
            if (OpenOrderExists(_list.Listings.Select(l => l.Listing.ListingID).ToList()))
            {
                return null;
            }

            var id = userService.GetCurrentID();

            //BuyerID
            int buyerID = id;

            //TrackingID
            Tracking tracking = db.Trackings.Where(t => t.SellerID == _list.SellerID).FirstOrDefault();

            if (tracking == null)
            {
                return null;
            }

            //StatusID
            int statusID;

            //System.Diagnostics.Debug.WriteLine(statusService.DoesStatusExist("PAYMENT MADE"));

            //if (statusService.DoesStatusExist("PAYMENT MADE") == false)
            //{
            //    statusID = statusService.CreateStatus("PAYMENT MADE").StatusID;
            //}
            //else
            //{
            //    statusID = statusService.FindStatus("PAYMENT MADE").StatusID;
            //}

            //System.Diagnostics.Debug.WriteLine(statusService.DoesStatusExist("CREATED"));

            if (statusService.DoesStatusExist("CREATED") == false)
            {
                statusID = statusService.CreateStatus("CREATED").StatusID;
            }
            else
            {
                statusID = statusService.FindStatus("CREATED").StatusID;
            }

            var paymentType = _list.PaymentMethod.ToLower().Contains("paypal") ? 2 : 1;

            Order order = new Order
            {
                StatusID = statusID,
                TrackingID = tracking.TrackingID,
                BuyerID = buyerID,
                QTY = (int)_list.TotalQTY,
                Price = _list.TotalPrice,
                CreatedDate = DateTime.Now,
                PaymentType = (byte)paymentType,
                PostageId = _list.PostageListing.PostageID
            };

            db.Orders.Add(order);
            db.SaveChanges();

            foreach (var listingDetail in _list.Listings)
            {
                //TODO: add many here... not just 1
                OrderListing orderListing = new OrderListing
                {
                    OrderId = order.OrderID,
                    ListingId = listingDetail.Listing.ListingID,
                    Price = listingDetail.Price,
                    Quantity = (int)listingDetail.Quantity
                };

                db.OrderListings.Add(orderListing);
            }

            db.SaveChanges();

            //ONly to this when payment is confirmed
            //foreach (var listingDetail in _list.Listings)
            //{
            //    Listing listing = db.Listings.Find(listingDetail.Listing.ListingID);

            // listing.QTY -= listingDetail.Quantity;

            // if (listing.QTY == 0) { // Send notification
            // notificationService.SendListingUnavailableNotification(listing.SellerD,
            // listing.ListingID, "Sold out");

            // listing.isActive = 0; }

            //    db.Entry(listing).State = EntityState.Modified;
            //}

            //db.SaveChanges();

            return order;
        }

        public bool CancelOrder(int orderID)
        {
            Order order = db.Orders.Find(orderID);

            if (order == null)
            {
                return false;
            }

            int statusID;

            System.Diagnostics.Debug.WriteLine(statusService.DoesStatusExist("CANCELLED"));

            if (statusService.DoesStatusExist("CANCELLED") == false)
            {
                statusID = statusService.CreateStatus("CANCELLED").StatusID;
            }
            else
            {
                statusID = statusService.FindStatus("CANCELLED").StatusID;
            }

            order.StatusID = statusID;
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();

            foreach (var orderListing in order.OrderListings)
            {
                Listing listing = db.Listings.Find(orderListing.ListingId);
                listing.QTY += order.QTY;
                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
            }

            return true;
        }

        public List<OrderViewModel> GetOrdersAsBuyer(int currentUserId)
        {
            var orders = this.db.Orders
                .Include(o => o.OrderListings)
                .Include("OrderListings.Listing")
                .Include(o => o.User)
                .Include(o => o.Status)
                .Include(o => o.Tracking)
                .Where(o => o.BuyerID == currentUserId)
                .Where(o => o.Status.State != "COMPLETED")
                .Where(o => o.Status.State != "CANCELLED");

            return this.AdaptToOrderViewModels(orders, true);
        }

        public List<OrderViewModel> GetOrdersAsSeller(int currentUserId)
        {
            var orders = this.db.Orders
               .Include(o => o.OrderListings)
               .Include("OrderListings.Listing")
               .Include(o => o.User)
               .Include(o => o.Status)
               .Include(o => o.Tracking)
               .Where(o => o.OrderListings.Any(l => l.Listing.SellerD == currentUserId))
               .Where(o => o.Status.State != "COMPLETED")
               .Where(o => o.Status.State != "CANCELLED");

            return this.AdaptToOrderViewModels(orders, false);
        }

        public List<OrderViewModel> GetOrdersHistory(int currentUserId, bool asBuyer)
        {
            var orders = this.db.Orders
                .Include(o => o.OrderListings)
                .Include("OrderListings.Listing")
                .Include(o => o.User)
                .Include(o => o.Status)
                .Include(o => o.Tracking)
                .Where(o => asBuyer ? o.BuyerID == currentUserId : o.OrderListings.Any(l => l.Listing.SellerD == currentUserId))
                .Where(o => o.Status.State == "COMPLETED" || o.Status.State == "CANCELLED");

            return this.AdaptToOrderViewModels(orders, asBuyer);
        }

        public int GetSellerId(Order order)
        {
            if (order is null)
            {
                return 0;
            }

            return order.OrderListings.First().Listing.SellerD;
        }

        public bool OpenOrderExists(int listingID)
        {
            var id = userService.GetCurrentID();

            Order order = db.Orders
                .Where(o => o.OrderListings.Any(l => l.ListingId == listingID) && o.BuyerID == id)
                .Where(o => !ClosedOrderStatuses.Contains(o.StatusID)).FirstOrDefault();

            if (order != null)
            {
                return true;
            }

            return false;
        }

        public bool OpenOrderExists(List<int> listingIDs)
        {
            var id = userService.GetCurrentID();

            Order order = db.Orders
                .Where(o => o.OrderListings.All(l => listingIDs.Any(li => li == l.ListingId && o.BuyerID == id)))
                .Where(o => !ClosedOrderStatuses.Contains(o.StatusID)).FirstOrDefault();

            if (order != null)
            {
                return true;
            }

            return false;
        }

        public void SendSaleNotification(Order order, int sellerId)
        {
            notificationService.SendSaleNotification(order.BuyerID, order.OrderID, "bought");
            notificationService.SendSaleNotification(sellerId, order.OrderID, "sold");
        }

        public bool UpdateOrderStatus(int orderID, string status)
        {
            Order order = db.Orders.Find(orderID);

            if (order == null)
            {
                return false;
            }

            int statusID;

            if (statusService.DoesStatusExist(status) == false)
            {
                statusID = statusService.CreateStatus(status).StatusID;
            }
            else
            {
                statusID = statusService.FindStatus(status).StatusID;
            }

            order.StatusID = statusID;
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        internal ListingBuyNowV2 Convert(CheckoutPageV2 checkoutPageV2)
        {
            var firstListingId = checkoutPageV2.CheckoutListings.FirstOrDefault().Listing.ListingID;

            var listing = db.Listings.Single(l => l.ListingID == firstListingId);

            var postage = db.PostageListings.Single(p => p.ListingID == checkoutPageV2.SelectedPostageListing.ListingID && p.PostageID == checkoutPageV2.SelectedPostageListing.PostageID);

            var shippingPrice = postage.Price + ((checkoutPageV2.CheckoutListings.Sum(c => c.Quantity) - 1) * (checkoutPageV2.SelectedPostageListing.EachAdditionalItemPrice ?? 0));

            var listingBuyNowDetails = new List<ListingBuyNowDetail>();

            foreach (var checkoutListing in checkoutPageV2.CheckoutListings)
            {
                var current_listing = db.Listings.Single(l => l.ListingID == checkoutListing.Listing.ListingID);

                listingBuyNowDetails.Add(new ListingBuyNowDetail()
                {
                    Price = current_listing.Price,
                    Listing = current_listing,
                    Quantity = checkoutListing.Quantity
                });
            }

            ListingBuyNowV2 listingBuyNow = new ListingBuyNowV2
            {
                Address = checkoutPageV2.Address,
                PaymentMethod = checkoutPageV2.PaymentMethod,
                ShippingPrice = shippingPrice,
                PostageListing = postage,
                Listings = listingBuyNowDetails,
                TotalListingsPrice = listingBuyNowDetails.Sum(l => l.TotalPrice),
                TotalQTY = listingBuyNowDetails.Sum(l => l.Quantity),
                SellerID = listing.SellerD,
            };

            return listingBuyNow;
        }

        internal bool OrderNotPaypalPayment(Order order, int sellerID, bool alreadyDeductedQty = false)
        {
            UpdateOrderStatus(order.OrderID, "PAYMENT PENDING");

            if (!alreadyDeductedQty)
            {
                this.DeductListingsQuantity(order.OrderID);
            }

            this.SendSaleNotification(order, sellerID);

            return true;
        }

        internal bool OrderPaypalPayment(Order order, int? sellerID = null)
        {
            UpdateOrderStatus(order.OrderID, "PAYMENT RECEIVED");

            this.DeductListingsQuantity(order.OrderID);

            this.SendSaleNotification(order, sellerID ?? this.GetSellerId(order));

            return true;
        }

        private List<OrderViewModel> AdaptToOrderViewModels(IQueryable<Order> orders, bool asBuyer)
        {
            var ordersViewModel = new List<OrderViewModel>();

            foreach (var order in orders)
            {
                var sellerViewModel = new SellerViewModel();

                if (asBuyer)
                {
                    var sellerId = order.OrderListings.First().Listing.SellerD;

                    sellerViewModel.SellerId = sellerId;
                    //sellerViewModel.MaskedSellerName = this.userService.GetMaskUsername(sellerId);
                    sellerViewModel.MaskedSellerName = this.userService.GetCoinstackerName(sellerId);
                    sellerViewModel.CurrentUserRating = this.userService.GetSellerRatingFromCurrentUser(sellerId);
                }

                string paymenttype = "";

                if (order.PaymentType.HasValue)
                {
                    if (order.PaymentType.Value == PaymentTypeProvider.PayPalType)
                    {
                        paymenttype = PaymentTypeProvider.PayPalNickName;
                    }
                    else if (order.PaymentType.Value == PaymentTypeProvider.BankTransferType)
                    {
                        paymenttype = PaymentTypeProvider.BankTransferNickName;
                    }
                }

                ordersViewModel.Add(
                    new OrderViewModel
                    {
                        BuyerID = order.BuyerID,
                        //MaskedBuyerName = this.userService.GetMaskUsername(order.BuyerID),
                        MaskedBuyerName = this.userService.GetCoinstackerName(order.BuyerID),
                        BuyerFullName = this.userService.GetUserFullName(order.BuyerID),
                        BuyerShippingAddress = this.userService.GetUserShippingAddress(order.BuyerID),
                        CreatedDate = order.CreatedDate,
                        Listings = order.OrderListings.Select(ol =>
                            new CheckoutListing
                            {
                                Listing = ol.Listing,
                                Quantity = ol.Quantity,
                                Price = ol.Price
                            }).ToList(),
                        OrderID = order.OrderID,
                        Price = order.Price,
                        QTY = order.QTY,
                        Status = order.Status,
                        Tracking = order.Tracking,
                        UpdatedDate = order.UpdatedDate,
                        Seller = sellerViewModel,
                        PaymentType = paymenttype,
                        Postage = order.Postage?.Name
                        //pau = order.paymentType
                    });
            }

            return ordersViewModel;
        }

        private void DeductListingsQuantity(int orderId)
        {
            var listings = db.OrderListings.Where(ol => ol.OrderId == orderId).ToList();

            foreach (var listingDetail in listings)
            {
                Listing listing = db.Listings.Find(listingDetail.Listing.ListingID);

                listing.QTY -= listingDetail.Quantity;

                if (listing.QTY == 0)
                {
                    // Send notification
                    notificationService.SendListingUnavailableNotification(listing.SellerD, listing.ListingID, "Sold out");

                    listing.isActive = 0;
                }

                db.Entry(listing).State = EntityState.Modified;
            }

            db.SaveChanges();
        }

        private bool OrderExists(int listingID)
        {
            var id = userService.GetCurrentID();

            Order order = db.Orders.Where(o => o.OrderListings.Any(l => l.ListingId == listingID) && o.BuyerID == id).FirstOrDefault();

            if (order != null)
            {
                return true;
            }

            return false;
        }
    }
}