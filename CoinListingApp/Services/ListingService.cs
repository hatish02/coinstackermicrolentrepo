﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CoinListingApp.Models;
using CoinListingApp.Utils;
using Ether.Outcomes;
using Microsoft.AspNet.Identity;

namespace CoinListingApp.Services
{
    public class ListingService
    {
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly SystemSettingsService systemSettingsService = new SystemSettingsService();
        private readonly UserService userService = new UserService();

        private WatchingService watchingService = new WatchingService();

        public double CalculateTotalPricePostage(PostageListing postageListing, int itemsQuantity)
        {
            itemsQuantity = itemsQuantity <= 1 ? 1 : itemsQuantity;

            var result = postageListing.Price;

            if (postageListing.EachAdditionalItemPrice.HasValue)
            {
                result += postageListing.EachAdditionalItemPrice.Value * (itemsQuantity - 1); // the first item does not have additional item price
            }
            return result;
        }

        public IOutcome<Listing> CreateListing(CreateListing listing)
        {
            var price = listing.Price;

            var userID = userService.GetCurrentID();

            Listing finalListing = new Listing
            {
                SellerD = userID,
                CategoryID = listing.SubcategoryID > 0 ? listing.SubcategoryID : listing.CategoryID,
                Title = listing.Title,
                Description = listing.Description,
                QTY = listing.QTY,
                AllowOffers = listing.AllowOffers ? (byte)1 : (byte)0,
                isHighlight = 0,
                isBump = 0,
                isActive = 1,
                Price = price,
                Photos = null,
                Expires = DateTime.Now.AddDays(listing.Expires),
                DeactivatedDate = null,
                DateCreated = DateTime.Now,
                SortDate = DateTime.Now,
                LastUpdatedDate = DateTime.Now,
                PaymentType = listing.PaymentSelectedId,
                shipInFourtoEightWeek = listing.shipInFourtoEightWeek,
                shipInTwotoFourWeek = listing.shipInTwotoFourWeek, 
            };

            if (!systemSettingsService.FreeAds() && !userService.IsCurrentUserSubscribed() && !userService.GetUserFreeAds(userID))
            {
                var credits = db.Credits.Where(c => c.UserID == userID).FirstOrDefault();

                var neededCredits = (listing.Expires == 30 ? 1 : 2);

                if (credits == null || credits.Amount < neededCredits)
                {
                    return Outcomes.Failure<Listing>()
                        .WithFailureCode(FailureCode.NotEnoughCredits)
                        .WithKey(OutcomesHelper.CreditsKey, credits is null ? "0" : credits.Amount.ToString())
                        .WithKey(OutcomesHelper.NeededCreditsKey, neededCredits.ToString());
                }

                credits.Amount -= neededCredits;
                db.Entry(credits).State = EntityState.Modified;
                db.SaveChanges();
            }

            System.Diagnostics.Debug.WriteLine(finalListing);

            return Outcomes.Success(finalListing);
        }

        public List<FullListing> GenerateFullListings(List<Listing> listings)
        {
            var userWatchingListingsIds = this.watchingService.GetUserWatchingListingsIds();

            List<FullListing> fls = new List<FullListing>();

            foreach (Listing list in listings)
            {
                var isWatching = false;
                if (userWatchingListingsIds.Contains(list.ListingID))
                {
                    isWatching = true;
                }

                fls.Add(GenerateFullListing(list, isWatching));
            }

            return fls;
        }

        public List<ListingsViewsViewModel> GetAllListingsForUser()
        {
            var id = HttpContext.Current.User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            var listings = db.Listings.Include(l => l.Category).Include(l => l.User).Include(l => l.PostageListings).Where(l => l.User.Email == email && l.isArchived == 0).OrderByDescending(l => l.DateCreated);

            List<ListingsViewsViewModel> finalModel = new List<ListingsViewsViewModel>();

            foreach (Listing list in listings)
            {
                finalModel.Add(new ListingsViewsViewModel
                {
                    Listing = list,
                    Views = NoOfViews(list.ListingID)
                }
                );
            }

            return finalModel;
        }

        //new method to remove extra loops 
        public List<ListingsViewsViewModel> CustomGetAllListingsForUserWithPhotos(HttpServerUtilityBase Server)
        {
            var id = HttpContext.Current.User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            var listings = db.Listings.Include(l => l.Category).Include(l => l.User).Include(l => l.PostageListings).Where(l => l.User.Email == email && l.isArchived == 0).OrderByDescending(l => l.DateCreated);

            List<ListingsViewsViewModel> finalModel = new List<ListingsViewsViewModel>();

            foreach (Listing list in listings)
            {
                //new changes 
                if (!string.IsNullOrEmpty(list.CoverImage))
                {
                    var coverImagePath = Server.MapPath(list.CoverImage);
                    if (System.IO.File.Exists(coverImagePath))
                    {
                        list.Photos = ".." + list.CoverImage;
                    }
                }

                finalModel.Add(new ListingsViewsViewModel
                {
                    Listing = list,
                    Views = NoOfViews(list.ListingID)
                }
                );
            }

            return finalModel;
        }

        public List<string> GetListingPaymentMethod(int listingID)
        {
            List<ListingPayment> payments = db.ListingPayments.Where(lp => lp.ListingID == listingID).ToList();

            List<string> accepted = new List<string>();

            if (payments == null || payments.Count == 0)
            {
                accepted.Add("EFT");
                return accepted;
            }

            foreach (ListingPayment lp in payments)
            {
                var payment = db.Payments.Find(lp.PaymentID);

                if (payment.Type == 1)
                {
                    accepted.Add("EFT");
                }
                else if (payment.Type == 2)
                {
                    accepted.Add("Paypal");
                }
            }

            return accepted;
        }

        public string GetListingPaymentMethod_(int listingID)
        {
            var listing = db.Listings.Find(listingID);

            if (listing.PaymentType == 1)
            {
                return "EFT";
            }
            if (listing.PaymentType == 2)
            {
                return "Paypal";
            }

            return "";
        }

        public List<Listing> GetListings(List<KeyValuePair<int, int>> listingIDs)
        {
            List<Listing> listings = new List<Listing>();

            foreach (var item in listingIDs)
            {
                listings.Add(db.Listings.Find(item.Key));
            }

            return listings;
        }

        public bool HasListingExpired(int id)
        {
            Listing listing = db.Listings.Find(id);

            if (listing.isActive == 1 && listing.Expires < DateTime.Now)
            {
                notificationService.SendListingUnavailableNotification(listing.SellerD, id, "Expired");

                listing.isActive = 0;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
            }

            return listing.Expires < DateTime.Now;
        }

        public List<KeyValuePair<int, int>> HotRightNow()
        {
            //old query
            //List<Listing> listings = db.Listings.Where(l => l.isActive > 0 && l.isArchived == 0).ToList();

            //List<KeyValuePair<int, int>> listingList = new List<KeyValuePair<int, int>>();

            //foreach (Listing s in listings)
            //{
            //    int count = db.ListingViews.Where(l => l.ListingID == s.ListingID && DbFunctions.DiffDays(l.DateViewed, DateTime.Now) <= 7).Count();
            //    listingList.Add(new KeyValuePair<int, int>(s.ListingID, count));
            //}

            //listingList.Sort(CompareSellers);
            //return listingList;

            try
            {
                //new query
                var listingList = db.Listings.Where(l => l.isActive > 0 && l.isArchived == 0).Select(x => new CustomListingModel()
                {
                    ListingID = x.ListingID,
                    ListingViewCount = x.ListingViews.Where(l => DbFunctions.DiffDays(l.DateViewed, DateTime.Now) <= 7).Count()
                }).ToList()
                .Select(item => new KeyValuePair<int, int>(item.ListingID, item.ListingViewCount)).ToList();

                listingList.Sort(CompareSellers);
                return listingList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<KeyValuePair<int, int>>();
            }
        }



        public List<KeyValuePair<int, int>> HotRightNow(string category)
        {
            List<Listing> listings = db.Listings.Where(l => l.Category.CategoryName.ToLower() == category.ToLower() && l.isActive > 0 && l.isArchived == 0).ToList();

            List<KeyValuePair<int, int>> listingList = new List<KeyValuePair<int, int>>();

            foreach (Listing s in listings)
            {
                int count = db.ListingViews.Where(l => l.ListingID == s.ListingID && DbFunctions.DiffDays(l.DateViewed, DateTime.Now) <= 7).Count();

                listingList.Add(new KeyValuePair<int, int>(s.ListingID, count));
            }

            listingList.Sort(CompareSellers);

            return listingList;
        }

        public bool IsFreePostage(Listing listing)
        {
            return listing.PostageListings.Any(p => p.Price == 0);
        }

        public int NoOfViews(int id)
        {
            var listingView = db.ListingViews.Where(lv => lv.ListingID == id);

            if (listingView == null)
            {
                return -1;
            }

            return listingView.Count();
        }

        public bool RenewListing(int listingID)
        {
            Listing listing = db.Listings.Find(listingID);

            if (listing != null)
            {
                if (listing.Expires < DateTime.Now)
                {
                    Credit credits = db.Credits.Where(c => c.UserID == listing.SellerD).FirstOrDefault();
                    if (credits == null || credits.Amount <= 0)
                    {
                        credits.Amount -= 1;
                        db.Entry(credits).State = EntityState.Modified;
                        db.SaveChanges();

                        listing.Expires = listing.Expires.AddDays(30);
                        db.Entry(listing).State = EntityState.Modified;
                        db.SaveChanges();

                        return true;
                    }
                }
            }

            return false;
        }

        private static int CompareSellers(KeyValuePair<int, int> a, KeyValuePair<int, int> b)
        {
            return b.Value.CompareTo(a.Value);
        }

        private FullListing GenerateFullListing(Listing listing, bool isWatching)
        {
            FullListing fl = new FullListing
            {
                ListingID = listing.ListingID,
                SellerD = listing.SellerD,
                CategoryID = listing.CategoryID,
                Title = listing.Title,
                Description = listing.Description,
                QTY = listing.QTY,
                AllowOffers = listing.AllowOffers,
                isHighlight = listing.isHighlight,
                isBump = listing.isBump,
                Price = listing.Price,
                Expires = listing.Expires,
                Photos = listing.Photos,
                CoverImage = listing.CoverImage,
                DateCreated = listing.DateCreated,
                NoViews = NoOfViews(listing.ListingID),
                Category = listing.Category,
                User = listing.User,
                shipInFourtoEightWeek = listing.shipInFourtoEightWeek,
                shipInTwotoFourWeek = listing.shipInTwotoFourWeek,
                SellerNameMasked = listing.User.Name + " " + listing.User.Surname[0] + ".",
                WatchlistButtonViewModel = new WatchlistButtonViewModel
                {
                    ListingId = listing.ListingID,
                    Watching = isWatching
                },
                AverageRating = this.userService.GetSellerAverageRating(listing.SellerD),
                PostageListing = listing.PostageListings.FirstOrDefault(),
            };

            return fl;
        }
    }
}