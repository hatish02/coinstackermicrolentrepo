﻿namespace CoinListingApp.Services.Keywords
{
    public class KeywordResolver
    {
        public string ResolveKeywords(string message, KeywordResolverContext context)
        {
            var result = message;

            foreach (var kvp in context.KeywordsDictionary)
            {
                result = result.Replace(kvp.Key, kvp.Value);
            }

            return result;
        }
    }
}
