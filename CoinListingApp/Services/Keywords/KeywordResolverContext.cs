﻿using System;
using System.Collections.Generic;
using CoinListingApp.Models;
using CoinListingApp.Utils;

namespace CoinListingApp.Services.Keywords
{
    public class KeywordResolverContext
    {
        private const string ActionKeyword = "{Action}";
        private const string BuyerNameKeyword = "{BuyerName}";
        private const string ChatMessageKeyword = "{ChatMessage}";
        private const string ChatTimestampKeyword = "{ChatTimestamp}";
        private const string DateKeyword = "{Date}";
        private const string LinkKeyword = "{Link}";
        private const string ListingDescriptionKeyword = "{ListingDescription}";
        private const string ListingImageKeyword = "{ListingImage}";
        private const string ListingLinkKeyword = "{ListingLink}";
        private const string ListingPriceKeyword = "{ListingPrice}";
        private const string ListingQTYKeyword = "{ListingQTY}";
        private const string ListingTitleKeyword = "{ListingTitle}";
        private const string NameKeyword = "{Name}";
        private const string OfferPriceKeyword = "{OfferPrice}";
        private const string OfferQuantityKeyword = "{OfferQuantity}";
        private const string OrderListingsKeyword = "{OrderListings}";
        private const string OrderNumKeyword = "{OrderNum}";
        private const string OrderPriceKeyword = "{OrderPrice}";
        private const string OrderQuantityKeyword = "{OrderQuantity}";
        private const string PaymentInstructionsKeyword = "{PaymentInstructions}";
        private const string ReasonKeyword = "{Reason}";
        private const string SenderNameKeyword = "{SenderName}";
        private const string TrackingInfoKeyword = "{TrackingInfo}";
        private const string TrackingNumberInfoKeyword = "{TrackingNumberInfo}";
        private const string TrackingNumberKeyword = "{TrackingNumber}";

        public KeywordResolverContext()
        {
            this.KeywordsDictionary = new Dictionary<string, string>();
        }

        public Dictionary<string, string> KeywordsDictionary { get; }

        public KeywordResolverContext WithAction(string action)
        {
            this.KeywordsDictionary.Add(ActionKeyword, action);

            return this;
        }

        public KeywordResolverContext WithBuyerName(string buyerName)
        {
            this.KeywordsDictionary.Add(BuyerNameKeyword, buyerName);

            return this;
        }

        public KeywordResolverContext WithChatMessage(string chatMessage)
        {
            this.KeywordsDictionary.Add(ChatMessageKeyword, chatMessage);

            return this;
        }

        public KeywordResolverContext WithChatTimestamp(string chatTimestamp)
        {
            this.KeywordsDictionary.Add(ChatTimestampKeyword, chatTimestamp);

            return this;
        }

        public KeywordResolverContext WithDate(DateTime date)
        {
            this.KeywordsDictionary.Add(DateKeyword, date.ToString("D"));

            return this;
        }

        public KeywordResolverContext WithLink(string link)
        {
            this.KeywordsDictionary.Add(LinkKeyword, link);

            return this;
        }

        public KeywordResolverContext WithListing(Listing listing, string listingImage)
        {
            this.KeywordsDictionary.Add(ListingTitleKeyword, listing.Title);
            this.KeywordsDictionary.Add(ListingDescriptionKeyword, listing.Description);
            this.KeywordsDictionary.Add(ListingPriceKeyword, "£ " + listing.Price.ToString());
            this.KeywordsDictionary.Add(ListingQTYKeyword, listing.QTY.ToString());
            this.KeywordsDictionary.Add(ListingLinkKeyword, GlobalVars.HostUrl + "/listings/details/" + listing.ListingID);
            this.KeywordsDictionary.Add(ListingImageKeyword, listingImage);

            return this;
        }

        public KeywordResolverContext WithOffer(Offer offer)
        {
            this.KeywordsDictionary.Add(OfferQuantityKeyword, offer.QTY.ToString());
            this.KeywordsDictionary.Add(OfferPriceKeyword, "£ " + offer.Price.ToString());

            return this;
        }

        public KeywordResolverContext WithOrder(Order order, string orderListingsText)
        {
            this.KeywordsDictionary.Add(OrderNumKeyword, order.OrderID.ToString());
            this.KeywordsDictionary.Add(OrderQuantityKeyword, order.QTY.ToString());
            this.KeywordsDictionary.Add(OrderPriceKeyword, "£ " + order.Price.ToString());
            this.KeywordsDictionary.Add(OrderListingsKeyword, orderListingsText);

            return this;
        }

        public KeywordResolverContext WithPaymentInstructions(string paymentInstructions)
        {
            this.KeywordsDictionary.Add(PaymentInstructionsKeyword, paymentInstructions);

            return this;
        }

        public KeywordResolverContext WithReason(string reason)
        {
            this.KeywordsDictionary.Add(ReasonKeyword, reason);

            return this;
        }

        public KeywordResolverContext WithSenderName(string senderName)
        {
            this.KeywordsDictionary.Add(SenderNameKeyword, senderName);

            return this;
        }

        public KeywordResolverContext WithTrackingInfo(string trackingInfo, string trackingNumber)
        {
            if (!string.IsNullOrEmpty(trackingInfo))
            {
                trackingInfo = trackingInfo.Replace(TrackingNumberKeyword, trackingNumber);

                this.KeywordsDictionary.Add(TrackingInfoKeyword, $"<p>{trackingInfo}</p>");
            }

            return this;
        }

        public KeywordResolverContext WithTrackingNumberInfo(string trackingNumber)
        {
            var trackingInfo = string.Empty;

            if (!string.IsNullOrEmpty(trackingNumber))
            {
                trackingInfo = $"<p>The following tracking number has been provided: <b>{trackingNumber}</b></p>";
            }

            this.KeywordsDictionary.Add(TrackingNumberInfoKeyword, trackingInfo);

            return this;
        }

        public KeywordResolverContext WithUserName(string username)
        {
            this.KeywordsDictionary.Add(NameKeyword, username);

            return this;
        }
    }
}