﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CoinListingApp.Models;

namespace CoinListingApp.Services
{
    public class CreditService
    {
        private readonly Entities db = new Entities();
        private readonly SystemSettingsService systemSettingsService = new SystemSettingsService();
        private readonly UserService userService = new UserService();

        public bool BuyCredits(CreditPackage cp)
        {
            var id = userService.GetCurrentID();

            Credit credits = db.Credits.Find(id);

            if (credits == null)
            {
                credits = new Credit
                {
                    UserID = id,
                    Amount = cp.QTY
                };
                db.Credits.Add(credits);
                db.SaveChanges();
            }
            else
            {
                credits.Amount += cp.QTY;
                db.Entry(credits).State = EntityState.Modified;
                db.SaveChanges();
            }

            //Price
            //double creditPrice = systemSettingsService.GetSystemCreditPrice();
            //double ogPrice = cp.QTY * creditPrice;
            //double globalDiscountPrice = ogPrice - (ogPrice * (systemSettingsService.GetSystemDiscount() / 100));

            //double price = globalDiscountPrice - (globalDiscountPrice * (cp.Discount / 100));

            double price = cp.Price ?? 0 - db.SystemSettings.Find(1).Discount;

            CreditHistory creditHistory = new CreditHistory
            {
                TransactionDetail = "Credit Package Purchase: " + cp.Name,
                DateOfPurchase = DateTime.Now,
                Amount = cp.QTY,
                Price = price,
                UserID = id
            };
            db.CreditHistories.Add(creditHistory);
            db.SaveChanges();

            return true;
        }

        public bool CreateTransactionHistory(string billingid, int id)
        {
            BillingPlan bl = db.BillingPlans.Where(b => b.PlanID == billingid).FirstOrDefault();

            if (bl == null)
            {
                return false;
            }

            CreditHistory creditHistory = new CreditHistory
            {
                TransactionDetail = "Subscription Purchase: " + bl.Name,
                DateOfPurchase = DateTime.Now,
                Amount = -1,
                Price = bl.Price,
                UserID = id
            };
            db.CreditHistories.Add(creditHistory);
            db.SaveChanges();

            return true;
        }

        public bool DeductCredit(int amount = 1)
        {
            var id = userService.GetCurrentID();

            Credit credits = db.Credits.Find(id);

            credits.Amount -= amount;
            db.Entry(credits).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public IndividualCredits GetCredits()
        {
            var id = userService.GetCurrentID();

            var credits = db.Credits.Include(c => c.User).Where(c => c.UserID == id).FirstOrDefault();

            var amount = 0.0;

            if (credits != null)
            {
                amount = credits.Amount;
            }

            List<CreditHistory> creditHistory = db.CreditHistories.Where(c => c.UserID == id).ToList();

            if (creditHistory == null)
            {
                creditHistory = new List<CreditHistory>();
            }

            Subscription sub = db.Subscriptions.Where(s => s.UserID == id).FirstOrDefault();

            IndividualCredits individualCredits;

            if (sub == null)
            {
                individualCredits = new IndividualCredits
                {
                    CreditAmount = amount,
                    CreditHistory = creditHistory,
                    IsSubscribed = false
                };
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Here");

                BillingPlan bp = db.BillingPlans.Where(b => b.PlanID == sub.BillingID).FirstOrDefault();

                individualCredits = new IndividualCredits
                {
                    CreditAmount = amount,
                    CreditHistory = creditHistory,
                    IsSubscribed = true,
                    SubExpiry = sub.DateExpires,
                    SubName = bp.Name
                };
            }

            return individualCredits;
        }

        public int GetUserAvailableCredit()
        {
            var currentUserId = userService.GetCurrentID();

            var credits = db.Credits.Where(c => c.UserID == currentUserId).FirstOrDefault();

            if (credits != null)
            {
                return (int)Math.Floor(credits.Amount);
            }

            return 0;
        }
    }
}