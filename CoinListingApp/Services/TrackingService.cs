﻿using CoinListingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinListingApp.Services
{
    public class TrackingService
    {
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();
        
        public bool HasTracking()
        {
            var userid = userService.GetCurrentID();

            Tracking track = db.Trackings.Where(t => t.SellerID == userid).FirstOrDefault();

            if (track != null)
            {
                return true;
            }

            return false;
        }

        public Tracking HasTrackingEntity()
        {
            var userid = userService.GetCurrentID();

            Tracking track = db.Trackings.Where(t => t.SellerID == userid).FirstOrDefault();

            if (track != null)
            {
                return track;
            }

            return null;
        }
    }
}