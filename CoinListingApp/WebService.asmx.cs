﻿using CoinListingApp.Models;
using CoinListingApp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using File = System.IO.File;

namespace CoinListingApp
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        private readonly Entities db = new Entities();
        private readonly CreditService creditService = new CreditService();
        private readonly UserService userService = new UserService();


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getGallery(string id)
        {
            DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/images/" + id + "/"));//Assuming Test is your Folder

            if (!d.Exists)
                d.Create();

            FileInfo[] Files = d.GetFiles("*.*"); //Getting Text files

            string str = "";

            if (Files.Count() > 0)
            {
                str = "<div class='row equal-height'> ";

                foreach (FileInfo file in Files)
                {
                    if (file.Name.Contains("favourite_"))
                    {
                        str += @"<div class='col-xs-12 col-md-2'>
                            <div class='thumbnail img-thumbnail-gal'>
                                <i class='fas fa-star fa-3x star isCover'></i>
                                <img src='/images/" + id + "/" + file.Name + @"' class='img img-thumbnail' style='height:250px;' />
                                <div class='caption'>
                                    <a href='spdashboard.aspx?action=delete&file=" + HttpUtility.HtmlEncode(file.Name) + @"' class='btn btn-danger btn-block pgiDelete' style='margin-bottom:10px;'>Delete</a>
                                    <button class='btn btn-primary btn-block btnMakeCover'>Make Cover Image</button>
                                </div>
                            </div>
                        </div>";
                    }
                    else
                    {
                        str += @"<div class='col-xs-12 col-md-2'>
                            <div class='thumbnail img-thumbnail-gal'>
                                <i class='fas fa-star fa-3x star'></i>
                                <img src='/images/" + id + "/" + file.Name + @"' class='img img-thumbnail' style='height:250px;' />
                                <div class='caption'>
                                    <a href='spdashboard.aspx?action=delete&file=" + HttpUtility.HtmlEncode(file.Name) + @"' class='btn btn-danger btn-block pgiDelete' style='margin-bottom:10px;'>Delete</a>
                                    <button class='btn btn-primary btn-block btnMakeCover'>Make Cover Image</button>
                                </div>
                            </div>
                        </div>";
                    }

                }

                str += "</div>";
            }
            else
            {
                str = "<p style='color:red;'>Ooooops! Your gallery seems empty.</p>";
            }



            return str;

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string makeCover(string src, string id)
        {
            src = src.Replace("datastore/profilegallery/", "");
            //id = id.Remove(id.IndexOf("_"));

            string fn = src.Remove(0, src.LastIndexOf("/") + 1);

            //datastore/profilegallery/2092_d8e475ce-a76b-401b-af39-32b38d3ef251.jpg"
            DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/images/" + id + "/"));//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.*"); //Getting Text files

            foreach (FileInfo f in Files)
            {
                File.Move(f.FullName, f.FullName.Replace("favourite_", ""));
            }

            Files = d.GetFiles(fn.Replace("favourite_", "") + ".*");

            foreach (FileInfo f in Files)
            {
                System.IO.File.Move(f.FullName, f.FullName.Insert(f.FullName.LastIndexOf(@"\") + 1, "favourite_"));
            }

            return "";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void deleteImage(string filename, string id)
        {
            string temp = filename.Replace("spdashboard.aspx?action=delete&file=", "");

            //  string id = temp.Remove(temp.IndexOf("_"));

            string fn = Server.MapPath("~/images/" + id + "/" + temp);

            File.Delete(fn);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getUserCredits(string useremail)
        {
            var user = db.Users.Where(u => u.Email == useremail).First();

            if (user.Credit == null)
                return "0";
            else
                return user.Credit.Amount.ToString();


        }
    }
}
