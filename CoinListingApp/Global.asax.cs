using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CoinListingApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public override void Dispose()
        {
            try
            {
                //var photoDirectoryPath = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Disposing " + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Disposing Error  " + ex.Message + Environment.NewLine);
            }

            base.Dispose();
        }

        protected void Application_End()
        {
            try
            {
                var photoDirectoryPath = Server.MapPath("~/Images/");

                System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Application_End" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Application_End Error  " + ex.Message + Environment.NewLine);
            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs eventArgs)
        {
            try
            {
                var session = HttpContext.Current.Session;
                if (session != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (OnlineVisitorsContainer.Visitors.ContainsKey(session.SessionID))
                        OnlineVisitorsContainer.Visitors[session.SessionID].AuthUser = HttpContext.Current.User.Identity.Name;
                }
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Error prehandling " + ex.Message + Environment.NewLine);
            }
        }

        protected void Application_Start()
        {
            try
            {
                var photoDirectoryPath = Server.MapPath("~/Images/");

                System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Application_Start - 1" + Environment.NewLine);

                AreaRegistration.RegisterAllAreas();
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);

                //Scheduler
                Utils.Scheduler.JobScheduler jobScheduler = new Utils.Scheduler.JobScheduler();
                jobScheduler.Start();

                System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Application_Start - 2" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Application_Start Error prehandling " + ex.Message + Environment.NewLine);
            }
        }

        protected void Session_End(Object sender, EventArgs e)
        {
            try
            {
                // Code that runs when a session ends.
                // Note: The Session_End event is raised only when the sessionstate mode is set to
                // InProc in the Web.config file. If session mode is set to StateServer or
                // SQLServer, the event is not raised.

                if (this.Session != null)
                {
                    WebsiteVisitor visitor;
                    OnlineVisitorsContainer.Visitors.TryRemove(this.Session.SessionID, out visitor);

                    //var photoDirectoryPath = Server.MapPath("~/Images/");

                    //System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Session_End removing session" + Environment.NewLine);
                }
                else
                {
                    //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                    //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Session_End" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Error Session_End " + ex.Message + Environment.NewLine);
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            try
            {
                // get current context
                HttpContext currentContext = HttpContext.Current;

                if (currentContext != null)
                {
                    if (!currentContext.Request.Browser.Crawler)
                    {
                        WebsiteVisitor currentVisitor = new WebsiteVisitor(currentContext);
                        OnlineVisitorsContainer.Visitors[currentVisitor.SessionId] = currentVisitor;

                        //var photoDirectoryPath = Server.MapPath("~/Images/");

                        //System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Session_Start with visitor" + Environment.NewLine);
                    }
                }

                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Session_Start" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Error Session_Start " + ex.Message + Environment.NewLine);
            }
        }

        protected void Session_Start()
        {
            try
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " - Session_Start old school" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //var photoDirectoryPath2 = Server.MapPath("~/Images/");

                //System.IO.File.AppendAllText(photoDirectoryPath2 + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " -Error Session_Start ols school " + ex.Message + Environment.NewLine);
            }
        }
    }
}