/*! Image Uploader - v1.0.0 - 15/07/2019
 * Copyright (c) 2019 Christian Bayer; Licensed MIT */

(function ($) {

    $.fn.imageDisplayer = function (options) {

        // Default settings
        let defaults = {
            preloaded: [],
            preloadedInputName: 'preloaded',
            maxImages: 5
        };

        // Get instance
        let plugin = this;

        // Set empty settings
        plugin.settings = {};

        // Plugin constructor
        plugin.init = function () {

            // Define settings
            plugin.settings = $.extend(plugin.settings, defaults, options);

            // Run through the elements
            plugin.each(function (i, wrapper) {

                // If there are preloaded images
                if (plugin.settings.preloaded.length) {

                // Cover legend
                let $coverLegend = $('<p>', { class: 'cover-legend-displayer' }),
                    $cl = $('<i>', { class: 'material-icons', text: 'star_rate' }).appendTo($coverLegend),
                    $txt = $('<span>', { text: ' Cover image' }).appendTo($coverLegend);

                $(wrapper).append($coverLegend);

                // Create the container
                let $container = createContainer();

                // Append the container to the wrapper
                $(wrapper).append($container);


      
                    // Get the upload images container
                    let $displayedContainer = $container.find('.displayed');

                    // Set preloaded images preview
                    for (let i = 0; i < plugin.settings.preloaded.length; i++) {
                        $displayedContainer.append(createImg(plugin.settings.preloaded[i].src, plugin.settings.preloaded[i].id, true));

                        if ((i + 1) >= plugin.settings.maxImages) {
                            alert('Maximum of ' + plugin.settings.maxImages + ' images reached.');
                            return;
                        }
                    }
                }

            });

        };

        let createContainer = function () {

            // Create the image uploader container
            let $container = $('<div>', { class: 'image-displayer' }),

                // Create the uploaded images container and append it to the container
                $displayedContainer = $('<div>', { class: 'displayed' }).appendTo($container);

            return $container;
        };

        let createImg = function (src, id) {

            // Create the upladed image container
            let $container = $('<div>', { class: 'displayed-image' }),

                // Create the img tag
                $img = $('<img>', { src: src }).appendTo($container),

                // Create cover button
                $starButton = $('<span>', { class: 'star-image-displayer' }).appendTo($container),

                // Create star icon
                $t = $('<i>', { class: 'material-icons', text: 'star_rate' }).appendTo($starButton);


            // check if first image and assumed as cover
            if (!$('.displayed-image').length) {
                $starButton.addClass('current-cover-displayer');
            }

            return $container;
        };

        this.init();

        // Return the instance
        return this;
    };

    $.fn.imageUploader = function (options) {

        // Default settings
        let defaults = {
            preloaded: [],
            imagesInputName: 'images',
            preloadedInputName: 'preloaded',
            label: 'Drag & Drop files here or click to browse',
            coverIndexInputName: "coverIndex",
            maxImages: 5
        };

        // Get instance
        let plugin = this;

        // Set empty settings
        plugin.settings = {};

        // Plugin constructor
        plugin.init = function () {

            // Define settings
            plugin.settings = $.extend(plugin.settings, defaults, options);

            // Run through the elements
            plugin.each(function (i, wrapper) {

                // Cover legend
                let $coverLegend = $('<p>', { class: 'cover-legend' }),
                    $cl = $('<i>', { class: 'material-icons', text: 'star_rate' }).appendTo($coverLegend),
                    $txt = $('<span>', { text: ' Cover image'}).appendTo($coverLegend);

                $(wrapper).append($coverLegend);

                // Create the container
                let $container = createContainer();

                // Append the container to the wrapper
                $(wrapper).append($container);

                // Set some bindings
                $container.on("dragover", fileDragHover.bind($container));
                $container.on("dragleave", fileDragHover.bind($container));
                $container.on("drop", fileSelectHandler.bind($container));

                // If there are preloaded images
                if (plugin.settings.preloaded.length) {

                    // Change style
                    $container.addClass('has-files');

                    // Get the upload images container
                    let $uploadedContainer = $container.find('.uploaded');

                    // Set preloaded images preview
                    for (let i = 0; i < plugin.settings.preloaded.length; i++) {
                        $uploadedContainer.append(createImg(plugin.settings.preloaded[i].src, plugin.settings.preloaded[i].id, true));

                        if ((i + 1) >= plugin.settings.maxImages) {
                            alert('Maximum of ' + plugin.settings.maxImages + ' images reached.');
                            return;
                        }
                    }

                    $('.cover-legend').show();
                } else {
                    $('.cover-legend').hide();
                }

            });

        };

        plugin.reset = function () {
            $('.image-uploader').remove();

            plugin.each(function (i, wrapper) {

                let $container = createContainer();

                // Append the container to the wrapper
                $(wrapper).append($container);

                // Set some bindings
                $container.on("dragover", fileDragHover.bind($container));
                $container.on("dragleave", fileDragHover.bind($container));
                $container.on("drop", fileSelectHandler.bind($container));

                // If there are preloaded images
                if (plugin.settings.preloaded.length) {

                    // Change style
                    $container.addClass('has-files');

                    // Get the upload images container
                    let $uploadedContainer = $container.find('.uploaded');

                    // Set preloaded images preview
                    for (let i = 0; i < plugin.settings.preloaded.length; i++) {
                        $uploadedContainer.append(createImg(plugin.settings.preloaded[i].src, plugin.settings.preloaded[i].id, true));
                    }

                    $('.cover-legend').show();
                } else {
                    $('.cover-legend').hide();
                }
            });
        };

        let dataTransfer = new DataTransfer();

        let createContainer = function () {

            // Create the image uploader container
            let $container = $('<div>', { class: 'image-uploader' }),

                // Create the input type file and append it to the container
                $input = $('<input>', {
                    type: 'file',
                    id: plugin.settings.imagesInputName + '-' + random(),
                    name: plugin.settings.imagesInputName,
                    multiple: ''
                }).appendTo($container),

                $coverIndexHiddenInput = $('<input>', {
                    type: 'hidden',
                    id: plugin.settings.coverIndexInputName,
                    name: plugin.settings.coverIndexInputName,
                    value: '-1'
                }).appendTo($container),

                // Create the uploaded images container and append it to the container
                $uploadedContainer = $('<div>', {class: 'uploaded'}).appendTo($container),

                // Create the text container and append it to the container
                $textContainer = $('<div>', {
                    class: 'upload-text'
                }).appendTo($container),

                // Create the icon and append it to the text container
                $i = $('<i>', {class: 'material-icons', text: 'cloud_upload'}).appendTo($textContainer),

                // Create the text and append it to the text container
                $span = $('<span>', {text: plugin.settings.label}).appendTo($textContainer);


            // Listen to container click and trigger input file click
            $container.on('click', function (e) {
                // Prevent browser default event and stop propagation
                prevent(e);

                // Trigger input click
                $input.trigger('click');
            });

            // Stop propagation on input click
            $input.on("click", function (e) {
                e.stopPropagation();
            });

            // Listen to input files changed
            $input.on('change', fileSelectHandler.bind($container));

            
            return $container;
        };


        let prevent = function (e) {
            // Prevent browser default event and stop propagation
            e.preventDefault();
            e.stopPropagation();
        };

        let createImg = function (src, id) {

            // Create the upladed image container
            let $container = $('<div>', {class: 'uploaded-image'}),

                // Create the img tag
                $img = $('<img>', {src: src}).appendTo($container),

                // Create cover button
                $starButton = $('<button>', { class: 'star-image' }).appendTo($container),

                // Create star icon
                $t = $('<i>', { class: 'material-icons', text: 'star_rate' }).appendTo($starButton)

           

                // Create the delete button
                $deleteButton = $('<button>', {class: 'delete-image'}).appendTo($container),

                // Create the delete icon
                $i = $('<i>', { class: 'material-icons', text: 'clear' }).appendTo($deleteButton);
            

            // If the images are preloaded
            if (plugin.settings.preloaded.length) {

                // Set a identifier
                $container.attr('data-preloaded', true);

                // Create the preloaded input and append it to the container
                let $preloaded = $('<input>', {
                    type: 'hidden',
                    name: plugin.settings.preloadedInputName + '[]',
                    value: id
                }).appendTo($container)

            } else {

                // Set the identifier
                $container.attr('data-index', id);

            }

            // Stop propagation on click
            $container.on("click", function (e) {
                // Prevent browser default event and stop propagation
                prevent(e);
            });
          
                // Set delete action
                $deleteButton.on("click", function (e) {
                    // Prevent browser default event and stop propagation
                    prevent(e);
                    
                 

                        // Get the image index
                        let index = parseInt($container.data('index'));

                        // Update other indexes
                        $container.find('.uploaded-image[data-index]').each(function (i, cont) {
                            if (i > index) {
                                $(cont).attr('data-index', i - 1);
                            }
                        });

                        // Remove the file from input
                        dataTransfer.items.remove(index);
                    

                    // Remove this image from the container
                    $container.remove();

                    // If there is no more uploaded files
                    if (!$('.uploaded-image').length) {
                        plugin.reset();
                    }
                    //check if current cover was removed to attribute to another
                    else if (!$('.current-cover').length) {
                        $('.star-image:first').addClass('current-cover');
                        $('#' + plugin.settings.coverIndexInputName).val('0');
                    }

                });

                // Set star action
                $starButton.on("click", function (e) {

                    prevent(e);

                    $('.star-image').removeClass('current-cover');

                    $starButton.addClass('current-cover');
                    $('#' + plugin.settings.coverIndexInputName).val(id);

                });
            
            // check if first image and assumed as cover
            if (!$('.uploaded-image').length) {
                $starButton.addClass('current-cover');
                $('#' + plugin.settings.coverIndexInputName).val(id);
            }

            return $container;
        };

        let fileDragHover = function (e) {

            // Prevent browser default event and stop propagation
            prevent(e);

            // Change the container style
            if (e.type === "dragover") {
                $(this).addClass('drag-over');
            } else {
                $(this).removeClass('drag-over');
            }
        };

        let fileSelectHandler = function (e) {

            // Prevent browser default event and stop propagation
            prevent(e);

            // Get the jQuery element instance
            let $container = $(this);

            // Change the container style
            $container.removeClass('drag-over');

            // Get the files
            let files = e.target.files || e.originalEvent.dataTransfer.files;

            // Makes the upload
            setPreview($container, files);
        };

        let setPreview = function ($container, files) {

            // Add the 'has-files' class
            $container.addClass('has-files');
            $('.cover-legend').show();

            // Get the upload images container
            let $uploadedContainer = $container.find('.uploaded'),

            // Get the files input
            $input = $container.find('input[type="file"]');

            let currentUploadedImages = $('.uploaded-image');
            let currentMaxImagesAllowed = 0;

            currentMaxImagesAllowed = plugin.settings.maxImages - currentUploadedImages.length;

            // Run through the files
            $(files).each(function (i, file) {
                if (i < currentMaxImagesAllowed) {
                    // Add it to data transfer
                    dataTransfer.items.add(file);

                    // Set preview
                    $uploadedContainer.append(createImg(URL.createObjectURL(file), dataTransfer.items.length - 1));
                }
            });

            // Update input files
            $input.prop('files', dataTransfer.files);

            if ((currentUploadedImages.length + files.length) > plugin.settings.maxImages) {
                alert('Maximum of ' + plugin.settings.maxImages + ' images reached.');
            } 

        };

        // Generate a random id
        let random = function () {
            return Date.now() + Math.floor((Math.random() * 100) + 1);
        };

        this.init();

        // Return the instance
        return this;
    };

}(jQuery));