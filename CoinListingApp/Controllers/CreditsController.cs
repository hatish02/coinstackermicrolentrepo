﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    public class CreditsController : Controller
    {
        private readonly CreditService creditService = new CreditService();
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();

        public ActionResult CancelSubscription(int userID)
        {
            Subscription sub = db.Subscriptions.Find(userID);
            if (sub == null)
            {
                return HttpNotFound();
            }

            //find agreement id
            var agreementid = sub.AgreementID;
            PayPalService.CancelBillingAgreement(agreementid);

            db.Subscriptions.Remove(sub);
            db.SaveChanges();

            return View("Subscriptions", "Credits");
        }

        // GET: Credits/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Email");
            return View();
        }

        // POST: Credits/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "UserID,Amount")] Credit credit)
        {
            Credit c = db.Credits.Find(credit.UserID);

            if (c != null)
            {
                c.Amount += credit.Amount;

                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The credit amount was successfully updated";

                return RedirectToAction("PricingDashboard", "Admin");
            }

            if (ModelState.IsValid)
            {
                db.Credits.Add(credit);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The credit amount was successfully updated";

                return RedirectToAction("PricingDashboard", "Admin");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The credit amount was not updated. Please try again";

            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", credit.UserID);
            return View(credit);
        }

        // GET: Credits/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credit credit = db.Credits.Find(id);
            if (credit == null)
            {
                return HttpNotFound();
            }
            return View(credit);
        }

        // POST: Credits/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Credit credit = db.Credits.Find(id);
            db.Credits.Remove(credit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Credits/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credit credit = db.Credits.Find(id);
            if (credit == null)
            {
                return HttpNotFound();
            }
            return View(credit);
        }

        // GET: Credits/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credit credit = db.Credits.Find(id);
            if (credit == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", credit.UserID);
            return View(credit);
        }

        // POST: Credits/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "UserID,Amount")] Credit credit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(credit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", credit.UserID);
            return View(credit);
        }

        // GET: Credits
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var credits = db.Credits.Include(c => c.User);
            return View(credits.ToList());
        }

        // GET: Credits
        public ActionResult Individual()
        {
            var credits = creditService.GetCredits();
            return View(credits);
        }

        [Route("Subscriptions")]
        public ActionResult Subscriptions()
        {
            var id = userService.GetCurrentID();

            Subscription subs = db.Subscriptions.Find(id);

            return View(subs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}