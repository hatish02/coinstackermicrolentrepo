﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly CategoryService service = new CategoryService();

        // GET: Categories/Create
        public ActionResult Create()
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel();
            categoryViewModel.Category = new Category();
            this.FillParentCategories(categoryViewModel);

            return View(categoryViewModel);
        }

        // POST: Categories/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel categoryVM)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(categoryVM.Category);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The category was successfully created";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The category was not created. Please try again";

            this.FillParentCategories(categoryVM);

            return View(categoryVM);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);

            if (service.DoesCategoryHaveListings(id))
            {
                category.MarkedAsDeleted = 1;

                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                TempData["StatusDescription"] = "The category was successfully marked as deleted";
            }
            else
            {
                db.Categories.Remove(category);
                db.SaveChanges();
                TempData["StatusDescription"] = "The category was successfully deleted";
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;

            return RedirectToAction("Index");
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            CategoryViewModel categoryViewModel = new CategoryViewModel();
            categoryViewModel.Category = category;
            this.FillParentCategories(categoryViewModel);

            return View(categoryViewModel);
        }

        // POST: Categories/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryViewModel categoryVM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoryVM.Category).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The category was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The category was not created. Please try again";

            this.FillParentCategories(categoryVM);

            return View(categoryVM);
        }

        // GET: Categories
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var categories = db.Categories.Where(c => c.MarkedAsDeleted == 0);

            var parentCategories = categories.Where(c => c.ParentCategoryID == 0).OrderBy(c => c.CategoryName);
            var sortedCategories = new List<Category>();

            foreach (var parentCategory in parentCategories)
            {
                sortedCategories.Add(parentCategory);

                sortedCategories.AddRange(categories.Where(c => c.ParentCategoryID == parentCategory.CategoryID).OrderBy(c => c.CategoryName));
            }

            return View(sortedCategories);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillParentCategories(CategoryViewModel categoryVM)
        {
            // SubCategories filed is used here for parent categories!
            categoryVM.SubCategories = db.Categories.Where(c => c.ParentCategoryID == 0 && c.MarkedAsDeleted == 0 && c.CategoryID != categoryVM.Category.CategoryID).ToList();
            categoryVM.SubCategories.Insert(0, new Category { CategoryID = 0, CategoryName = "New" });
        }
    }
}