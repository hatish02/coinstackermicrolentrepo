﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Resources;
using CoinListingApp.Services;
using PayPal;
using PayPal.Api;

namespace CoinListingApp.Controllers
{
    public class CreditPackagesController : Controller
    {
        private static Random random = new Random();
        private readonly CreditService creditService = new CreditService();
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();
        private PayPal.Api.Payment payment;

        public ActionResult Buy(int? id, string Cancel = null)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditPackage cp = db.CreditPackages.Find(id);

            if (cp == null)
            {
                return HttpNotFound();
            }

            //getting the apiContext
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    var totalPrice = GetPriceWithoutDiscount(cp);
                    var price = GetItemPrice();
                    var discount = GetDiscount(cp);

                    var globalDiscount = GetGlobalDiscount();

                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/CreditPackages/Buy?id=" + id + "&";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    //var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, cp, totalPrice, price, discount);
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, cp, globalDiscount);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    Thread.Sleep(1000);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Purchase for credits failed. Please try again.";

                        System.Diagnostics.Debug.WriteLine("Failed");

                        return RedirectToAction("BuyCredits", "Users", db.CreditPackages.ToList());
                    }

                    if (creditService.BuyCredits(cp) == false)
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Purchase was successful but credits could not be updated";

                        System.Diagnostics.Debug.WriteLine("Failed");

                        return RedirectToAction("BuyCredits", "Users", db.CreditPackages.ToList());
                    }
                }
            }
            catch (PaymentsException ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for credits failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("BuyCredits", "Users", db.CreditPackages.ToList());
            }
            catch (Exception ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for credits failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("BuyCredits", "Users", db.CreditPackages.ToList());
            }
            //on successful payment, show success page to user.
            System.Diagnostics.Debug.WriteLine("Success");

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "Successfully purchased credits";

            return RedirectToAction("BuyCredits", "Users", db.CreditPackages.ToList());
        }

        // GET: CreditPackages/Create
        public ActionResult Create()
        {
            CreditPackage cp = new CreditPackage
            {
                Price = db.SystemSettings.Find(1).CreditPrice
            };
            return View(cp);
        }

        // POST: CreditPackages/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PackageID,Name,Description,QTY,Discount,Price")] CreditPackage creditPackage)
        {
            if (ModelState.IsValid)
            {
                creditPackage.Price = CreditPackageDiscountedPrice(creditPackage.Price.Value, creditPackage.Discount);

                db.CreditPackages.Add(creditPackage);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The credit package was successfully created";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The credit package was not created. Please try again";

            return View(creditPackage);
        }

        // GET: CreditPackages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditPackage creditPackage = db.CreditPackages.Find(id);
            if (creditPackage == null)
            {
                return HttpNotFound();
            }
            return View(creditPackage);
        }

        // POST: CreditPackages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CreditPackage creditPackage = db.CreditPackages.Find(id);
            db.CreditPackages.Remove(creditPackage);
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The credit package was successfully deleted";

            return RedirectToAction("Index");
        }

        // GET: CreditPackages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditPackage creditPackage = db.CreditPackages.Find(id);
            if (creditPackage == null)
            {
                return HttpNotFound();
            }

            return View(creditPackage);
        }

        // GET: CreditPackages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditPackage creditPackage = db.CreditPackages.Find(id);

            if (creditPackage == null)
            {
                return HttpNotFound();
            }
            return View(creditPackage);
        }

        // POST: CreditPackages/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PackageID,Name,Description,QTY,Discount,Price,PlanID")] CreditPackage creditPackage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(creditPackage).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The credit package was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The credit package was not updated. Please try again";

            return View(creditPackage);
        }

        // GET: CreditPackages
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }
            var packages = db.CreditPackages.ToList();

            if (!User.IsInRole("Admin"))
            {
                Subscription subscription = db.Subscriptions.Find(userService.GetCurrentID());

                if (subscription != null)
                {
                    ViewBag.Subscribed = true;
                }
                else
                {
                    ViewBag.Subscribed = false;
                }
            }

            return View(packages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private PayPal.Api.Payment CreatePayment(APIContext apiContext, string redirectUrl, CreditPackage cp, double totalPrice, double price, double discount)
        {
            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            //Adding Item Details like name, currency, price etc
            itemList.items.Add(new Item()
            {
                name = cp.Name + " Credit Package",
                currency = "GBP",
                price = price.ToString(),
                quantity = ((int)cp.QTY).ToString(),
                sku = cp.PackageID.ToString()
            });

            //Adding discount
            itemList.items.Add(new Item()
            {
                name = "Discount",
                currency = "GBP",
                price = (-discount).ToString(),
                quantity = "1",
                sku = "999"
            });

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = (totalPrice - discount).ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                total = (totalPrice - discount).ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = "example@paypal.com"
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext
            return this.payment.Create(apiContext);
        }

        private PayPal.Api.Payment CreatePayment(APIContext apiContext, string redirectUrl, CreditPackage cp, double discount)
        {
            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            //Adding Item Details like name, currency, price etc
            itemList.items.Add(new Item()
            {
                name = cp.Name + " " + cp.QTY + " Credits Package",
                currency = "GBP",
                price = cp.Price.ToString(),
                quantity = "1",
                sku = cp.PackageID.ToString()
            });

            //Adding discount
            itemList.items.Add(new Item()
            {
                name = "Discount",
                currency = "GBP",
                price = (-discount).ToString(),
                quantity = "1",
                sku = "999"
            });

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = (cp.Price - discount).ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                total = (cp.Price - discount).ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = "info@coinstacker.co.uk"
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext
            return this.payment.Create(apiContext);
        }

        private double CreditPackageDiscountedPrice(double price, double discount)
        {
            return price - (price * (discount / 100));
        }

        private PayPal.Api.Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new PayPal.Api.Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }

        private double GetDiscount(CreditPackage cp)
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            double ogPrice = cp.QTY * ss.CreditPrice;
            double globalDiscountPrice = ogPrice - (ogPrice * (ss.Discount / 100));

            double price = globalDiscountPrice - (globalDiscountPrice * (cp.Discount / 100));

            var discount = ogPrice - price;

            return discount;
        }

        private double GetGlobalDiscount()
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            return ss.Discount;
        }

        private double GetItemPrice()
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            return ss.CreditPrice;
        }

        //Paypal Helper functions
        private double GetPriceWithoutDiscount(CreditPackage cp)
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            double price = cp.QTY * ss.CreditPrice;

            return price;
        }

        private double GetPriceWithoutDiscount(double QTY)
        {
            SystemSetting ss = db.SystemSettings.Find(1);

            double price = QTY * ss.CreditPrice;

            return price;
        }
    }
}