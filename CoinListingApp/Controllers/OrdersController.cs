﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Resources;
using CoinListingApp.Services;
using PayPal;
using PayPal.Api;

namespace CoinListingApp.Controllers
{
    public class OrdersController : Controller
    {
        private static Random random = new Random();
        private readonly Entities db = new Entities();
        private readonly ListingService listingService = new ListingService();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly OrderService orderService = new OrderService();
        private readonly PaymentService paymentService = new PaymentService();
        private readonly UserService userService = new UserService();
        private PayPal.Api.Payment payment;

        public ActionResult BuyerOrders()
        {
            var orders = userService.GetBuyerOrders();
            return View(orders.ToList());
        }

        // ======================================================================================================= paypal
        public ActionResult BuyNow(int id, string Cancel = null)
        {
            //Find all the necessary information
            Models.Order order = db.Orders.Find(id);
            var sellerId = this.orderService.GetSellerId(order);

            //getting the apiContext
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                if (order is null)
                {
                    throw new ArgumentNullException();
                }

                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    Models.Payment pay = db.Payments
                        .Where(p => p.UserID == sellerId && p.Type == PaymentTypeProvider.PayPalType)
                        .FirstOrDefault();

                    if (pay == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    string email = pay.Email;

                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Listings/BuyNow?id=" + order.OrderID + "&amount=" + order.QTY + "&";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, order, email);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                        System.Diagnostics.Debug.WriteLine("Failed");

                        return RedirectToAction("Payment", "Orders", new { id });
                    }
                }
            }
            catch (PaymentsException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Details);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Request);
                System.Diagnostics.Debug.WriteLine(ex.Response);
                System.Diagnostics.Debug.WriteLine(ex.StatusCode);
                System.Diagnostics.Debug.WriteLine(ex.WebExceptionStatus);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);

                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("Payment", "Orders", new { id });
            }
            catch (Exception ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("Payment", "Orders", new { id });
            }

            //The purchase was successful -> continue

            //clean cart
            Session["Cart"] = new List<ItemCartViewModel>();

            if (orderService.UpdateOrderStatus(id, "PAYMENT RECEIVED") == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The order status was not updated";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The order status was updated";
            }

            return RedirectToAction("Index", "Orders", new { id });
        }

        [HttpPost]
        public ActionResult CancelOrder(int id)
        {
            if (orderService.CancelOrder(id) == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The order status was not updated";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The order was cancelled";
            }

            return Json("refresh");
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title");
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name");
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State");
            ViewBag.TrackingID = new SelectList(db.Trackings, "TrackingID", "Description");
            return View();
        }

        // POST: Orders/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderID,ListingID,StatusID,TrackingID,BuyerID,QTY")] Models.Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", order.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", order.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", order.StatusID);
            ViewBag.TrackingID = new SelectList(db.Trackings, "TrackingID", "Description", order.TrackingID);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Models.Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            var list = new List<Models.Order>
            {
                order
            };

            CartOrderViewModel or = new CartOrderViewModel
            {
                Orders = list,
                UserID = userService.GetCurrentID()
            };

            return View(or);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", order.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", order.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", order.StatusID);
            ViewBag.TrackingID = new SelectList(db.Trackings, "TrackingID", "Description", order.TrackingID);
            return View(order);
        }

        // POST: Orders/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,ListingID,StatusID,TrackingID,BuyerID,QTY")] Models.Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", order.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", order.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", order.StatusID);
            ViewBag.TrackingID = new SelectList(db.Trackings, "TrackingID", "Description", order.TrackingID);
            return View(order);
        }

        [HttpPost]
        public ActionResult Export(OrdersHistoryViewModel model)
        {
            if (model?.Orders is null || !model.Orders.Any())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "There are not any order specified";

                return RedirectToAction("History", new { asBuyer = model.AsBuyer });
            }

            return this.BuildCsv(model.Orders, model.AsBuyer);
        }

        [Route("orders/history/{asBuyer}/{dateFrom?}&{dateTo}")]
        [Route("orders/history/{asBuyer}/{period?}")]
        [Route("orders/history/{asBuyer}/id={period?}")]
        [Route("orders/history/{asBuyer}/{years?}")]
        [Route("orders/history/{asBuyer}/id={years?}")]
        [Route("orders/history/{asBuyer}")]
        public ActionResult History(bool asBuyer, DateTime? dateFrom, DateTime? dateTo, int? period, int? years)
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var currentUserId = userService.GetCurrentID();

            var orders = this.orderService.GetOrdersHistory(currentUserId, asBuyer).AsEnumerable();

            if (dateFrom.HasValue && dateTo.HasValue && dateFrom.Value > dateTo.Value)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The start date cannot be after the last date";
            }
            else
            {
                orders = orders.Where(o =>
                {
                    if (!o.CreatedDate.HasValue)
                    {
                        return true;
                    }

                    var result = true;

                    if (dateTo.HasValue)
                    {
                        dateTo = dateTo.Value.AddDays(1);
                        result &= o.CreatedDate <= dateTo;
                    }

                    if (dateFrom.HasValue)
                    {
                        result &= o.CreatedDate >= dateFrom;
                    }

                    return result;
                });

                if (period.HasValue)
                {
                    orders = orders.Where(o => o.CreatedDate.Value.Month == period);
                }

                if (years.HasValue)
                {
                    orders = orders.Where(o => o.CreatedDate.Value.Year == years);
                }
            }

            var ordersHistoryViewModel = new OrdersHistoryViewModel
            {
                Orders = orders.ToList(),
                AsBuyer = asBuyer
            };

            return View(ordersHistoryViewModel);
        }

        // GET: Orders
        public ActionResult Index()
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/orders" });
            }

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var currentUserId = userService.GetCurrentID();

            var viewModel = new AllOrdersViewModel
            {
                OrdersAsBuyer = this.orderService.GetOrdersAsBuyer(currentUserId),
                OrdersAsSeller = this.orderService.GetOrdersAsSeller(currentUserId),
                CurrentUserId = currentUserId
            };

            return View(viewModel);
        }

        public ActionResult NotPaypalPaymentSent(int id)
        {
            this.orderService.UpdateOrderStatus(id, "PAYMENT MADE");

            return RedirectToAction("Index");
        }

        public ActionResult Payment(int? id)
        {
            if (!id.HasValue)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Failed getting order info.";

                return RedirectToAction("Index");
            }

            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in first";

                return RedirectToAction("Login", "Account", new { returnUrl = "/Orders/Payment/" + id.Value });
            }

            Models.Order order = db.Orders.Find(id);

            if (order is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Failed getting order info.";

                return RedirectToAction("Index");
            }

            if (order.BuyerID != userService.GetCurrentID())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Failed getting order info.";

                return RedirectToAction("Index");
            }

            if (order.PaymentType == PaymentTypeProvider.BankTransferType)
            {
                Listing listing = db.Listings.Find(order.OrderListings.First().ListingId);
                Payments paymentInstructions = paymentService.RetrievePaymentInfoWithUserID(listing.SellerD);

                if (paymentInstructions == null)
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "Failed getting payment info.";

                    return RedirectToAction("Index");
                }

                paymentInstructions.OrderID = id.Value;
                paymentInstructions.TotalPrice = order.Price;

                return View(paymentInstructions);
            }
            else if (order.PaymentType == PaymentTypeProvider.PayPalType)
            {
                return this.TriggerPaypalPayment(order);
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Failed getting payment info.";

                return RedirectToAction("Index");
            }
        }

        public ActionResult PaymentReceived(int id)
        {
            if (orderService.UpdateOrderStatus(id, "PAYMENT RECEIVED") == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The order status was not updated";
            }
            else
            {
                Models.Order o = db.Orders.Find(id);

                //Send email
                notificationService.SendPaymentReceivedNotification(o.BuyerID, id);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "Thank you for your payment confirmation.";
            }

            return Json("refresh");
        }

        [HttpPost]
        public ActionResult ProductReceived(int id)
        {
            if (orderService.UpdateOrderStatus(id, "COMPLETED") == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The order status was not updated";
            }
            else
            {
                /*if (o.RatingValue != 0)
                {
                    Rating r = new Rating
                    {
                        Date = DateTime.Today,
                        RatingValue = o.RatingValue,
                        Reason = o.RatingReason,
                        UserID = o.UserID,
                        SellerID = o.Orders[0].Listing.SellerD
                    };

                    db.Ratings.Add(r);
                    db.SaveChanges();
                }   */

                //Send email
                Models.Order o = db.Orders.Find(id);
                notificationService.SendProductReceivedNotification(o.OrderListings.First().Listing.SellerD, id);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The order status was updated";
            }

            return Json("refresh");
        }

        //ProductShipped
        [HttpPost]
        public ActionResult ProductShipped(int id, string trackingNumber)
        {
            if (orderService.UpdateOrderStatus(id, "SHIPPED") == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The order status was not updated";
            }
            else
            {
                //Send email
                Models.Order o = db.Orders.Find(id);
                notificationService.SendProductDispatchedNotification(o.BuyerID, id, trackingNumber);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The order status was updated";
            }

            return Json("refresh");
        }

        [HttpPost]
        public ActionResult RateSeller(int sellerId, double rating)
        {
            this.userService.RateSeller(sellerId, rating);

            return Json("Ok");
        }

        public ActionResult ReportSeller(int? id)
        {
            if (id is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Failed getting payment info.";

                return RedirectToAction("Index");
            }

            var viewModel = new ReportSellerViewModel
            {
                SellerId = id.Value,
                //SellerMaskedName = this.userService.GetMaskUsername(id.Value)
                SellerMaskedName = this.userService.GetCoinstackerName(id.Value)
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReportSeller(ReportSellerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (this.userService.ReportSeller(model))
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.OK;
                    TempData["StatusDescription"] = "The seller was successfully reported";
                }
                else
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "Fail reporting the seller";
                }
            }
            else
            {
                return View(model);
            }

            return RedirectToAction("Index", "Orders");
        }

        public ActionResult SellerOrderDetails(int id)
        {
            var orders = db.Orders.Find(id);
            return View(orders);
        }

        public ActionResult SellerOrders()
        {
            var orders = userService.GetSellerOrders();
            return View(orders.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private FileResult BuildCsv(List<OrderViewModel> orders, bool asBuyer)
        {
            var role = "asSeller";
            var otherRoleColumn = "Buyer";

            if (asBuyer)
            {
                role = "asBuyer";
                otherRoleColumn = "Seller";
            }

            StringBuilder sb = new StringBuilder();
            string headers = $"Order ID, Order create Date, Order Price, Listings Amount, Listings Titles, Listings Quantities, Listings Prices, Order Status, Postage, Payment Type, {otherRoleColumn}\r\n";

            sb.Append(headers);

            foreach (var order in orders)
            {
                sb.Append(OrderToString(order, asBuyer));

                //Append new line character.
                sb.Append("\r\n");
            }

            string name = userService.GetCurrentUsername();
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"{name}_{role}_orders.csv");
        }

        private PayPal.Api.Payment CreatePayment(APIContext apiContext, string redirectUrl, Models.Order order, string email)
        {
            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };

            double shippingPrice = 0;

            foreach (var orderListing in order.OrderListings)
            {
                Listing list = db.Listings.Find(orderListing.ListingId);

                //Adding Item Details like name, currency, price etc
                itemList.items.Add(new Item()
                {
                    name = list.Title,
                    currency = "GBP",
                    price = order.Price.ToString(),
                    quantity = (order.QTY).ToString(),
                    sku = list.ListingID.ToString()
                });

                //TODO shipping price must be obtained through the postage chosen by the user
                if (shippingPrice == 0)
                    shippingPrice = list.PostageListings.FirstOrDefault().Price;
            }

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                shipping = shippingPrice.ToString(),
                subtotal = ((order.Price * order.QTY) + shippingPrice).ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                total = ((order.Price * order.QTY) + shippingPrice).ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = email
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext
            return this.payment.Create(apiContext);
        }

        private PayPal.Api.Payment CreatePaymentV2(APIContext apiContext, string redirectUrl, ListingBuyNowV2 listingBuyNowV2, string email)
        {
            //Listing list = db.Listings.Find(listing.ListingID);

            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };

            double shippingPrice = 0;

            foreach (var listing in listingBuyNowV2.Listings)
            {
                //Listing list = db.Listings.Find(listing.ListingID);

                //Adding Item Details like name, currency, price etc
                itemList.items.Add(new Item()
                {
                    name = listing.Listing.Title,
                    currency = "GBP",
                    price = listing.Price.ToString(),
                    quantity = (listing.Quantity).ToString(),
                    sku = listing.Listing.ListingID.ToString()
                });
            }

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                shipping = listingBuyNowV2.ShippingPrice.ToString(),
                subtotal = listingBuyNowV2.TotalListingsPrice.ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                total = listingBuyNowV2.TotalPrice.ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = email
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            PayPal.Api.Payment payment = null;

            try
            {
                payment = this.payment.Create(apiContext);
            }
            catch (Exception ex)
            {
                throw;
            }

            // Create a payment using a APIContext
            return payment;
        }

        //Paypal Helper functions
        private PayPal.Api.Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new PayPal.Api.Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }

        private string OrderToString(OrderViewModel order, bool asBuyer)
        {
            return order.OrderID
                + "," + order.CreatedDate
                + "," + order.Price
                + "," + order.QTY
                + "," + string.Join("|", order.Listings.Select(l => l.Listing.Title))
                + "," + string.Join("|", order.Listings.Select(l => l.Quantity))
                + "," + string.Join("|", order.Listings.Select(l => l.Price))
                + "," + order.Status.State
                + "," + order.Postage
                + "," + order.PaymentType
                //+ "," + order.Tracking.Description
                + "," + (asBuyer ? order.Seller.MaskedSellerName : order.MaskedBuyerName);
        }

        private ActionResult TriggerPaypalPayment(Models.Order order)
        {
            var listingBuyNowDetails = new List<ListingBuyNowDetail>();

            foreach (var orderListing in order.OrderListings)
            {
                if (listingService.HasListingExpired(orderListing.ListingId))
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "The listing has expired. You will not be able to purchase this listing";

                    return RedirectToAction("Details", "Listings", new { id = orderListing.ListingId });
                }

                listingBuyNowDetails.Add(new ListingBuyNowDetail()
                {
                    Price = orderListing.Price,
                    Listing = orderListing.Listing,
                    Quantity = orderListing.Quantity
                });
            }

            var postage = db.PostageListings.SingleOrDefault(p => p.ListingID == order.OrderListings.First().ListingId && p.PostageID == order.PostageId);

            var shippingPrice = postage.Price + (order.OrderListings.Sum(ol => ol.Quantity) * postage.EachAdditionalItemPrice ?? 0);

            var sellerId = order.OrderListings.First().Listing.SellerD;

            ListingBuyNowV2 listingBuyNowV2 = new ListingBuyNowV2
            {
                Address = userService.GetBusinessAddress(),
                PaymentMethod = listingService.GetListingPaymentMethod_(order.OrderListings.FirstOrDefault().ListingId),
                ShippingPrice = shippingPrice,
                PostageListing = postage,
                Listings = listingBuyNowDetails,
                TotalListingsPrice = listingBuyNowDetails.Sum(l => l.TotalPrice),
                TotalQTY = listingBuyNowDetails.Sum(l => l.Quantity),
                SellerID = sellerId,
            };

            //getting the apiContext
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    Models.Payment pay = db.Payments.Where(p => p.UserID == listingBuyNowV2.SellerID && p.Type == 2).FirstOrDefault();

                    if (pay == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
                    string email = pay.Email;

                    //TODO: we need to save the info into a session ? se we can use a get method to paypal callback
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Listings/BuyNow?id=" + order.OrderID + "&";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePaymentV2(apiContext, baseURI + "guid=" + guid, listingBuyNowV2, email);
                    var links
                    = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while
                    (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                        System.Diagnostics.Debug.WriteLine("Failed");

                        return RedirectToAction("Index", "Orders");
                    }

                    var updateStatus = orderService.OrderPaypalPayment(order, listingBuyNowV2.SellerID);

                    if (updateStatus == false)
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "The purchase of the listing was unsuccessful. Please try again";

                        return RedirectToAction("Index", "Orders");
                    }

                    TempData["StatusCode"] = (int)HttpStatusCode.OK;
                    TempData["StatusDescription"] = "The purchase of the listing was successful";

                    return RedirectToAction("Index", "Orders");
                }
            }
            catch (PaymentsException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Details);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Request);
                System.Diagnostics.Debug.WriteLine(ex.Response);
                System.Diagnostics.Debug.WriteLine(ex.StatusCode);
                System.Diagnostics.Debug.WriteLine(ex.WebExceptionStatus); System.Diagnostics.Debug.WriteLine(ex.StackTrace);

                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("Index", "Orders");
            }
        }
    }
}