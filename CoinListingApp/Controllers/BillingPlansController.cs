﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;
using PayPal;
using PayPal.Api;

namespace CoinListingApp.Controllers
{
    public class BillingPlansController : Controller
    {
        private readonly CreditService creditService = new CreditService();
        private readonly Entities db = new Entities();
        private readonly UserService userService = new UserService();

        public ActionResult Buy(int? id, string Cancel = null)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillingPlan bl = db.BillingPlans.Find(id);

            if (bl == null)
            {
                return HttpNotFound();
            }

            //it's a subscription
            // Make user a subscriber
            var userid = userService.GetCurrentID();
            User user = db.Users.Find(userid);

            // Determine if the user has a home address
            Models.Address address = userService.GetDeliveryAddress();

            if (address == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You need to update your billing address before you can buy a subscription";

                return RedirectToAction("UpdateProfile", "Manage");
            }

            // Determine if another subscription exists
            Models.Subscription subscription = db.Subscriptions.Find(userid);

            if (subscription != null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You already have a subscription";

                return RedirectToAction("BuyCredits", "Users", db.BillingPlans.ToList());
            }

            try
            {
                string token = Request.Params["token"];
                if (string.IsNullOrEmpty(token))
                {
                    var agreement = PayPalService.CreateBillingAgreement(
                        bl.PlanID,
                        PayPalService.CreateShippingAddress(address),
                        bl.Name,
                        bl.Description,
                        DateTime.Now.AddMinutes(1)
                    );

                    System.Diagnostics.Debug.WriteLine(agreement.id);
                    System.Diagnostics.Debug.WriteLine(agreement.links[0].href);
                    System.Diagnostics.Debug.WriteLine(agreement.GetTokenFromApprovalUrl());

                    var guid = Convert.ToString((new Random()).Next(100000));
                    var links = agreement.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    //Session.Add(bl.PlanID, agreement.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var t = Request.Params["token"];
                    var agreementID = PayPalService.ExecuteBillingAgreement(t);

                    //Add it to subscriptions
                    Subscription sub = new Subscription
                    {
                        UserID = userService.GetCurrentID(),
                        AgreementID = agreementID,
                        DateExpires = DateTime.Now.AddMonths(bl.Duration),
                        BillingID = bl.PlanID,
                        VerifyDate = DateTime.Now.AddMonths(1)
                    };

                    db.Subscriptions.Add(sub);
                    db.SaveChanges();

                    user.isSubscribed = 1;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                    //Add it to transaction history
                    creditService.CreateTransactionHistory(bl.PlanID, sub.UserID);
                }
            }
            catch (PaymentsException ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for subscription failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("BuyCredits", "Users", db.BillingPlans.ToList());
            }
            catch (Exception ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for subcription failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return RedirectToAction("BuyCredits", "Users", db.BillingPlans.ToList());
            }

            //on successful payment, show success page to user.
            System.Diagnostics.Debug.WriteLine("Success");

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "Successfully purchased subcription";

            return RedirectToAction("BuyCredits", "Users", db.BillingPlans.ToList());
        }

        // GET: BillingPlans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingPlans/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Description,Discount,Price,PlanID,Duration")] BillingPlan billingPlan)
        {
            if (ModelState.IsValid)
            {
                billingPlan.Price = BillingPlanDiscountedPrice(billingPlan.Price, billingPlan.Discount);

                // placeholder
                billingPlan.PlanID = "Placeholder";

                db.BillingPlans.Add(billingPlan);
                db.SaveChanges();

                //Create billing plan
                var plan = PayPalService.CreateBillingPlan(billingPlan.Name, billingPlan.Description, GetBaseURI(billingPlan.ID.ToString()),
                    GetBaseURI(billingPlan.ID.ToString()), "Month", 1, (decimal)billingPlan.Price, billingPlan.Duration);

                //Create and activate billing plan
                var id = PayPalService.ActivateBillingPlan(plan);

                billingPlan.PlanID = id;

                db.Entry(billingPlan).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The billing plan was successfully created";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The billing plan was not created. Please try again";

            return View(billingPlan);
        }

        // GET: BillingPlans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillingPlan billingPlan = db.BillingPlans.Find(id);
            if (billingPlan == null)
            {
                return HttpNotFound();
            }
            return View(billingPlan);
        }

        // POST: BillingPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BillingPlan billingPlan = db.BillingPlans.Find(id);

            //Create and activate billing plan
            PayPalService.DeactivateBillingPlan(billingPlan.PlanID);

            db.BillingPlans.Remove(billingPlan);
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The billing plan was successfully deleted";

            return RedirectToAction("Index");
        }

        // GET: BillingPlans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillingPlan billingPlan = db.BillingPlans.Find(id);
            if (billingPlan == null)
            {
                return HttpNotFound();
            }
            return View(billingPlan);
        }

        // GET: BillingPlans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillingPlan billingPlan = db.BillingPlans.Find(id);
            if (billingPlan == null)
            {
                return HttpNotFound();
            }
            return View(billingPlan);
        }

        // POST: BillingPlans/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Description,Discount,Price,PlanID,Duration")] BillingPlan billingPlan)
        {
            if (ModelState.IsValid)
            {
                billingPlan.Price = BillingPlanDiscountedPrice(billingPlan.Price, billingPlan.Discount);

                //PayPalService.UpdateBillingPlan(billingPlan.PlanID, billingPlan.Price.ToString(), billingPlan.Name, billingPlan.Description);

                db.Entry(billingPlan).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The billing plan was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The billing plan was not updated. Please try again";

            return View(billingPlan);
        }

        // GET: BillingPlans
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(db.BillingPlans.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private double BillingPlanDiscountedPrice(double price, double discount)
        {
            return price - (price * (discount / 100));
        }

        private string GetBaseURI(string id)
        {
            return Request.Url.Scheme + "://" + Request.Url.Authority + "/BillingPlans/Buy?id=" + id + "&";
        }
    }
}