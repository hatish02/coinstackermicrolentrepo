﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;
using CoinListingApp.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CoinListingApp.Controllers
{
    public class ManageController : Controller
    {
        private readonly CountryCodes countryCodes = new CountryCodes();
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly UserService userService = new UserService();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Manage/AddPhoneNumber
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        // GET: /Manage/ChangePassword
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        // POST: /Manage/SetPassword
        [Route("Manage/CloseAccount")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult CloseAccount()
        {
            try
            {
                var currentUserId = this.userService.GetCurrentID();

                this.userService.DeleteUser(currentUserId);
            }
            catch (Exception)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Error closing the account. Please contact us if the error persists.";

                return RedirectToAction("UpdateProfile", "Manage");
            }

            return RedirectToAction("logout", "account");
        }

        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        // GET: /Manage/Index
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            if (!userService.IsLoggedIn())
            {
                return RedirectToAction("Login", "Account", new { returnUrl = "/Manage/Index" });
            }

            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
            };

            var aspUser = db.AspNetUsers.Find(userId);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (Session["isVerifiedSeller"] is null)
            {
                Session.Add("isVerifiedSeller", user.isVerified == 1 ? true : false);
            }

            Session.Add("UserName", user.Name);

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(model);
        }

        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        // GET: /Manage/LinkLoginCallback
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        // GET: /Manage/ManageLogins
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        // GET: /Manage/SetPassword
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult SetPassword()
        {
            return View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Manage/UpdateProfile
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult UpdateProfile()
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/manage/UpdateProfile" });
            }

            var id = User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (user == null)
            {
                return HttpNotFound();
            }

            if (Session["isVerifiedSeller"] is null)
            {
                Session.Add("isVerifiedSeller", user.isVerified == 1 ? true : false);
            }

            Session.Add("UserName", user.Name);

            var home = db.Addresses.Find(user.Home_Address);
            var delivery = db.Addresses.Find(user.Delivery_Address);

            if (user.ProfilePicture != null)
            {
                ViewBag.ImageData = user.ProfilePicture;
            }


            UpdateProfileViewModel model = new UpdateProfileViewModel
            {
                Name = user.Name,
                Surname = user.Surname,
                MaskedName = this.userService.GetMaskUsername(user.UserID),
                CoinStackerName = this.userService.GetCoinstackerName(user.UserID),
                IsReceiveNewsletter = user.IsReceiveNewsletter,
                Phone = aspUser.PhoneNumber,
                Email = user.Email,
                HomeId = (home == null) ? -1 : home.AddressID,
                HomeAddressLine1 = (home == null ? "" : home.Address_line1),
                HomeAddressLine2 = (home == null ? "" : home.Address_line2),
                HomeCity = (home == null ? "" : home.City),
                HomeStateProvince = (home == null ? "" : home.State_Province),
                HomeCountry = (home == null ? "" : home.Country),
                HomePostalCode = (home == null ? "" : home.PostalCode),
                DeliveryId = (delivery == null) ? -1 : delivery.AddressID,
                DeliveryAddressLine1 = (delivery == null ? "" : delivery.Address_line1),
                DeliveryAddressLine2 = (delivery == null ? "" : delivery.Address_line2),
                DeliveryCity = (delivery == null ? "" : delivery.City),
                DeliveryStateProvince = (delivery == null ? "" : delivery.State_Province),
                DeliveryCountry = (delivery == null ? "" : delivery.Country),
                DeliveryPostalCode = (delivery == null ? "" : delivery.PostalCode),
                ProfilePicture = null,
                Countries = countryCodes.countries
            };

            if (home is object && delivery is object)
            {
                if (home.Address_line1 == delivery.Address_line1 &&
                home.Address_line2 == delivery.Address_line2 &&
                home.City == delivery.City &&
                home.Country == delivery.Country &&
                home.PostalCode == delivery.PostalCode &&
                home.State_Province == delivery.State_Province)
                {
                    model.SameAsHomeAddress = true;
                }
            }

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(model);
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult UpdateProfile(UpdateProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var id = User.Identity.GetUserId();

                var aspUser = db.AspNetUsers.Find(id);
                var email = aspUser.Email;

                User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

                if (user == null)
                {
                    return HttpNotFound();
                }

                db.Users.Attach(user);
                db.Entry(user).Reload();

                // Handle HomeAddress Delete a home address
                if (model.HomeId > -1 && (model.HomeAddressLine1 == null || model.HomeAddressLine1 == ""))
                {
                    Address hAddress = db.Addresses.Find(model.HomeId);

                    db.Addresses.Attach(hAddress);
                    db.Entry(hAddress).Reload();

                    if (hAddress != null)
                    {
                        var result = db.Addresses.Remove(hAddress);
                        db.SaveChanges();
                        db.Entry(hAddress).State = EntityState.Detached;
                        user.Home_Address = null;
                    }
                }
                // Create a new home address
                else if (model.HomeId == -1 && (model.HomeAddressLine1 != null && model.HomeAddressLine1 != ""))
                {
                    Address homeAddress = new Address
                    {
                        Address_line1 = model.HomeAddressLine1,
                        Address_line2 = model.HomeAddressLine2,
                        City = model.HomeCity,
                        State_Province = model.HomeStateProvince,
                        Country = model.HomeCountry,
                        PostalCode = model.HomePostalCode
                    };

                    if (ModelState.IsValid)
                    {
                        var homeAddressResults = db.Addresses.Add(homeAddress);
                        db.SaveChanges();
                        System.Diagnostics.Debug.WriteLine("Home Address Creation Results: " + homeAddressResults);
                        user.Home_Address = homeAddressResults.AddressID;
                    }
                }
                // Update an existing home address
                else if (model.HomeId > -1 && (model.HomeAddressLine1 != null && model.HomeAddressLine1 != ""))
                {
                    Address hAddress = db.Addresses.Find(model.HomeId);

                    db.Addresses.Attach(hAddress);
                    db.Entry(hAddress).Reload();

                    if (hAddress != null)
                    {
                        hAddress.Address_line1 = model.HomeAddressLine1;
                        hAddress.Address_line2 = model.HomeAddressLine2;
                        hAddress.City = model.HomeCity;
                        hAddress.State_Province = model.HomeStateProvince;
                        hAddress.Country = model.HomeCountry;
                        hAddress.PostalCode = model.HomePostalCode;

                        System.Diagnostics.Debug.WriteLine("Update Home Address: " + hAddress);

                        db.Entry(hAddress).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(hAddress).State = EntityState.Detached;
                        user.Home_Address = hAddress.AddressID;
                    }
                }

                if (model.SameAsHomeAddress)
                {
                    model.DeliveryAddressLine1 = model.HomeAddressLine1;
                    model.DeliveryAddressLine2 = model.HomeAddressLine2;
                    model.DeliveryCity = model.HomeCity;
                    model.DeliveryCountry = model.HomeCountry;
                    model.DeliveryPostalCode = model.HomePostalCode;
                    model.DeliveryStateProvince = model.HomeStateProvince;
                }

                // Handle DeliveryAddress Delete existing delivery address
                if (model.DeliveryId > -1 && (model.DeliveryAddressLine1 == null || model.DeliveryAddressLine1 == ""))
                {
                    Address dAddress = db.Addresses.Find(model.DeliveryId);

                    if (dAddress != null)
                    {
                        user.Delivery_Address = null;
                        db.SaveChanges();

                        db.Addresses.Attach(dAddress);
                        db.Entry(dAddress).Reload();

                        db.Addresses.Remove(dAddress);
                        db.SaveChanges();
                        db.Entry(dAddress).State = EntityState.Detached;
                    }
                }
                // Create a new delivery address
                else if (model.DeliveryId == -1 && (model.DeliveryAddressLine1 != null && model.DeliveryAddressLine1 != ""))
                {
                    Address deliveryAddress = new Address
                    {
                        Address_line1 = model.DeliveryAddressLine1,
                        Address_line2 = model.DeliveryAddressLine2,
                        City = model.DeliveryCity,
                        State_Province = model.DeliveryStateProvince,
                        Country = model.DeliveryCountry,
                        PostalCode = model.DeliveryPostalCode
                    };

                    if (ModelState.IsValid)
                    {
                        var deliveryAddressResults = db.Addresses.Add(deliveryAddress);
                        db.SaveChanges();
                        System.Diagnostics.Debug.WriteLine("Delivery Address Creation Results: " + deliveryAddressResults);
                        user.Delivery_Address = deliveryAddressResults.AddressID;
                    }
                }
                // Update an existing delivery address
                else if (model.DeliveryId > -1 && (model.DeliveryAddressLine1 != null && model.DeliveryAddressLine1 != ""))
                {
                    Address dAddress = db.Addresses.Find(model.DeliveryId);

                    db.Addresses.Attach(dAddress);
                    db.Entry(dAddress).Reload();

                    if (dAddress != null)
                    {
                        dAddress.Address_line1 = model.DeliveryAddressLine1;
                        dAddress.Address_line2 = model.DeliveryAddressLine2;
                        dAddress.City = model.DeliveryCity;
                        dAddress.State_Province = model.DeliveryStateProvince;
                        dAddress.Country = model.DeliveryCountry;
                        dAddress.PostalCode = model.DeliveryPostalCode;

                        System.Diagnostics.Debug.WriteLine("Update Delivery Address: " + dAddress);

                        db.Entry(dAddress).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(dAddress).State = EntityState.Detached;
                        user.Delivery_Address = dAddress.AddressID;
                    }
                }

                // Update the name and surname
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.IsReceiveNewsletter = model.IsReceiveNewsletter;
                user.CoinStackerName = model.CoinStackerName;

                // Update the profilePicture
                if (model.ProfilePicture != null)
                {
                    MemoryStream target = new MemoryStream();
                    model.ProfilePicture.InputStream.CopyTo(target);
                    byte[] data = target.ToArray();

                    var imgDataURL = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(data));
                    user.ProfilePicture = imgDataURL;
                }

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(user).State = EntityState.Detached;

                // Update phone number
                if (model.Phone != null || model.Phone != "")
                {
                    aspUser.PhoneNumber = model.Phone;
                    db.Entry(aspUser).State = EntityState.Modified;
                    db.SaveChanges();
                }

                // Send message
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The user profile was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The user profile was not updated. Please try again";

            return View();
        }

        public ActionResult updateIsReceiveNewsletter(bool IsReceiveNewsletter)
        {
            var id = User.Identity.GetUserId();
            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();
            if (user == null)
            {
                return HttpNotFound();
            }
            user.IsReceiveNewsletter = IsReceiveNewsletter;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            db.Entry(user).State = EntityState.Detached;
            return View();
        }

        // GET: Home
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult UploadFiles()
        {
            var id = User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.isVerified == 1)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You are already verified";

                return RedirectToAction("Index");
            }

            if (user.VerficationStarted == 1)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You have already submitted your documents";

                return RedirectToAction("Index");
            }

            ViewBag.isVerified = user.isVerified;
            ViewBag.verificationStarted = user.VerficationStarted;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public ActionResult UploadFiles(SellerVerifyViewModel files)
        {
            var id = User.Identity.GetUserId();

            var aspUser = db.AspNetUsers.Find(id);
            var email = aspUser.Email;

            User user = db.Users.Where(r => r.Email == email).FirstOrDefault();

            if (user == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                if (files.ID.ContentLength > 0 || files.PoR.ContentLength > 0)
                {
                    try
                    {
                        string[] acceptedExtensions = { ".jpg", ".JPG", ".png", ".PNG", ".jpeg", ".JPEG", ".pdf", ".PDF" };

                        //Get ID info
                        var IDfileName = files.ID.FileName;
                        var IDextension = Path.GetExtension(files.ID.FileName);
                        var IDsize = files.ID.ContentLength;

                        MemoryStream IDtarget = new MemoryStream();
                        files.ID.InputStream.CopyTo(IDtarget);
                        byte[] data = IDtarget.ToArray();

                        //Get PoR info
                        var PoRfileName = files.PoR.FileName;
                        var PoRextension = Path.GetExtension(files.PoR.FileName);
                        var PoRsize = files.PoR.ContentLength;

                        MemoryStream PoRtarget = new MemoryStream();
                        files.PoR.InputStream.CopyTo(PoRtarget);
                        byte[] PoRdata = PoRtarget.ToArray();

                        if (Array.Exists(acceptedExtensions, ext => ext == IDextension) && Array.Exists(acceptedExtensions, ext => ext == PoRextension))
                        {
                            if (user.ProofOfResidence != null)
                            {
                                var file = db.Files.Find(user.ProofOfResidence);

                                db.Files.Remove(file);
                            }

                            if (user.CopyOfID != null)
                            {
                                var file = db.Files.Find(user.CopyOfID);

                                db.Files.Remove(file);
                            }

                            Models.File IDfileToUpload = new Models.File
                            {
                                FileName = IDfileName,
                                FileType = IDextension.Substring(1, IDextension.Length - 1),
                                FileSize = IDsize,
                                FileContent = data
                            };

                            db.Files.Add(IDfileToUpload);
                            db.SaveChanges();

                            Models.File PoRfileToUpload = new Models.File
                            {
                                FileName = PoRfileName,
                                FileType = PoRextension.Substring(1, PoRextension.Length - 1),
                                FileSize = PoRsize,
                                FileContent = PoRdata
                            };

                            db.Files.Add(PoRfileToUpload);
                            db.SaveChanges();

                            user.VerficationStarted = 1;
                            user.CopyOfID = IDfileToUpload.ID;
                            user.ProofOfResidence = PoRfileToUpload.ID;
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();

                            TempData["StatusCode"] = (int)HttpStatusCode.OK;
                            TempData["StatusDescription"] = "You have successfully uploaded your verification documents";

                            this.notificationService.SendEmailSellerVerificationProcessNotification(user);

                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                            TempData["StatusDescription"] = "Only JPEG, JPG, PNG, and PDF files are supported";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Something went wrong. Please try again";
                    }
                }
                else
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "Please select 5 or fewer supported documents";
                }
            }

            ViewBag.isVerified = user.isVerified;
            ViewBag.verificationStarted = user.VerficationStarted;

            return View();
        }

        // GET: /Manage/VerifyPhoneNumber
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller,Buyer")]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }



        #endregion Helpers
    }



}