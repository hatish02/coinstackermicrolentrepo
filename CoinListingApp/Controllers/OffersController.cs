﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    public class OffersController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly OfferService offerService = new OfferService();

        public ActionResult Accept(int id)
        {
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }

            var orderId = offerService.AcceptOffer(id);

            if (!orderId.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The offer was successfully accepted";

            //Send email
            if (offer.BuyerUpdate == 0)
            {
                notificationService.SendOfferNotification(offer.Listing.SellerD, id, "Accepted");
                // take buyer to the complete order
                return RedirectToAction("Payment", "Orders", new { id = orderId.Value });
            }

            if (offer.SellerUpdate == 0)
            {
                notificationService.SendAcceptedOfferToBuyerNotification(offer.BuyerID, id, orderId.Value);
            }

            return Json("refresh");
        }

        public ActionResult Counter(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }
            return View(offer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Counter(int OfferID, int QTY, double Price)
        {
            if (!offerService.CounterOffer(OfferID, QTY, Price))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The offer was successfully countered";

            //Send email
            Offer offer = db.Offers.Find(OfferID);
            if (offer.BuyerUpdate == 1)
            {
                notificationService.SendOfferNotification(offer.Listing.SellerD, OfferID, "Countered");
            }
            else
            {
                notificationService.SendOfferNotification(offer.BuyerID, OfferID, "Countered");
            }

            return RedirectToAction("Index");
        }

        // GET: Offers/Create
        public ActionResult Create()
        {
            ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title");
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name");
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State");
            return View();
        }

        // POST: Offers/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OfferID,ListingID,StatusID,BuyerID,QTY,Price,Counters,SellerUpdate,BuyerUpdate")] Offer offer)
        {
            if (ModelState.IsValid)
            {
                db.Offers.Add(offer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", offer.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", offer.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", offer.StatusID);
            return View(offer);
        }

        public ActionResult Decline(int id)
        {
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }

            if (!offerService.DeclineOffer(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The offer was successfully declined";

            //Send email
            notificationService.SendOfferNotification(offer.BuyerID, id, "Declined");

            return Json("refresh");
        }

        // GET: Offers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }
            return View(offer);
        }

        // POST: Offers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Offer offer = db.Offers.Find(id);
            db.Offers.Remove(offer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Offers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }
            return View(offer);
        }

        // GET: Offers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }
            ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", offer.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", offer.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", offer.StatusID);
            return View(offer);
        }

        // POST: Offers/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OfferID,ListingID,StatusID,BuyerID,QTY,Price,Counters,SellerUpdate,BuyerUpdate")] Offer offer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(offer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ListingID = new SelectList(db.Listings, "ListingID", "Title", offer.ListingID);
            ViewBag.BuyerID = new SelectList(db.Users, "UserID", "Name", offer.BuyerID);
            ViewBag.StatusID = new SelectList(db.Status, "StatusID", "State", offer.StatusID);
            return View(offer);
        }

        // GET: Offers
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(this.offerService.GetOffers());
        }

        public ActionResult Order(int id)
        {
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }

            Order order = db.Orders.Where(o => o.OrderListings.Any(l => l.ListingId == offer.ListingID)).FirstOrDefault();
            if (order == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Details", "Orders", new { id = order.OrderID });
        }

        public ActionResult Withdraw(int id)
        {
            Offer offer = db.Offers.Find(id);
            if (offer == null)
            {
                return HttpNotFound();
            }

            if (!offerService.WithdrawOffer(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The offer was successfully withdrawn";

            //Send email

            notificationService.SendOfferNotification(offer.Listing.SellerD, id, "Withdrawn");

            return Json("refresh");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}