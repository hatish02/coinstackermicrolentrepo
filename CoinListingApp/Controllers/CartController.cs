﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    public class AddListingToCartResponse
    {
        public AddListingToCartResponse(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public bool CartIsEmpty { get; set; } = false;
        public string Message { get; set; }
        public bool Success { get; }
    }

    public class CartController : Controller
    {
        private readonly Entities db = new Entities();

        private readonly NotificationService notificationService = new NotificationService();
        private readonly OrderService orderService = new OrderService();
        private readonly PaymentService paymentService = new PaymentService();
        private readonly UserService userService = new UserService();

        [HttpPost]
        public ActionResult Add(ItemCartViewModel itemCartViewModel)
        {
            Listing listing = db.Listings.Find(itemCartViewModel.IdListing);

            var cart = Session["Cart"] as List<ItemCartViewModel>;

            if (cart is null)
            {
                cart = new List<ItemCartViewModel>();
            }
            else
            {
                if (cart.Any())
                {
                    var listingInCart = db.Listings.Find(cart.FirstOrDefault().IdListing);
                    if (listingInCart.SellerD != listing.SellerD)
                    {
                        return Json(new AddListingToCartResponse(false, "Error! You can’t add items to your Cart from different Sellers. Please complete your current order first before making a new order."));
                    }
                }
            }

            //check stock before add
            var existingCartListing = cart.SingleOrDefault(x => x.IdListing == itemCartViewModel.IdListing);

            if (existingCartListing != null)
            {
                var qtyFinal = existingCartListing.Qty + itemCartViewModel.Qty;

                if (listing.QTY - qtyFinal < 0)
                {
                    return Json(new AddListingToCartResponse(false, "Error! The seller doesn't have enough quantity available!"));
                }
                else
                {
                    existingCartListing.Qty = qtyFinal;

                    //cart.Remove(cart.SingleOrDefault(x => x.IdListing == itemCartViewModel.IdListing));
                    //cart.Add(existingCartListing);
                }
            }
            else
            {
                cart.Add(itemCartViewModel);
            }

            Session["Cart"] = cart;

            return Json(new AddListingToCartResponse(true, "Items in your cart can still be purchased by other Buyers.\n\tYou must complete the Checkout process to secure this item."));
        }

        // GET: Orders
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var id = userService.GetCurrentID();

            var orders = db.Orders.Include(o => o.OrderListings).Include("OrderListings.Listing").Include(o => o.User).Include(o => o.Status).Include(o => o.Tracking).Where(o => o.BuyerID == id || o.OrderListings.Any(l => l.Listing.SellerD == id))
                .Where(o => o.Status.State != "COMPLETED").Where(o => o.Status.State != "CANCELLED");

            CartOrderViewModel or = new CartOrderViewModel
            {
                Orders = orders.ToList(),
                UserID = id
            };

            return View(or);
        }

        [HttpPost]
        public ActionResult Remove(int IdListing)
        {
            var cart = Session["Cart"] as List<ItemCartViewModel>;

            if (cart is null)
            {
                cart = new List<ItemCartViewModel>();
                Session["Cart"] = cart;

                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Cart was empty";

                return Json(new AddListingToCartResponse(false, "Cart was empty!"));
            }
            else
            {
                cart.Remove(cart.SingleOrDefault(c => c.IdListing == IdListing));
                Session["Cart"] = cart;

                var response = new AddListingToCartResponse(true, "Listing removed!");

                if (!cart.Any())
                {
                    response.CartIsEmpty = true;
                }

                return Json(response);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class ItemCartViewModel
    {
        public int IdListing { get; set; }
        public int Qty { get; set; }
    }
}