﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;

namespace CoinListingApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PostagesController : Controller
    {
        private Entities db = new Entities();

        // GET: Postages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Postages/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PostageID,Name,Description")] Postage postage)
        {
            if (ModelState.IsValid)
            {
                postage.markedAsDeleted = 0;
                db.Postages.Add(postage);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The postage was successfully created";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The postage was not created. Please try again";

            return View(postage);
        }

        // GET: Postages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postage postage = db.Postages.Find(id);
            if (postage == null)
            {
                return HttpNotFound();
            }
            return View(postage);
        }

        // POST: Postages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Postage postage = db.Postages.Find(id);
            //db.Postages.Remove(postage);
            postage.markedAsDeleted = 1;
            db.Entry(postage).State = EntityState.Modified;
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The postage was successfully deleted";

            return RedirectToAction("Index");
        }

        // GET: Postages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postage postage = db.Postages.Find(id);
            if (postage == null)
            {
                return HttpNotFound();
            }
            return View(postage);
        }

        // GET: Postages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postage postage = db.Postages.Find(id);
            if (postage == null)
            {
                return HttpNotFound();
            }
            return View(postage);
        }

        // POST: Postages/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PostageID,Name,Description,markedAsDeleted")] Postage postage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postage).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The postage was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The postage was not updated. Please try again";

            return View(postage);
        }

        // GET: Postages
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(db.Postages.Where(p => p.markedAsDeleted == 0).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}