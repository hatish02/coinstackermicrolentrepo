﻿using System.Linq;
using System.Web.Mvc;
using CoinListingApp.Services;
using PagedList;

namespace CoinListingApp.Controllers
{
    public class SellersAZController : Controller
    {
        private readonly FavouriteService favouriteService = new FavouriteService();
        private readonly SellerService sellerService = new SellerService();
        private readonly UserService userService = new UserService();

        // GET: SellersAZ
        [Route("sellersAZ")]
        [Route("sellersAZ/Index")]
        public ActionResult Index(string sort, string search, bool? newSearch, int? Page_No)
        {
            var sellersViewModel = this.sellerService.CreateSellersViewModel(true);

            //Search
            if (newSearch.HasValue && newSearch.Value)
            {
                Page_No = 1;
            }

            if (!string.IsNullOrEmpty(search))
            {
                sellersViewModel = sellersViewModel.Where(s =>
                s.Seller.Name.ToLower().Contains(search.ToLower())
                || s.Seller.Surname.ToLower().Contains(search.ToLower())
                || s.MaskedSellerName.ToLower().Contains(search.ToLower())
                || s.CityAndCountry.ToLower().Contains(search.ToLower())
                || s.FormattedSignUpDate.ToLower().Contains(search.ToLower()));
            }

            ViewBag.filter = search;

            //Sort
            if (string.IsNullOrEmpty(sort))
            {
                sort = "AZ";
            }

            switch (sort)
            {
                case "AZ":
                    sellersViewModel = sellersViewModel.OrderBy(s => s.MaskedSellerName);
                    break;

                case "ZA":
                    sellersViewModel = sellersViewModel.OrderByDescending(s => s.MaskedSellerName);
                    break;

                case "Newest":
                    sellersViewModel = sellersViewModel.OrderByDescending(s => s.Seller.SignUpDate).ThenBy(s => s.MaskedSellerName);
                    break;

                case "Oldest":
                    sellersViewModel = sellersViewModel.OrderBy(s => s.Seller.SignUpDate).ThenBy(s => s.MaskedSellerName);
                    break;

                case "Highest":
                    sellersViewModel = sellersViewModel.OrderByDescending(s => s.CurrentUserRating).ThenBy(s => s.MaskedSellerName);
                    break;

                case "Lowest":
                    sellersViewModel = sellersViewModel.OrderBy(s => s.CurrentUserRating).ThenBy(s => s.MaskedSellerName);
                    break;

                default:
                    break;
            }

            ViewBag.CurrentSortOrder = sort;

            int Size_Of_Page = 10;
            int No_Of_Page = (Page_No ?? 1);

            ViewBag.CurrentAction = "Index";

            return View(sellersViewModel.ToPagedList(No_Of_Page, Size_Of_Page));
        }
    }
}