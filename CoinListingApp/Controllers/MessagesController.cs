﻿using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    public class MessagesController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly MessageService messageService = new MessageService();
        private readonly UserService userService = new UserService();

        [Authorize]
        public ActionResult Chat(int id)
        {
            var messages = messageService.GetChat(id);

            if (messages == null)
            {
                return RedirectToAction("Index");
            }

            return View(messages);
        }

        // GET: Messages
        public ActionResult Index()
        {
            return View(this.messageService.GetMessagesIndexViewModel());
        }

        [HttpPost]
        [Authorize]
        public ActionResult SendMessage(SendMessageViewModel sendMessageViewModel)
        {
            var result = this.messageService.SendMessage(sendMessageViewModel);

            if (result)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "Message sent to seller.";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Error sending message to the seller.";
            }

            //return PartialView("_SendMessageChat", sendMessageViewModel);
            return Json("refresh");
        }

        // GET
        public ActionResult SendMessageChatPartial(int receiverUserId)
        {
            var sendMessageViewModel = new SendMessageViewModel
            {
                ReceiverUserId = receiverUserId,
                Message = string.Empty
            };

            return PartialView("_SendMessageChat", sendMessageViewModel);
        }

        public ActionResult StartNewChat(int receiverUserId)
        {
            var conversationId = this.messageService.StartNewChat(receiverUserId);

            return RedirectToAction("Chat", new { id = conversationId });
        }
    }
}