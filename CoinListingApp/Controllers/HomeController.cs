﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CoinListingApp.Models;

namespace CoinListingApp.Controllers
{
    public class HomeController : Controller
    {
        private Entities db = new Entities();

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult HowItWorks()
        {
            return View();
        }

        public ActionResult Index(string search)
        {
            HomeViewModel homeModel = new HomeViewModel();
            homeModel.MessageOfTheDay = db.SystemSettings.Find(1).MessageOfTheDay;
            homeModel.Categories = new List<Category>(db.Categories.ToList());

            if (!string.IsNullOrEmpty(search))
            {
                return RedirectToAction("Index", "Listings", new { search = search });
            }

            return View(homeModel);
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult Safety()
        {
            return View();
        }

        public ActionResult Shipping()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }
    }
}
