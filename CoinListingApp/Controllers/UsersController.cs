﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CoinListingApp.Models;
using CoinListingApp.Services;
using CoinListingApp.Services.Validation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;

namespace CoinListingApp.Controllers
{
    public class ToggleFavouriteNotifyViewModel
    {
        public int FavouriteId { get; set; }
        public bool Notify { get; set; }
    }

    public class UsersController : Controller
    {
        private const int BumpCreditCost = 2;
        private const int HighlightCreditCost = 2;
        private const int MaxPhotosAllowed = 5;
        private const string WatchlistRoutePath = "users/watchlist";

        private readonly CreditService creditService = new CreditService();
        private readonly Entities db = new Entities();
        private readonly FavouriteService favouriteService = new FavouriteService();
        private readonly ListingService listingService = new ListingService();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly PaymentService paymentService = new PaymentService();
        private readonly TrackingService trackingService = new TrackingService();
        private readonly UserService userService = new UserService();
        private readonly WatchingService watchingService = new WatchingService();

        public ActionResult ActionEmails(byte allowEmails)
        {
            var userid = userService.GetCurrentID();

            User user = db.Users.Find(userid);

            user.AcceptEmails = allowEmails;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", "Manage");
        }

        public ActionResult Activate(int id)
        {
            User user = db.Users.Find(id);

            user.isActive = 1;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            //Send notification to account
            notificationService.SendAccountNotification(id, "Activated");

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult ActivateListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }

            listing.isActive = 1;
            listing.isArchived = 0;
            listing.ActiveDays = 0;
            listing.DeactivatedDate = null;
            listing.LastUpdatedDate = DateTime.Now;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully activated";

            // Send email
            notificationService.SendListingUpdateNotification(listing.SellerD, id.Value, "Activated");

            return Json("refresh");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult Bump(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            if (userService.GetCurrentCredit() >= BumpCreditCost)
            {
                listing.isBump = 1;
                listing.BumpExpires = DateTime.Now.AddDays(1);
                listing.LastUpdatedDate = DateTime.Now;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();

                creditService.DeductCredit(BumpCreditCost);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The listing was successfully bumped";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.PreconditionFailed;
                TempData["StatusDescription"] = "You do not have enough credits to bump this listing. Please buy more credits";
            }

            return Json("refresh");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult BuyCredits()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            Packages packages = new Packages
            {
                CreditPackages = db.CreditPackages.ToList(),
                BillingPlans = db.BillingPlans.ToList()
            };

            return View(packages);
        }

        // GET: Users/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Delivery_Address = new SelectList(db.Addresses, "AddressID", "Address_line1");
            ViewBag.Home_Address = new SelectList(db.Addresses, "AddressID", "Address_line1");
            ViewBag.UserID = new SelectList(db.Credits, "UserID", "UserID");
            ViewBag.CopyOfID = new SelectList(db.Files, "ID", "FileName");
            ViewBag.ProofOfResidence = new SelectList(db.Files, "ID", "FileName");
            return View();
        }

        // POST: Users/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "UserID,Name,Surname,Email,Home_Address,Delivery_Address,isActive,hasFreeAds,isVerified,isSubscribed,ProofOfResidence,CopyOfID,VerficationStarted")] User user)
        {
            if (ModelState.IsValid)
            {
                user.SignUpDate = DateTime.Today;
                user.LoginCount = 0;
                user.AcceptEmails = 1;
                user.ShowListingsRules = 1;

                db.Users.Add(user);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The user was successfully created";

                return RedirectToAction("Index");
            }

            ViewBag.Delivery_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Delivery_Address);
            ViewBag.Home_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Home_Address);
            ViewBag.UserID = new SelectList(db.Credits, "UserID", "UserID", user.UserID);
            ViewBag.CopyOfID = new SelectList(db.Files, "ID", "FileName", user.CopyOfID);
            ViewBag.ProofOfResidence = new SelectList(db.Files, "ID", "FileName", user.ProofOfResidence);

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The user was not created. Please try again";

            return View(user);
        }

        public ActionResult Deactivate(int id)
        {
            User user = db.Users.Find(id);

            user.isActive = 0;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            //Send notification to account
            notificationService.SendAccountNotification(id, "Deactivated");

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult DeactivateListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            listing.isActive = 0;
            listing.ActiveDays = (int)(listing.Expires - DateTime.Now).TotalDays;
            listing.DeactivatedDate = DateTime.Now;
            listing.LastUpdatedDate = DateTime.Now;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully deactivated";

            // Send email
            notificationService.SendListingUpdateNotification(listing.SellerD, id.Value, "Deactivated");

            return Json("refresh");
        }

        // Get: Users/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            //this.DeleteAllUserInfo(id);

            this.userService.DeleteUser(id);

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The user was successfully deleted";

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult DeleteListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Listing listing = db.Listings.Find(id);
            listing.isActive = 0;
            listing.isArchived = 1;
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully deleted";

            return Json("refresh");
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpGet]
        public FileResult Download(int id, string user, string type)
        {
            Models.File file = db.Files.Find(id);

            return File(file.FileContent, "application/pdf", user + "_" + type + "-" + file.FileName);
        }

        // GET: Users/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Delivery_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Delivery_Address);
            ViewBag.Home_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Home_Address);
            ViewBag.UserID = new SelectList(db.Credits, "UserID", "UserID", user.UserID);
            ViewBag.CopyOfID = new SelectList(db.Files, "ID", "FileName", user.CopyOfID);
            ViewBag.ProofOfResidence = new SelectList(db.Files, "ID", "FileName", user.ProofOfResidence);
            return View(user);
        }

        // POST: Users/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "UserID,Name,Surname,Email,Home_Address,Delivery_Address,isActive,hasFreeAds,isVerified,isSubscribed,ProofOfResidence,CopyOfID,VerficationStarted,ProfilePicture,SignUpDate,LastLoggedIn,AcceptEmails,LoginCount,ShowListingsRules")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The user was successfully updated";

                return RedirectToAction("Index");
            }

            ViewBag.Delivery_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Delivery_Address);
            ViewBag.Home_Address = new SelectList(db.Addresses, "AddressID", "Address_line1", user.Home_Address);
            ViewBag.UserID = new SelectList(db.Credits, "UserID", "UserID", user.UserID);
            ViewBag.CopyOfID = new SelectList(db.Files, "ID", "FileName", user.CopyOfID);
            ViewBag.ProofOfResidence = new SelectList(db.Files, "ID", "FileName", user.ProofOfResidence);

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The user was not updated. Please try again";

            return View(user);
        }

        [Authorize(Roles = "Seller")]
        public ActionResult EditListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            var userid = userService.GetCurrentID();

            if (listing.SellerD == userid)
            {
                this.LoadViewBagForEditListing(listing);

                var postages = db.Postages.Where(p => (p.markedAsDeleted.HasValue && p.markedAsDeleted.Value == 0) || p.markedAsDeleted == null);
                var postageList = new List<PostageListingCreate>();

                foreach (var postage in postages)
                {
                    var postageListing = listing.PostageListings.FirstOrDefault(pl => pl.PostageID == postage.PostageID);

                    postageList.Add(new PostageListingCreate
                    {
                        PostageID = postage.PostageID,
                        Postage = postage,
                        Selected = postageListing is object,
                        Price = postageListing is object ? postageListing.Price : 0,
                        EachAdditionalItemPrice = postageListing is object ? postageListing.EachAdditionalItemPrice : 0
                    });
                }

                var editListing = new EditListing
                {
                    Listing = listing,
                    ListingID = listing.ListingID,
                    Title = listing.Title,
                    Description = listing.Description,
                    QTY = Convert.ToInt32(Math.Floor(listing.QTY)),
                    Price = listing.Price,
                    PostagePriceIsIncluded = listing.PostageListings.All(pl => pl.Price == 0),
                    PostageList = postageList,
                    shipInFourtoEightWeek = listing.shipInFourtoEightWeek,
                    shipInTwotoFourWeek = listing.shipInTwotoFourWeek
                };

                return View(editListing);
            }

            return RedirectToAction("Listings");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Seller")]
        public ActionResult EditListing([Bind(Include = "ListingID,Title,Description,Price,QTY,Expires,Photos,CoverImageIndex,PostagePriceIsIncluded,PostageList,shipInTwotoFourWeek,shipInFourtoEightWeek")] UpdateListing updatelisting)
        {
            Listing listing = db.Listings.Find(updatelisting.ListingID);

            if (updatelisting.Photos.Length > 5) // TODO: check photos (new) + preloaded
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You cannot specify more than 5 images. Please only pick 5 and try again";

                return RedirectToAction("EditListing", new { id = listing.ListingID });
            }
            else
            {
                bool canUpdate = true;
                Credit credits = null;
                int creditsNeeded = 0;

                var isFreeUpdate = false;

                if (listing != null && ModelState.IsValid)
                {
                    if (listing.Expires < DateTime.Now || userService.IsCurrentUserSubscribed())
                    {
                        canUpdate = true;
                        isFreeUpdate = true;
                    }
                    else
                    {
                        isFreeUpdate = false;
                        if (updatelisting.Expires != 0)
                        {
                            creditsNeeded += 1;
                        }

                        if (updatelisting.Photos.Any(f => f is object))
                        {
                            creditsNeeded += 1;
                        }

                        credits = db.Credits.Where(c => c.UserID == listing.SellerD).FirstOrDefault();
                        if (credits == null || credits.Amount - creditsNeeded <= 0)
                        {
                            canUpdate = false;
                        }
                    }

                    if (canUpdate)
                    {
                        if (!isFreeUpdate)
                        {
                            credits.Amount -= creditsNeeded;
                            db.Entry(credits).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.Listings.Attach(listing);
                        db.Entry(listing).Reload();
                        listing.Title = updatelisting.Title;
                        listing.Description = updatelisting.Description;
                        listing.Price = updatelisting.Price;
                        listing.QTY = updatelisting.QTY;
                        listing.shipInTwotoFourWeek = updatelisting.shipInTwotoFourWeek;
                        listing.shipInFourtoEightWeek = updatelisting.shipInFourtoEightWeek;

                         var currentPostages = db.PostageListings.Where(pl => pl.ListingID == updatelisting.ListingID);

                        // remove postages
                        var postageIdsToRemove = updatelisting.PostageList.Where(up => !up.Selected).Select(up => up.PostageID);
                        db.PostageListings.RemoveRange(currentPostages.Where(cp => postageIdsToRemove.Contains(cp.PostageID)));

                        foreach (var postage in updatelisting.PostageList)
                        {
                            if (!postage.Selected)
                            {
                                continue;
                            }

                            var currentPostage = currentPostages.FirstOrDefault(p => p.PostageID == postage.PostageID);

                            if (currentPostage is null)
                            {
                                PostageListing postageListing = new PostageListing
                                {
                                    ListingID = updatelisting.ListingID,
                                    PostageID = postage.PostageID,
                                    Price = updatelisting.PostagePriceIsIncluded ? 0 : postage.Price,
                                    EachAdditionalItemPrice = updatelisting.PostagePriceIsIncluded ? 0 : postage.EachAdditionalItemPrice
                                };

                                db.PostageListings.Add(postageListing);
                            }
                            else
                            {
                                currentPostage.Price = postage.Price;
                                currentPostage.EachAdditionalItemPrice = postage.EachAdditionalItemPrice;
                            }
                        }

                        listing.Expires = listing.Expires.AddDays(updatelisting.Expires);

                        if (UploadNewPhotos(updatelisting.Photos, listing.ListingID))
                        {
                            listing.Photos = "/Images/" + listing.ListingID;
                            if (updatelisting.Photos.Any())
                            {
                                updatelisting.CoverImageIndex = updatelisting.CoverImageIndex < 0 ? 0 : updatelisting.CoverImageIndex;
                                var coverImageName = updatelisting.Photos[updatelisting.CoverImageIndex]?.FileName.GetValidFileName();
                                if (!string.IsNullOrEmpty(coverImageName))
                                {
                                    listing.CoverImage = listing.Photos + "/" + Path.GetFileName(coverImageName);
                                }
                            }
                        }
                        db.Entry(listing).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(listing).State = EntityState.Detached;

                        TempData["StatusCode"] = (int)HttpStatusCode.OK;
                        TempData["StatusDescription"] = "The listing was successfully updated";

                        //Send notification to favourites
                        notificationService.SendFavouriteSellerNotification(listing.SellerD, listing.ListingID, "Updated");

                        return RedirectToAction("Listings");
                    }
                    else
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.PreconditionFailed;
                        TempData["StatusDescription"] = "You do not have enough credits to edit the listing. Please buy more";

                        this.LoadViewBagForEditListing(listing);

                        return View(new EditListing
                        {
                            Listing = listing,
                            ListingID = updatelisting.ListingID,
                            Title = updatelisting.Title,
                            Description = updatelisting.Description,
                            QTY = updatelisting.QTY,
                            Price = updatelisting.Price,
                            PostagePriceIsIncluded = updatelisting.PostagePriceIsIncluded,
                            PostageList = updatelisting.PostageList
                        });
                    }
                }
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The listing was not update. Please try again";

            this.LoadViewBagForEditListing(listing);

            return View(new EditListing
            {
                Listing = listing,
                ListingID = updatelisting.ListingID,
                Title = updatelisting.Title,
                Description = updatelisting.Description,
                QTY = updatelisting.QTY,
                Price = updatelisting.Price,
                PostagePriceIsIncluded = updatelisting.PostagePriceIsIncluded,
                PostageList = updatelisting.PostageList,
                shipInFourtoEightWeek = updatelisting.shipInFourtoEightWeek,
                shipInTwotoFourWeek = updatelisting.shipInTwotoFourWeek
                
            });
        }

        //SellerID
        [Authorize]
        public ActionResult Favourite(int sellerId, int? listingId)
        {
            this.AddFavourite(sellerId);

            var previousUrl = Request.UrlReferrer.ToString();

            if (previousUrl.ToLowerInvariant().Contains("/sellersaz"))
            {
                return Json("OK");
            }

            if (listingId is null)
            {
                return RedirectToAction("Seller", new { id = sellerId });
            }

            return RedirectToAction("Details", "Listings", new { id = listingId });
        }

        public ActionResult Favourites()
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/users/favourites" });
            }

            int userID = userService.GetCurrentID();

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var favourites = db.Favourites.Where(f => f.UserID == userID);

            var favouritesViewModel = new List<FavouriteViewModel>();

            foreach (var favourite in favourites)
            {
                favouritesViewModel.Add(
                    new FavouriteViewModel
                    {
                        FavouriteID = favourite.FavouriteID,
                        Seller = favourite.User,
                        MaskedSellerName = userService.MaskUsername(favourite.User),
                        Notify = favourite.Notify == 1 ? true : false
                    });
            }

            return View(favouritesViewModel);
        }

        [Authorize(Roles = "Seller")]
        public ActionResult Highlight(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            if (userService.GetCurrentCredit() >= HighlightCreditCost)
            {
                listing.isHighlight = 1;
                listing.LastUpdatedDate = DateTime.Now;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();

                creditService.DeductCredit(HighlightCreditCost);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The listing was successfully highlighted.";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.PreconditionFailed;
                TempData["StatusDescription"] = "You do not have enough credits to highlight this listing. Please buy more credits";
            }

            return Json("refresh");
        }

        // GET: Users
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var users = db.Users.Include(u => u.Address).Include(u => u.Address1).Include(u => u.Credit).Include(u => u.File).Include(u => u.File1);
            return View(users.ToList());
        }

        [Authorize(Roles = "Seller")]
        [Route("users/listings/details/{id}")]
        [Route("users/listingdetails/{id}")]
        public ActionResult ListingDetails(int id)
        {
            Listing listing = db.Listings.Find(id);

            var userid = userService.GetCurrentID();

            if (listing.SellerD != userid)
            {
                return RedirectToAction("Listings");
            }

            ListingsViewsViewModel lvvm = new ListingsViewsViewModel
            {
                Listing = listing,
                Views = listingService.NoOfViews(listing.ListingID)
            };

            return View(lvvm);
        }

        // ================================================================================== Listings
        [Authorize(Roles = "Seller")]
        public ActionResult Listings()
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/users/listings" });
            }

            if (!trackingService.HasTracking())
            {
                //TempData["StatusCode"] = (int)HttpStatusCode.NotFound;
                //TempData["StatusDescription"] = "You need to set up tracking before you can create a listing";

                //return RedirectToAction("Create", "Trackings");

                // INSERT DEFAULT TRACKING SINCE THIS FEATURE IS NOT BEING USED
                var tracking = new Tracking
                {
                    Description = "Tracking",
                    Message = "Tracking Message",
                    SellerID = this.userService.GetCurrentID()
                };
                this.db.Trackings.Add(tracking);
                this.db.SaveChanges();
            }
            else if (!paymentService.HasPreferredPayments())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.NotFound;
                TempData["StatusDescription"] = "You need to set up your preferred payments before you can create a listing. " + Environment.NewLine +
                    "If you do not use PayPal, you may leave this empty, but you still need to click on the Update Payment Details button to continue listing.";

                return RedirectToAction("Payments");
            }

            //var listings = listingService.GetAllListingsForUser();//old
            var listings = listingService.CustomGetAllListingsForUserWithPhotos(Server);

            //foreach (var listingViewModel in listings)
            //{
            //    listingViewModel.Listing.Photos = this.GetListingImagePath(listingViewModel.Listing);
            //}

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            ViewBag.UserAvailableCredit = this.creditService.GetUserAvailableCredit();
            ViewBag.UserShowListingRules = this.userService.GetShowListingRulesForCurrentUser();

            return View(listings);
        }

        [Authorize(Roles = "Seller")]
        public ActionResult ManageListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", listing.CategoryID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", listing.SellerD);
            ViewBag.ListingID = new SelectList(db.PostageListings, "ListingID", "ListingID", listing.ListingID);
            ViewBag.CanRenew = listing.Expires < DateTime.Now;

            return View(listing);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Seller")]
        public ActionResult ManageListing([Bind(Include = "ListingID,Renew,Active,isHighlight,isBump")] ManageListing updatelisting)
        {
            bool canUpdate = true;
            Credit credits = null;
            int creditsNeeded = 0;

            Listing listing = db.Listings.Find(updatelisting.ListingID);

            if (listing != null && listing.isArchived == 0)
            {
                credits = db.Credits.Where(c => c.UserID == listing.SellerD).FirstOrDefault();

                if (updatelisting.Renew != 0)
                {
                    if (credits == null || credits.Amount <= 0)
                    {
                        canUpdate = false;
                    }
                    creditsNeeded += 1;
                }

                if (listing.isHighlight == 0 && updatelisting.isHighlight != 0)
                {
                    if (credits == null || credits.Amount <= 0)
                    {
                        canUpdate = false;
                    }
                    creditsNeeded += 1;
                }

                if (listing.isBump == 0 && updatelisting.isBump != 0)
                {
                    if (credits == null || credits.Amount <= 0)
                    {
                        canUpdate = false;
                    }
                    creditsNeeded += 1;
                }

                if (canUpdate)
                {
                    if (ModelState.IsValid)
                    {
                        credits.Amount -= creditsNeeded;
                        db.Entry(credits).State = EntityState.Modified;
                        db.SaveChanges();

                        db.Listings.Attach(listing);
                        db.Entry(listing).Reload();
                        listing.Expires = DateTime.Now.AddDays(30);
                        listing.isActive = updatelisting.Active;
                        listing.isBump = updatelisting.isBump;
                        listing.isHighlight = updatelisting.isHighlight;
                        db.Entry(listing).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(listing).State = EntityState.Detached;

                        return RedirectToAction("Listings");
                    }
                }
                else
                {
                    TempData["Message"] = "You do not have enough credits to update your listing";
                }
            }

            return View(listing);
        }

        public ActionResult Notify()
        {
            return View();
        }

        public ActionResult Payments()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(paymentService.RetrievePaymentInfo());
        }

        // ==================================================================================
        [HttpPost]
        public ActionResult Payments([Bind(Include = "UserID,OptionOneSelected,OptionTwoSelected,InstructionsMessage,AutomaticNotifications,PaypalEmail,OptionOneApplyToAll,OptionTwoApplyToAll")] Payments payment)
        {
            if (paymentService.CreatePreferredPayment(payment))
            {
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The preferred payment methods were successfully updated";

                return RedirectToAction("Index", "Manage");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "he preferred payment methods were not updated. Please try again";

            return View(payment);
        }

        public ActionResult Rate(int listingID, string message, double rating)
        {
            var signedIn = userService.IsLoggedIn();

            if (signedIn == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to rate a listing";

                return RedirectToAction("Login", "Account", new { returnUrl = "/users/rate/listingID=" + listingID + "?message=" + message + "?rating=" + rating });
            }

            var successful = userService.RateListing(listingID, message, rating);

            if (successful)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You have successfully rated this listing";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "The rating for this listing could not be saved. Please try again";
            }

            return RedirectToAction("Details", "Listing", new { id = listingID });
        }

        public ActionResult RemoveAddress(int addressID)
        {
            var userid = userService.GetCurrentID();

            User user = db.Users.Find(userid);

            if (addressID == user.Delivery_Address)
            {
                Address add = db.Addresses.Find(addressID);

                if (add != null)
                {
                    db.Addresses.Remove(add);
                    db.SaveChanges();
                }

                user.Delivery_Address = null;
            }

            if (addressID == user.Home_Address)
            {
                Address add = db.Addresses.Find(addressID);

                if (add != null)
                {
                    db.Addresses.Remove(add);
                    db.SaveChanges();
                }

                user.Home_Address = null;
            }

            return RedirectToAction("UpdateProfile", "Manage");
        }

        public ActionResult RemoveProfilePicture()
        {
            var userid = userService.GetCurrentID();

            User user = db.Users.Find(userid);

            user.ProfilePicture = null;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("UpdateProfile", "Manage");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult Renew(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            if (userService.GetCurrentCredit() > 1 || userService.IsCurrentUserSubscribed())
            {
                listing.Expires = DateTime.Now.AddDays(30);
                listing.isActive = 1;
                listing.LastUpdatedDate = DateTime.Now;
                listing.SortDate = DateTime.Now;

                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();

                if (!userService.IsCurrentUserSubscribed())
                    creditService.DeductCredit();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The listing was successfully renewed";

                //Send notification to favourites
                //notificationService.SendFavouriteSellerNotification(listing.SellerD, listing.ListingID, "Renewed");
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.PreconditionFailed;
                TempData["StatusDescription"] = "You do not have enough credits to renew this listing. Please buy more credits";
            }

            return Json("refresh");
        }

        // GET: Sellers
        public ActionResult Seller(int id, string sort, string search, bool? newSearch, int? Page_No)
        {
            User seller = db.Users.Find(id);

            if (seller is null)
            {
                return null;
            }

            var listings = db.Listings.Include(l => l.PostageListings).Where(l => l.SellerD == id && l.isActive == 1 && l.isArchived == 0);

            //Search
            if (newSearch.HasValue && newSearch.Value)
            {
                Page_No = 1;
            }

            if (!string.IsNullOrEmpty(search))
            {
                listings = listings.Where(
                    l => l.Title.ToLower().Contains(search.ToLower())
                    || l.Description.ToLower().Contains(search.ToLower())
                    || l.Category.CategoryName.ToLower().Contains(search.ToLower())
                    || l.User.Name.ToLower().Contains(search.ToLower()));
            }

            ViewBag.filter = search;

            //Sort
            if (string.IsNullOrEmpty(sort))
            {
                listings = listings.OrderByDescending(l => l.isBump).ThenByDescending(l => l.DateCreated);
            }
            else
            {
                switch (sort)
                {
                    case "Newest":
                        listings = listings.OrderByDescending(l => l.DateCreated);
                        break;

                    case "Oldest":
                        listings = listings.OrderBy(l => l.DateCreated);
                        break;

                    case "Highest":
                        listings = listings.OrderByDescending(l => l.Price);
                        break;

                    case "Lowest":
                        listings = listings.OrderBy(l => l.Price);
                        break;

                    default:
                        break;
                }
            }

            ViewBag.CurrentSortOrder = sort;

            int Size_Of_Page = 10;
            int No_Of_Page = (Page_No ?? 1);

            var fullListings = listingService.GenerateFullListings(listings.ToList());

            //Get photo
            foreach (FullListing l in fullListings)
            {
                l.Photos = this.GetListingImagePath(l.ListingID, l.CoverImage);
            }

            var maskedUsername = userService.MaskUsername(seller);

            SellerViewModel sp = new SellerViewModel
            {
                Seller = seller,
                MaskedSellerName = maskedUsername,
                PagedFullListings = fullListings.ToPagedList(No_Of_Page, Size_Of_Page),
                CurrentUserRating = this.userService.GetSellerAverageRating(seller.UserID),
                FormattedSignUpDate = seller.SignUpDate is null ? string.Empty : seller.SignUpDate.Value.ToString("d MMMM yyyy"),
                CityAndCountry = this.userService.GetSellerCityAndCountry(seller.Address),
                Favourite = this.favouriteService.IsFavourite(seller.UserID),
                CurrentUserId = this.userService.GetAnIdentifier(),
                AverageRating = this.userService.GetSellerAverageRating(seller.UserID)
            };

            ViewBag.CategoryName = maskedUsername;

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            ViewBag.SellerId = sp.Seller.UserID;

            ViewBag.CurrentAction = "Seller";

            return View("Sellers", sp);
        }

        [HttpPost]
        public ActionResult ToggleFavouriteNotify(ToggleFavouriteNotifyViewModel toggleFavouriteNotifyViewModel)
        {
            this.favouriteService.ToggleNotify(toggleFavouriteNotifyViewModel.FavouriteId, toggleFavouriteNotifyViewModel.Notify);

            return RedirectToAction("Favourites");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult TurnOffShowListingsRules()
        {
            this.userService.TurnOffShowListingRulesForCurrentUser();

            return Json("ok");
        }

        [Authorize]
        public ActionResult UnFavourite(int sellerId, int? listingId)
        {
            this.RemoveFavourite(sellerId);

            var previousUrl = Request.UrlReferrer.ToString();

            if (previousUrl.ToLowerInvariant().Contains("/sellersaz"))
            {
                return Json("OK");
            }

            if (listingId is null)
            {
                return RedirectToAction("Seller", new { id = sellerId });
            }

            return RedirectToAction("Details", "Listings", new { id = listingId });
        }

        [Authorize]
        public ActionResult UnFavouriteById(int favouriteId)
        {
            var result = this.favouriteService.DeleteFavouriteById(favouriteId);

            if (result)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The seller was successfully removed from your favourites";
            }
            else
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Error removing seller from your favourites";
            }

            return RedirectToAction("Favourites");
        }

        [Authorize]
        [HttpPost]
        public ActionResult UnWatch(int id)
        {
            this.watchingService.DeleteWatching(id);

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully removed to your watchlist";

            var previousUrl = Request.UrlReferrer.ToString();

            if (previousUrl.EndsWith(WatchlistRoutePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return Json("refresh");
            }

            return Json("OK");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Verify(int id)
        {
            User user = db.Users.Find(id);

            user.isVerified = 1;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            var currentSeller = db.Sellers.Find(id);

            if (currentSeller is null)
            {
                Seller seller = new Seller
                {
                    UserID = id
                };

                db.Sellers.Add(seller);
                db.SaveChanges();
            }

            ApplicationDbContext roledb = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(roledb));

            var u = userService.GetIDToVerify(user.UserID);

            _ = UserManager.AddToRole(u, "Seller");

            // Send email to user
            notificationService.SendAccountNotification(id, "Verified");

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Watch(int id)
        {
            Listing listing = db.Listings.Find(id);

            int userID = userService.GetCurrentID();

            Watching watch = new Watching
            {
                ListingID = listing.ListingID,
                UserID = userID
            };

            db.Watchings.Add(watch);
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully added to your watchlist";

            return Json("OK");
        }

        [Route(WatchlistRoutePath)]
        public ActionResult Watchings(int? id)
        {
            int userID = userService.GetCurrentID();

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var watchlist = db.Watchings.Where(w => w.UserID == userID).ToList();
            List<ListingViewModel> listings = new List<ListingViewModel>();

            foreach (Watching w in watchlist)
            {
                Listing l = db.Listings.Find(w.ListingID);

                if (l.isActive > 0 && l.isArchived == 0)
                {
                    //Get image
                    string baseDirectory = Server.MapPath("~/Images/" + w.ListingID + "/");

                    string file = "";
                    if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
                    {
                        file = Directory.GetFiles(baseDirectory)[0];

                        byte[] imageArray = System.IO.File.ReadAllBytes(Directory.GetFiles(baseDirectory)[0]);
                        file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);

                        System.Diagnostics.Debug.WriteLine(file);
                    }

                    //Get rating
                    double avg = 0.0;
                    var ratings = db.Ratings;
                    if (ratings.Count() > 0)
                    {
                        var rates = ratings.Where(r => r.SellerID == l.SellerD);
                        if (rates.Any())
                        {
                            avg = rates.Average(r => r.RatingValue);
                        }
                        avg = Math.Round(avg * 2) / 2;
                    }

                    //Get Views
                    int views = db.ListingViews.Where(list => list.ListingID == l.ListingID).Count();

                    ListingViewModel lvm = new ListingViewModel
                    {
                        ListingID = l.ListingID,
                        Category = l.Category,
                        Description = l.Description,
                        Price = l.Price,
                        SellerD = l.SellerD,
                        Title = l.Title,
                        Image = file,
                        Rating = avg,
                        Views = views,
                        WatchlistButtonViewModel = new WatchlistButtonViewModel
                        {
                            ListingId = l.ListingID,
                            Watching = true
                        }
                    };

                    listings.Add(lvm);
                }
            }

            return View(listings);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void AddFavourite(int sellerId)
        {
            int currentUserID = userService.GetCurrentID();

            if (currentUserID == sellerId)
            {
                return;
            }

            User user = db.Users.Find(sellerId);

            if (user is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Error saving seller to your favourites";
            }
            else
            {
                this.favouriteService.AddFavourite(currentUserID, sellerId);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The seller was successfully added to your favourites";
            }
        }

        private void DeleteAllUserInfo(int id)
        {
            User user = db.Users.Find(id);

            var convs = db.Conversations.Where(c => c.User1 == user.UserID || c.User2 == user.UserID);

            if (convs.Any())
            {
                var conversationIds = convs.Select(c => c.ConversationID);
                var conversationsReply = db.ConversationReplies.Where(cr => conversationIds.Contains(cr.ConversationID));
                if (conversationsReply.Any())
                {
                    db.ConversationReplies.RemoveRange(conversationsReply);
                }
                db.Conversations.RemoveRange(convs);
            }

            var addresses = db.Addresses.Where(c => c.AddressID == user.Delivery_Address || c.AddressID == user.Home_Address);

            if (addresses.Any())
                db.Addresses.RemoveRange(addresses);

            var watchings = db.Watchings.Where(c => c.UserID == user.UserID);

            if (watchings.Any())
                db.Watchings.RemoveRange(watchings);

            var transactionHistories = db.TransactionHistories.Where(c => c.Order.BuyerID == user.UserID || c.Order.OrderListings.Any(l => l.Listing.SellerD == user.UserID));

            if (transactionHistories.Any())
                db.TransactionHistories.RemoveRange(transactionHistories);

            var postageListings = db.PostageListings.Where(o => o.Listing.SellerD == user.UserID);

            if (postageListings.Any())
                db.PostageListings.RemoveRange(postageListings);

            var orders = db.Orders.Where(o => o.OrderListings.Any(l => l.Listing.SellerD == user.UserID));

            if (orders.Any())
            {
                var orderListings = orders.SelectMany(o => o.OrderListings);

                if (orderListings.Any())
                {
                    db.OrderListings.RemoveRange(orderListings);
                }

                var transactionHistory = orders.SelectMany(o => o.TransactionHistories);

                if (transactionHistory.Any())
                {
                    db.TransactionHistories.RemoveRange(transactionHistory);
                }

                db.Orders.RemoveRange(orders);
            }

            var listings = db.Listings.Where(l => l.SellerD == user.UserID);

            if (listings.Any())
            {
                var listingIds = listings.Select(l => l.ListingID);
                var listingViews = db.ListingViews.Where(lv => listingIds.Contains(lv.ListingID));

                if (listingViews.Any())
                {
                    db.ListingViews.RemoveRange(listingViews);
                }

                var listingsWatchings = db.Watchings.Where(w => listingIds.Contains(w.ListingID));

                if (listingsWatchings.Any())
                {
                    db.Watchings.RemoveRange(listingsWatchings);
                }

                db.Listings.RemoveRange(listings);
            }

            var credits = db.Credits.Where(c => c.UserID == user.UserID);

            if (credits.Any())
            {
                db.Credits.RemoveRange(credits);
            }

            var favourites = db.Favourites.Where(f => f.UserID == user.UserID || f.SellerID == user.UserID);

            if (favourites.Any())
            {
                db.Favourites.RemoveRange(favourites);
            }

            var trackings = db.Trackings.Where(t => t.SellerID == user.UserID);

            if (trackings.Any())
            {
                db.Trackings.RemoveRange(trackings);
            }

            var subscriptions = db.Subscriptions.Where(s => s.UserID == user.UserID);

            if (subscriptions.Any())
            {
                db.Subscriptions.RemoveRange(subscriptions);
            }

            var seller = db.Sellers.Find(user.UserID);

            if (seller is object)
            {
                db.Sellers.Remove(seller);
            }

            var reportUsers = db.ReportUsers.Where(ru => ru.UserID == user.UserID || ru.SellerID == user.UserID);

            if (reportUsers.Any())
            {
                db.ReportUsers.RemoveRange(reportUsers);
            }

            var ratings = db.Ratings.Where(r => r.UserID == user.UserID || r.SellerID == user.UserID);

            if (ratings.Any())
            {
                db.Ratings.RemoveRange(ratings);
            }
            // missing if this method is gonna be used listing payment (listingId and paymentId)
            // payment offers (listing Id and Buyerid) creditHistory

            //address (address id) -> not a technical problem but it should be deleted too (GDPR)

            //counterOffer(orderId)

            // aspNetUsers

            db.Users.Remove(user);
            db.Entry(user).State = EntityState.Deleted;
            db.SaveChanges();

            ApplicationDbContext roledb = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(roledb));

            AspNetUser aspnetuser = db.AspNetUsers.Where(u => u.Email == user.Email).FirstOrDefault();

            var appUser = UserManager.FindById(aspnetuser.Id);

            UserManager.Delete(appUser);
        }

        private string GetListingImagePath(int listingId, string coverImage)
        {
            if (!string.IsNullOrEmpty(coverImage))
            {
                var coverImagePath = Server.MapPath(coverImage);

                if (System.IO.File.Exists(coverImagePath))
                {
                    byte[] imageArray = System.IO.File.ReadAllBytes(coverImagePath);
                    return "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                }
            }

            string baseDirectory = Server.MapPath("~/Images/" + listingId + "/");

            string file = string.Empty;
            if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
            {
                file = Directory.GetFiles(baseDirectory)[0];

                byte[] imageArray = System.IO.File.ReadAllBytes(file);
                file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
            }

            return file;
        }

        private string GetListingImagePath(Listing listing)
        {
            string file = string.Empty;

            try
            {
                //new changes 
                if (!string.IsNullOrEmpty(listing.CoverImage))
                {
                    var coverImagePath = Server.MapPath(listing.CoverImage);
                    if (System.IO.File.Exists(coverImagePath))
                    {
                        return ".." + listing.CoverImage;
                    }
                }
                
                //old code
                //if (!string.IsNullOrEmpty(listing.CoverImage))
                //{
                //    byte[] imageArray = System.IO.File.ReadAllBytes(Server.MapPath(listing.CoverImage));
                //    return "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                //}
                //string baseDirectory = Server.MapPath("~/Images/" + listing.ListingID + "/");
                //if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
                //{
                //    file = Directory.GetFiles(baseDirectory)[0];
                //    byte[] imageArray = System.IO.File.ReadAllBytes(file);
                //    file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                //}
            }
            catch (Exception ex)
            {
                this.Log(ex.ToString());
            }
            return file;
        }

        private void LoadViewBagForEditListing(Listing listing)
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", listing.CategoryID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", listing.SellerD);
            ViewBag.ListingID = new SelectList(db.PostageListings, "ListingID", "ListingID", listing.ListingID);
            ViewBag.MaxPhotosAllowed = MaxPhotosAllowed;
            ViewBag.IsFreeUpdate = listing.Expires < DateTime.Now;

            var currentPhotosFileNames = new List<string>();
            var photosDirectoryPath = Server.MapPath(listing.Photos);
            if (Directory.Exists(photosDirectoryPath))
            {
                currentPhotosFileNames = Directory
                    .GetFiles(photosDirectoryPath)
                    .Select(p => listing.Photos + "/" + Path.GetFileName(p))
                    .ToList();
            }
            ViewBag.CurrentPhotosFileNames = currentPhotosFileNames;

            ViewBag.UserAvailableCredit = this.creditService.GetUserAvailableCredit();
        }

        private void Log(string message)
        {
            var fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "errors.log");

            using (FileStream fs = System.IO.File.Open(fileName, FileMode.OpenOrCreate))
            {
                var log = $"[{DateTime.Now}] {message}";
                byte[] text = new UTF8Encoding(true).GetBytes(log);
                fs.Write(text, 0, text.Length);
            }
        }

        private void RemoveFavourite(int sellerId)
        {
            int currentUserID = userService.GetCurrentID();

            if (currentUserID == sellerId)
            {
                return;
            }

            User user = db.Users.Find(sellerId);

            if (user is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Error removing seller from your favourites";
            }
            else
            {
                favouriteService.DeleteFavourite(sellerId);

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The seller was successfully removed from your favourites";
            }
        }

        private bool UploadNewPhotos(HttpPostedFileBase[] files, int listingID)
        {
            if (ModelState.IsValid)
            {
                var photoDirectoryPath = Server.MapPath("~/Images/" + listingID + "/");
                if (Directory.Exists(photoDirectoryPath))
                {
                    // DELETE current photos
                    if (files.Any(f => f is object))
                    {
                        DirectoryInfo directory = new DirectoryInfo(photoDirectoryPath);
                        directory.Attributes = FileAttributes.Normal;

                        foreach (FileInfo f in directory.GetFiles())
                        {
                            f.Attributes = FileAttributes.Normal;
                            f.Delete();
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(photoDirectoryPath);
                }

                // Upload new photos
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        var InputFileName = Path.GetFileName(file.FileName);
                        var serverSavePath = photoDirectoryPath + InputFileName.GetValidFileName();
                        file.SaveAs(serverSavePath);
                    }
                }
                return true;
            }
            return false;
        }
    }
}