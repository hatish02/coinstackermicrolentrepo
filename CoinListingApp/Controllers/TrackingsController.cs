﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;

namespace CoinListingApp.Controllers
{
    public class TrackingsController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly TrackingService trackingService = new TrackingService();
        private readonly UserService userService = new UserService();

        // GET: Trackings/Create
        public ActionResult Create()
        {
            ViewBag.SellerID = new SelectList(db.Users, "UserID", "Name");

            return View();
        }

        // POST: Trackings/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TrackingID,SellerID,Description,Message")] Tracking tracking)
        {
            if (ModelState.IsValid)
            {
                tracking.SellerID = userService.GetCurrentID();

                db.Trackings.Add(tracking);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The tracking information has been successfully captured";

                return RedirectToAction("Index", "Manage");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The tracking information was not captured. Please try again";

            ViewBag.SellerID = new SelectList(db.Users, "UserID", "Name", tracking.SellerID);
            return View(tracking);
        }

        // GET: Trackings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tracking tracking = db.Trackings.Find(id);
            if (tracking == null)
            {
                return HttpNotFound();
            }
            return View(tracking);
        }

        // POST: Trackings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tracking tracking = db.Trackings.Find(id);
            db.Trackings.Remove(tracking);
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The tracking information has been successfully deleted";

            return RedirectToAction("Index");
        }

        // GET: Trackings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tracking tracking = db.Trackings.Find(id);
            if (tracking == null)
            {
                return HttpNotFound();
            }
            return View(tracking);
        }

        // GET: Trackings/Edit/5
        public ActionResult Edit(int? id)
        {
            Tracking tracking = null;

            if (id == null)
            {
                tracking = trackingService.HasTrackingEntity();
            }
            else
            {
                tracking = db.Trackings.Find(id);
            }

            if (tracking == null)
            {
                return HttpNotFound();
            }
            ViewBag.SellerID = new SelectList(db.Users, "UserID", "Name", tracking.SellerID);
            return View(tracking);
        }

        // POST: Trackings/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TrackingID,SellerID,Description,Message")] Tracking tracking)
        {
            if (ModelState.IsValid)
            {
                tracking.Description = string.Empty;
                db.Entry(tracking).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The tracking information has been successfully updated";

                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index", "Manage");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The tracking information was not updated. Please try again";

            ViewBag.SellerID = new SelectList(db.Users, "UserID", "Name", tracking.SellerID);
            return View(tracking);
        }

        // GET: Trackings
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var trackings = db.Trackings.Include(t => t.User);
            return View(trackings.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}