﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using CoinListingApp.Resources;

namespace CoinListingApp.Controllers
{
    public class EmailController : Controller
    {
        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public async Task<ActionResult> Notify(string emailAddress, string emailMsg, string emailSubject)
        {
            try
            {
                // Sending Email.
                await this.SendEmailAsync(emailAddress, emailMsg, emailSubject);
                // Info.
                //return this.Json(new { EnableSuccess = true, SuccessTitle = "Success", SuccessMsg = "Notification has been sent successfully! to '" + model[0].Email + "' Check your email." });
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);

                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                System.Diagnostics.Debug.WriteLine(ex.HResult);
                System.Diagnostics.Debug.WriteLine(ex.InnerException);
                System.Diagnostics.Debug.WriteLine(ex.TargetSite);

                // Info
                return this.Json(new { EnableError = true, ErrorTitle = "Error", ErrorMsg = ex.Message });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public async Task<ActionResult> NotifyWithException(string emailAddress, string emailMsg, string emailSubject)
        {
            var taskResult = await this.SendEmailAsyncWithException(emailAddress, emailMsg, emailSubject);
            // Info.
            //return this.Json(new { EnableSuccess = true, SuccessTitle = "Success", SuccessMsg = "Notification has been sent successfully! to '" + model[0].Email + "' Check your email." });
            return RedirectToAction("Index", "Home");
            try
            {
                // Sending Email.
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);

                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                System.Diagnostics.Debug.WriteLine(ex.HResult);
                System.Diagnostics.Debug.WriteLine(ex.InnerException);
                System.Diagnostics.Debug.WriteLine(ex.TargetSite);

                // Info
                return this.Json(new { EnableError = true, ErrorTitle = "Error", ErrorMsg = ex.Message });
            }
        }

        public async Task<bool> SendEmailAsync(string email, string msg, string subject = "")
        {
            // Initialization.
            bool isSend = false;

            try
            {
                // Initialization.
                var body = msg;
                var message = new MailMessage();

                // Settings.
                message.To.Add(new MailAddress(email));
                message.From = new MailAddress(EmailNotifications.FROM_EMAIL_ACCOUNT, "Coinstacker");
                //message.From = new MailAddress("info@coinstacker.co.uk");
                message.Subject = !string.IsNullOrEmpty(subject) ? subject : EmailNotifications.EMAIL_SUBJECT_DEFAULT;
                message.Body = body;
                message.IsBodyHtml = true;

                await Task.Run(async () =>
                {
                    var credential = new NetworkCredential
                    {
                        UserName = EmailNotifications.FROM_EMAIL_ACCOUNT,
                        Password = EmailNotifications.FROM_EMAIL_PASSWORD
                    };

                    using (var smtp = new SmtpClient())
                    {
                        // Settings.

                        //var credential = new NetworkCredential
                        //{
                        //    UserName = "info@coinstacker.co.uk",
                        //    Password = "Phoenix123!"
                        //};

                        // Settings.
                        smtp.Credentials = credential;
                        smtp.Host = EmailNotifications.SMTP_HOST_TYPE;
                        //smtp.Host = "outlook.office365.com";
                        smtp.Port = Convert.ToInt32(EmailNotifications.SMTP_PORT);
                        //smtp.EnableSsl = true;

                        // Sending
                        try
                        {
                            await smtp.SendMailAsync(message);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        // Settings.
                        isSend = true;
                    }
                });
            }
            catch (Exception ex)
            {
                // Info
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
            }

            System.Diagnostics.Debug.WriteLine(isSend);

            // info.
            return isSend;
        }

        public async Task<bool> SendEmailAsyncWithException(string email, string msg, string subject = "")
        {
            // Initialization.
            bool isSend = false;

            try
            {
                // Initialization.
                var body = msg;
                var message = new MailMessage();

                // Settings.
                message.To.Add(new MailAddress(email));
                message.From = new MailAddress(EmailNotifications.FROM_EMAIL_ACCOUNT);
                //message.From = new MailAddress("info@coinstacker.co.uk");
                message.Subject = !string.IsNullOrEmpty(subject) ? subject : EmailNotifications.EMAIL_SUBJECT_DEFAULT;
                message.Body = body;
                message.IsBodyHtml = true;

                var credential = new NetworkCredential
                {
                    UserName = EmailNotifications.FROM_EMAIL_ACCOUNT,
                    Password = EmailNotifications.FROM_EMAIL_PASSWORD
                };

                using (var smtp = new SmtpClient())
                {
                    // Settings.

                    //var credential = new NetworkCredential
                    //{
                    //    UserName = "info@coinstacker.co.uk",
                    //    Password = "Phoenix123!"
                    //};

                    // Settings.
                    smtp.Credentials = credential;
                    smtp.Host = EmailNotifications.SMTP_HOST_TYPE;
                    //smtp.Host = "outlook.office365.com";
                    smtp.Port = Convert.ToInt32(EmailNotifications.SMTP_PORT);
                    //smtp.EnableSsl = true;

                    // Sending
                    try
                    {
                        //var photoDirectoryPath = Server.MapPath("~/Images/");

                        //System.IO.File.AppendAllText(photoDirectoryPath + "logSession2.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + Environment.NewLine + "| " + smtp.Host + " | " + credential.UserName + " - " + credential.Password);

                        await smtp.SendMailAsync(message);
                    }
                    catch (Exception ex)
                    {
                        //var photoDirectoryPath = Server.MapPath("~/Images/");

                        //System.IO.File.AppendAllText(photoDirectoryPath + "logSession.txt", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:ffff") + Environment.NewLine + ex.ToString() + " | " + credential.UserName + " - " + credential.Password);
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        throw ex;
                    }

                    // Settings.
                    isSend = true;
                }
            }
            catch (Exception ex)
            {
                // Info
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
            }

            System.Diagnostics.Debug.WriteLine(isSend);

            // info.
            return isSend;
        }
    }
}