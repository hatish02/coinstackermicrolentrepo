﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Resources;
using CoinListingApp.Services;
using CoinListingApp.Services.Validation;
using CoinListingApp.Utils;
using PagedList;

//using CoinListingApp.Resources;
using PayPal;
using PayPal.Api;

namespace CoinListingApp.Controllers
{
    public class ListingsController : Controller
    {
        private const int MaxPhotosAllowed = 5;
        private static Random random = new Random();
        private readonly CreditService creditService = new CreditService();
        private readonly Entities db = new Entities();
        private readonly FavouriteService favouriteService = new FavouriteService();
        private readonly ListingService listingService = new ListingService();
        private readonly NotificationService notificationService = new NotificationService();
        private readonly OfferService offerService = new OfferService();
        private readonly OrderService orderService = new OrderService();
        private readonly PaymentService paymentService = new PaymentService();
        private readonly UserService userService = new UserService();
        private readonly WatchingService watchingService = new WatchingService();
        private PayPal.Api.Payment payment;

        //THIS METHOD IS NOT USED!
        public ActionResult AddListingAndGoToCheckoutV2(int id, int amount)
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/listings/checkoutv2/" + id + "?amount=" + amount });
            }

            Models.Address address = userService.GetBusinessAddress();

            CheckoutPageV2 cp = new CheckoutPageV2
            {
                CheckoutListings = new List<CheckoutListing>(),
                //Listing = listing,
                //Amount = amount,
                Address = address,
                //PaymentMethod = listingService.GetListingPaymentMethod_(Listing.ListingID)
            };

            var cart = Session["Cart"] as List<ItemCartViewModel>;

            if (cart != null)
            {
                foreach (var item in cart)
                {
                    Listing listingDb = db.Listings.Find(item.IdListing);

                    cp.CheckoutListings.Add(new CheckoutListing() { Listing = listingDb, Quantity = item.Qty });
                }
            }

            Listing listing = db.Listings.Find(id);
            cp.CheckoutListings.Add(new CheckoutListing() { Listing = listing, Quantity = amount });
            cp.SelectedPostageListing = cp.CheckoutListings.SelectMany(o => o.Listing.PostageListings).OrderByDescending(pl => pl.Price).FirstOrDefault();
            cp.PostageListings = cp.CheckoutListings.SelectMany(o => o.Listing.PostageListings).GroupBy(g => g.PostageID).Select(s => s.First()).ToList();
            cp.PaymentMethod = listingService.GetListingPaymentMethod_(id);

            return View("CheckoutV2", cp);
        }

        // ===== PAYPAL ONLY =====
        public ActionResult BuyNow(int id, string Cancel = null)
        {
            //Find all the necessary information
            Models.Order order = db.Orders.Find(id);

            if (order == null)
            {
                return View("Details", id);
            }

            //getting the apiContext
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                string payerId = Request.Params["PayerID"];

                var guid = Request.Params["guid"];
                var paymentId = Request.Params["paymentId"];
                var executedPayment = ExecutePayment(apiContext, payerId, paymentId);
                if (executedPayment.state.ToLower() != "approved")
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                    System.Diagnostics.Debug.WriteLine("Failed");

                    return View("Details", new { id });
                }
            }
            catch (PaymentsException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Details);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Request);
                System.Diagnostics.Debug.WriteLine(ex.Response);
                System.Diagnostics.Debug.WriteLine(ex.StatusCode);
                System.Diagnostics.Debug.WriteLine(ex.WebExceptionStatus);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);

                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return View("Details", new { id });
            }
            catch (Exception ex)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                return View("Details", new { id });
            }

            //The purchase was successful -> update order state

            var updateStatus = orderService.OrderPaypalPayment(order);

            if (updateStatus == false)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The purchase of the listing was unsuccessful. Please try again";

                return RedirectToAction("Details", new { id });
            }

            Session["Cart"] = null;

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The purchase of the listing was successful";

            return RedirectToAction("Index", "Orders");
        }

        // Checkout
        [HttpPost]
        public ActionResult CheckoutV2(CheckoutPageV2 checkoutPageV2)
        {
            //Check if user same as seller
            var currentUserId = this.userService.GetCurrentID();
            if (checkoutPageV2.CheckoutListings.Any(cl => cl.Listing.SellerD == currentUserId))
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You are the seller of these listings.";

                return RedirectToAction("Index", "Home");
            }

            //TODO: PUT the properties of checkoutPageV2 in the view page so they don't come null ffs
            foreach (var checkoutListing in checkoutPageV2.CheckoutListings)
            {
                //Check if listing expired
                if (listingService.HasListingExpired(checkoutListing.Listing.ListingID))
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "The listing has expired. You will not be able to purchase this listing";

                    return RedirectToAction("Details", new { id = checkoutListing.Listing.ListingID });
                }

                //Check if order exists first if
                if (orderService.OpenOrderExists(checkoutListing.Listing.ListingID))
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest; TempData["StatusDescription"] = "An order for this listing exists. Please complete it before creating another one";

                    return RedirectToAction("Details", new { id = checkoutListing.Listing.ListingID });
                }
            }

            checkoutPageV2.SelectedPostageListing = checkoutPageV2.PostageListings.FirstOrDefault(p => p.Selected) ?? checkoutPageV2.PostageListings.FirstOrDefault();

            ListingBuyNowV2 listingBuyNowV2 = orderService.Convert(checkoutPageV2);
            //create order with CREATED status
            Models.Order order = orderService.BuyNowV2(listingBuyNowV2);

            if (listingBuyNowV2.PaymentMethod.ToLower().Contains("paypal"))
            {
                return this.HandlePaypalPayment(listingBuyNowV2, order);
            }
            else // EFT
            {
                //The purchase was successful -> continue
                //Update ORder status to PAYMENT PENDING

                bool resultConfirm = orderService.OrderNotPaypalPayment(order, listingBuyNowV2.SellerID);

                if (order == null)
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "The purchase of the listing was unsuccessful. Please try again";

                    return RedirectToAction("CheckoutV2", new { });
                }

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "Please send the payment to complete your order.";

                Session["Cart"] = null;

                return RedirectToAction("Payment", "Orders", new { id = order.OrderID });
            }
        }

        public ActionResult CheckoutV2()
        {
            if (!userService.IsSignedUp())
            {
                TempData["StatusCode"] = (int)HttpStatusCode.Forbidden;
                TempData["StatusDescription"] = "You need to be signed in to checkout";

                return RedirectToAction("Login", "Account", new { returnUrl = "/listings/CheckoutV2" });
            }

            Models.Address address = userService.GetBusinessAddress();

            CheckoutPageV2 cp = new CheckoutPageV2
            {
                CheckoutListings = new List<CheckoutListing>(),
                //Listing = listing,
                //Amount = amount,
                Address = address,
                //PaymentMethod = listingService.GetListingPaymentMethod_(Listing.ListingID)
            };

            var cart = Session["Cart"] as List<ItemCartViewModel>;

            if (cart is object && cart.Any())
            {
                foreach (var item in cart)
                {
                    Listing listingDb = db.Listings.Find(item.IdListing);
                    listingDb.CoverImage = this.GetListingCoverImageOrOtherPath(listingDb);
                    cp.CheckoutListings.Add(
                        new CheckoutListing
                        {
                            Listing = listingDb,
                            Quantity = item.Qty,
                            SellerViewModel = new SellerViewModel
                            {
                                SellerId = listingDb.SellerD,
                                //MaskedSellerName = this.userService.GetMaskUsername(listingDb.SellerD)
                                MaskedSellerName = this.userService.GetCoinstackerName(listingDb.SellerD)
                            }
                        });
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            var totalQuantity = cp.CheckoutListings.Sum(cl => cl.Quantity);

            var postageListingsDic = new Dictionary<int, PostageListing>();

            foreach (var checkoutListing in cp.CheckoutListings)
            {
                foreach (var postageListing in checkoutListing.Listing.PostageListings)
                {
                    var postageListingAlreadyAdded = postageListingsDic.TryGetValue(postageListing.PostageID, out var addedPostageListing);

                    if (!postageListingAlreadyAdded)
                    {
                        postageListingsDic.Add(postageListing.PostageID, postageListing);
                    }
                    else if (this.listingService.CalculateTotalPricePostage(addedPostageListing, totalQuantity)
                        < this.listingService.CalculateTotalPricePostage(postageListing, totalQuantity))
                    {
                        postageListingsDic[postageListing.PostageID] = postageListing;
                    }
                }
            }

            cp.PostageListings = postageListingsDic.Values.OrderByDescending(pl => pl.Price).ToList();

            if (cp.PostageListings.Any())
            {
                cp.PostageListings.FirstOrDefault().Selected = true;
                cp.SelectedPostageListing = cp.PostageListings.FirstOrDefault();
            }

            if (cp.CheckoutListings.Any())
                cp.PaymentMethod = listingService.GetListingPaymentMethod_(cp.CheckoutListings.FirstOrDefault().Listing.ListingID);

            return View(cp);
        }

        // GET: Listings/Create
        [Auth(Roles = "Seller")]
        public ActionResult Create()
        {
            var currentUser = this.userService.GetCurrentUser();

            if (userService.GetBusinessAddress(currentUser) is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You need to update your business address before you can create a listing";

                return RedirectToAction("UpdateProfile", "Manage");
            }
            else if (!this.userService.IsCurrentUserSubscribed(currentUser) && this.userService.GetCurrentCredit(currentUser) <= 0.0)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You need to buy credits first or subscribe to create a new listing.";

                return RedirectToAction("BuyCredits", "Users");
            }
            else if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            CreateListing createListing = new CreateListing();
            this.LoadCreateListingAndViewBag(createListing);

            return View(createListing);
        }

        // POST: Listings/Create To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Auth(Roles = "Seller")]
        public ActionResult Create(CreateListing listing)
        {
            if (listing is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "There was something wrong with your request. Please try again.";

                return RedirectToAction("UpdateProfile", "Manage");
            }

            if (listing.PaymentSelectedId == PaymentTypeProvider.PayPalType)
            {
                var payments = paymentService.RetrievePaymentInfo();

                if (string.IsNullOrEmpty(payments.PaypalEmail))
                {
                    this.LoadCreateListingAndViewBag(listing);

                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = "To receive PayPal G&S payments, please enter your Paypal address in the Payment management section.";

                    return View(listing);
                }
            }

            if (!listing.PostageList.Any(p => p.Selected))
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "You need to select a Postage method.";
            }
            else if (listing.Photos.Length > MaxPhotosAllowed)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = $"Maximum of {MaxPhotosAllowed} photos exceeded.";
            }
            else
            {
                var listResult = listingService.CreateListing(listing); 

                if (listResult.Success)
                {
                    var list = listResult.Value;
                    if (ModelState.IsValid)
                    {
                        db.Listings.Add(list);
                        db.SaveChanges();

                        //if (listing.PaymentSelectedId > 0)
                        //{
                        //    ListingPayment lp = new ListingPayment
                        //    {
                        //        ListingID = list.ListingID,
                        //        PaymentID = listing.PaymentSelectedId
                        //    };

                        //    db.ListingPayments.Add(lp);
                        //    db.SaveChanges();
                        //}

                        //db.Entry(list).State = EntityState.Modified;
                        //db.SaveChanges();

                        if (UploadFiles(listing.Photos, list.ListingID))
                        {
                            list.Photos = "/Images/" + list.ListingID;
                            if (listing.Photos.Any())
                            {
                                listing.CoverImageIndex = listing.CoverImageIndex < 0 ? 0 : listing.CoverImageIndex;
                                var coverImageName = listing.Photos[listing.CoverImageIndex]?.FileName.GetValidFileName();
                                if (!string.IsNullOrEmpty(coverImageName))
                                {
                                    list.CoverImage = list.Photos + "/" + Path.GetFileName(coverImageName);
                                }
                            }
                        }

                        db.Entry(list).State = EntityState.Modified;
                        db.SaveChanges();

                        foreach (var postage in listing.PostageList)
                        {
                            if (!postage.Selected)
                            {
                                continue;
                            }

                            PostageListing postageListing = new PostageListing
                            {
                                ListingID = list.ListingID,
                                PostageID = postage.PostageID,
                                Price = listing.PostagePriceIsIncluded ? 0 : postage.Price,
                                EachAdditionalItemPrice = listing.PostagePriceIsIncluded ? 0 : postage.EachAdditionalItemPrice
                            };

                            db.PostageListings.Add(postageListing);
                        }

                        db.SaveChanges();

                        TempData["StatusCode"] = (int)HttpStatusCode.OK;
                        TempData["StatusDescription"] = "The listing was successfully created";

                        //Send notification to favourites
                        notificationService.SendFavouriteSellerNotification(list.SellerD, list.ListingID, "Posted");

                        return RedirectToAction("Listings", "Users");
                    }
                }
                else if (listResult.GetFailureCode() == FailureCode.NotEnoughCredits)
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    var neededCredits = listResult.Keys[OutcomesHelper.NeededCreditsKey].ToString();
                    var currentCredits = listResult.Keys[OutcomesHelper.CreditsKey].ToString();
                    TempData["StatusDescription"] = $"You don't have enough credits. It's necessary {neededCredits} credits and you currently have {currentCredits} credits.";
                }
            }

            this.LoadCreateListingAndViewBag(listing);

            if (string.IsNullOrEmpty(TempData["StatusDescription"].ToString()))
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "There was a problem. The listing was not created";
            }

            return View(listing);
        }

        // GET: Listings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            return View(listing);
        }

        // POST: Listings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Listing listing = db.Listings.Find(id);
            db.PostageListings.RemoveRange(listing.PostageListings);
            db.Listings.Remove(listing);
            db.SaveChanges();
            return RedirectToAction("Listings", "Users");
        }

        // GET: Listings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }

            var userID = userService.GetAnIdentifier();

            //Cookies!
            if (HttpContext.Request.Cookies[listing.ListingID.ToString() + userID.ToString()] == null)
            {
                HttpCookie cookie = new HttpCookie(listing.ListingID.ToString(), "Viewed");
                cookie.Expires = DateTime.MaxValue;
                Response.Cookies.Add(cookie);

                ListingView view = new ListingView
                {
                    DateViewed = DateTime.Today,
                    ListingID = listing.ListingID
                };

                db.ListingViews.Add(view);
                db.SaveChanges();
            }

            //System.Diagnostics.Debug.WriteLine(photos[0]);

            //Create DetailListing viewmodel
            DetailsListing _listing = new DetailsListing
            {
                Listing = listing,
                Amount = 1,
                Rating = -1,
                Report = null,
                WatchlistButtonViewModel = new WatchlistButtonViewModel
                {
                    ListingId = listing.ListingID,
                    Watching = watchingService.IsWatching(listing.ListingID)
                },
                Favourite = favouriteService.IsFavourite(listing.SellerD),
                Reported = userService.IsReported(listing.User.UserID),
                OfferMade = offerService.OfferExists(listing.ListingID),
                UserID = userID,
                Expired = listingService.HasListingExpired(listing.ListingID),
                PaymentMethods = listingService.GetListingPaymentMethod(listing.ListingID),
                PaymentMethod = listingService.GetListingPaymentMethod_(listing.ListingID),
                NoViews = listingService.NoOfViews(listing.ListingID),
                Photos = this.GetListingPhotos(listing),
                SellerNameMasked = userService.MaskUsername(listing.User),
                AverageRating = this.userService.GetSellerAverageRating(listing.User.UserID),
                PostagePriceIsIncluded = this.listingService.IsFreePostage(listing),
                shipInTwotoFourWeek = listing.shipInTwotoFourWeek,
                shipInFourtoEightWeek = listing.shipInFourtoEightWeek,
            };

            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            System.Diagnostics.Debug.WriteLine(_listing.Photos);

            return View(_listing);
        }

        // GET: Listings/Edit/5
        [Auth(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null || listing.isArchived == 1)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", listing.CategoryID);
            ViewBag.SellerD = new SelectList(db.Users, "UserID", "Name", listing.SellerD);
            ViewBag.ListingID = new SelectList(db.PostageListings, "ListingID", "ListingID", listing.ListingID);
            return View(listing);
        }

        // POST: Listings/Edit/5 To protect from overposting attacks, enable the specific properties
        // you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ListingID,SellerD,CategoryID,Title,Description,QTY,AllowOffers,isHighlight,isBump,isActive,Price,Expires,DeactivatedDate,ActiveDays,Photos")] Listing listing)
        {
            if (ModelState.IsValid)
            {
                listing.LastUpdatedDate = DateTime.Now;
                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", listing.CategoryID);
            ViewBag.SellerD = new SelectList(db.Users, "UserID", "Name", listing.SellerD);
            ViewBag.ListingID = new SelectList(db.PostageListings, "ListingID", "ListingID", listing.ListingID);
            return View(listing);
        }

        public ActionResult Featured()
        {
            List<Listing> listings = db.Listings.Where(l => l.isBump > 1).Where(l => l.isActive > 0 && l.isArchived == 0).ToList();
            return View(listings);
        }

        public ActionResult FrequentSellers(int number = 0)
        {
            var sellers = userService.FrequentSellers();

            List<KeyValuePair<int, int>> frequentSellers;

            if (number > 0)
            {
                frequentSellers = sellers.Take(number).ToList();
            }
            else
            {
                frequentSellers = sellers;
            }

            var _sellers = userService.GetSellers(frequentSellers);

            return PartialView("_FrequentSellers", _sellers);
        }

        // GET: Listings
        [Route("listings")]
        [Route("listings/Index")]
        [Route("listings/Category={category?}")]
        [Route("listings/Index/Category={category?}")]
        public ActionResult Index(string sort, string search, bool? newSearch, int? Page_No, int? category)
        {
            IQueryable<Listing> listings;

            // Find listings according to a category
            if (category != null)
            {
                listings = db.Listings.Include(l => l.Category).Include(l => l.User).Include(l => l.PostageListings).Where(l => l.Category.CategoryID == category).Where(l => l.isActive > 0 && l.isArchived == 0);

                ViewBag.CategoryName = db.Categories.Find(category).CategoryName;
                ViewBag.category = category;
            }
            else
            {
                listings = db.Listings.Include(l => l.Category).Include(l => l.User).Include(l => l.PostageListings).Where(l => l.isActive > 0 && l.isArchived == 0);
            }

            //Search
            if (newSearch.HasValue && newSearch.Value)
            {
                Page_No = 1;
            }

            if (!string.IsNullOrEmpty(search))
            {
                listings = listings.Where(
                    l => l.Title.ToLower().Contains(search.ToLower())
                    || l.Description.ToLower().Contains(search.ToLower())
                    || l.Category.CategoryName.ToLower().Contains(search.ToLower())
                    || l.User.Name.ToLower().Contains(search.ToLower()));
            }

            ViewBag.filter = search;

            //Sort
            if (string.IsNullOrEmpty(sort))
            {
                listings = listings.OrderByDescending(l => l.isBump).ThenByDescending(l => l.SortDate);
            }
            else
            {
                switch (sort)
                {
                    case "Newest":
                        listings = listings.OrderByDescending(l => l.SortDate);
                        break;

                    case "Oldest":
                        listings = listings.OrderBy(l => l.SortDate);
                        break;

                    case "Highest":
                        listings = listings.OrderByDescending(l => l.Price);
                        break;

                    case "Lowest":
                        listings = listings.OrderBy(l => l.Price);
                        break;

                    default:
                        break;
                }
            }

            ViewBag.CurrentSortOrder = sort;

            int Size_Of_Page = 10;
            int No_Of_Page = (Page_No ?? 1);

            var _listings = listingService.GenerateFullListings(listings.ToList());

            //Get photo
            foreach (FullListing l in _listings)
            {
                l.Photos = this.GetListingImagePath(l.ListingID, l.CoverImage);
            }

            ViewBag.CurrentAction = "Index";

            return View(_listings.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [Authorize]
        public ActionResult MakeOffer(int id)
        {
            Listing listing = db.Listings.Find(id);

            if (listing != null && listing.isArchived == 0)
            {
                var detailsListing = new DetailsListing
                {
                    Listing = listing,
                    Amount = 1,
                    Expired = listingService.HasListingExpired(listing.ListingID),
                    Photos = this.GetListingPhotos(listing),
                    SellerNameMasked = userService.MaskUsername(listing.User)
                };

                return View(detailsListing);
            }

            return RedirectToAction("Details", new { id });
        }

        [HttpPost, ActionName("MakeOffer")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult MakeOfferProcess(DetailsListing detailsListing)
        {
            //Check if listing expired
            if (listingService.HasListingExpired(detailsListing.Listing.ListingID))
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The listing has expired. You will not be able to make an offer for this listing";

                return RedirectToAction("Details", new { detailsListing.Listing.ListingID });
            }

            var offer = offerService.MakeOffer(detailsListing.Listing.ListingID, (int)detailsListing.Listing.QTY, detailsListing.Listing.Price);

            if (offer > 0)
            {
                //Send email
                Offer o = db.Offers.Find(offer);
                if (o.BuyerUpdate == 1)
                {
                    notificationService.SendOfferReceivedNotification(o.Listing.SellerD, offer);
                }

                return RedirectToAction("Index", "Offers");
            }
            Listing listing = db.Listings.Find(detailsListing.Listing.ListingID);

            return View(listing);
        }

        public ActionResult MostViewed(int number = 0)
        {
            var listings = listingService.HotRightNow();

            List<KeyValuePair<int, int>> hotListings;

            if (number > 0)
            {
                hotListings = listings.Take(number).ToList();
            }
            else
            {
                hotListings = listings;
            }

            var _listings = listingService.GetListings(hotListings);

            foreach (var listing in _listings)
            {
                listing.Photos = this.GetListingImagePath(listing);
            }

            var mostPopularViewModel = new MostPopularViewModel
            {
                Listings = _listings,
                UserWatchingListingsIds = watchingService.GetUserWatchingListingsIds()
            };

            return PartialView("_MostPopular", mostPopularViewModel);
        }

        [RequireRequestValue("category")]
        public ActionResult MostViewed(string category, int number = 0)
        {
            var listings = listingService.HotRightNow(category);

            List<KeyValuePair<int, int>> hotListings;

            if (number > 0)
            {
                hotListings = listings.Take(number).ToList();
            }
            else
            {
                hotListings = listings;
            }

            var _listings = listingService.GetListings(hotListings);

            foreach (var listing in _listings)
            {
                listing.Photos = this.GetListingImagePath(listing);
            }

            var mostPopularViewModel = new MostPopularViewModel
            {
                Listings = _listings,
                UserWatchingListingsIds = watchingService.GetUserWatchingListingsIds()
            };

            return PartialView("_MostPopular", mostPopularViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private PayPal.Api.Payment CreatePayment(APIContext apiContext, string redirectUrl, ListingBuyNow listing, string email)
        {
            Listing list = db.Listings.Find(listing.ListingID);

            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            //Adding Item Details like name, currency, price etc
            itemList.items.Add(new Item()
            {
                name = list.Title,
                currency = "GBP",
                price = listing.ListingPrice.ToString(),
                quantity = (listing.ListingQTY).ToString(),
                sku = listing.ListingID.ToString()
            });

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                shipping = list.PostageListings.SingleOrDefault().Price.ToString(),
                subtotal = ((listing.ListingPrice * listing.ListingQTY)).ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                total = ((listing.ListingPrice * listing.ListingQTY) + list.PostageListings.SingleOrDefault().Price).ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = email
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            PayPal.Api.Payment payment = null;

            try
            {
                payment = this.payment.Create(apiContext);
            }
            catch (Exception ex)
            {
                throw;
            }

            // Create a payment using a APIContext
            return payment;
        }

        private PayPal.Api.Payment CreatePaymentV2(APIContext apiContext, string redirectUrl, ListingBuyNowV2 listingBuyNowV2, string email)
        {
            //Listing list = db.Listings.Find(listing.ListingID);

            //create itemlist and add item objects to it
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };

            double shippingPrice = 0;

            foreach (var listing in listingBuyNowV2.Listings)
            {
                //Listing list = db.Listings.Find(listing.ListingID);

                //Adding Item Details like name, currency, price etc
                itemList.items.Add(new Item()
                {
                    name = listing.Listing.Title,
                    currency = "GBP",
                    price = listing.Price.ToString(),
                    quantity = (listing.Quantity).ToString(),
                    sku = listing.Listing.ListingID.ToString()
                });
            }

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details
            var details = new Details()
            {
                tax = "0",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                shipping = listingBuyNowV2.ShippingPrice.ToString(),
                subtotal = listingBuyNowV2.TotalListingsPrice.ToString()
            };
            //Final amount with details
            var amount = new Amount()
            {
                currency = "GBP",
                //TODO: instead of using list.PostageListings.SingleOrDefault().Price, we need to let the user select the postage required.
                total = listingBuyNowV2.TotalPrice.ToString(), // Total must be equal to sum of tax, shipping and subtotal.
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction

            //Random invoice number
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var inv = new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            transactionList.Add(new Transaction()
            {
                description = "Credit Package Transaction",
                invoice_number = inv, //Generate an Invoice No
                amount = amount,
                item_list = itemList,
                payee = new Payee
                {
                    email = email
                }
            });
            this.payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            PayPal.Api.Payment payment = null;

            try
            {
                payment = this.payment.Create(apiContext);
            }
            catch (Exception ex)
            {
                throw;
            }

            // Create a payment using a APIContext
            return payment;
        }

        //Paypal Helper functions
        private PayPal.Api.Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new PayPal.Api.Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }

        private string GetListingCoverImageOrOtherPath(Listing listing)
        {
            string file = "";
            if (!string.IsNullOrEmpty(listing.CoverImage) && System.IO.File.Exists(Server.MapPath(listing.CoverImage)))
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(Server.MapPath(listing.CoverImage));
                file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
            }
            else
            {
                string baseDirectory = Server.MapPath("~/Images/" + listing.ListingID + "/");

                if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
                {
                    file = Directory.GetFiles(baseDirectory)[0];

                    byte[] imageArray = System.IO.File.ReadAllBytes(file);
                    file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                }
            }

            return file;
        }

        private string GetListingImagePath(Listing listing)
        {
            return this.GetListingImagePath(listing.ListingID, listing.CoverImage);
        }

        private string GetListingImagePath(int listingId, string coverImage)
        {
            if (!string.IsNullOrEmpty(coverImage))
            {
                var coverImagePath = Server.MapPath(coverImage);

                if (System.IO.File.Exists(coverImagePath))
                {
                    return ".." + coverImage;// coverImage.Remove(0, 1);

                    //byte[] imageArray = System.IO.File.ReadAllBytes(coverImagePath);
                    //return "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                }
            }

            string baseDirectory = Server.MapPath("~/Images/" + listingId + "/");

            string file = string.Empty;
            if (Directory.Exists(baseDirectory) && Directory.GetFiles(baseDirectory).Length > 0)
            {
                file = Directory.GetFiles(baseDirectory)[0];

                var filename = file.Substring(file.LastIndexOf("\\") + 1, file.Length - file.LastIndexOf("\\") - 1);

                return "../Images/" + listingId + "/" + filename;

                //byte[] imageArray = System.IO.File.ReadAllBytes(file);
                //file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
            }

            return file;
        }

        private List<string> GetListingPhotos(Listing listing)
        {
            List<string> photos = new List<string>();
            var hasCoverImagePath = string.Empty;

            if (Directory.Exists(Server.MapPath("~/Images/" + listing.ListingID + "/")) && Directory.GetFiles(Server.MapPath("~/Images/" + listing.ListingID + "/")).Length > 0)
            {
                if (!string.IsNullOrEmpty(listing.CoverImage))
                {
                    var coverImagePath = Server.MapPath(listing.CoverImage);

                    if (System.IO.File.Exists(coverImagePath))
                    {
                        byte[] imageArray = System.IO.File.ReadAllBytes(coverImagePath);
                        var coverImageData = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);

                        photos.Add(coverImageData);
                        hasCoverImagePath = coverImagePath;
                    }
                }

                var files = Directory.GetFiles(Server.MapPath("~/Images/" + listing.ListingID + "/"));

                foreach (string f in files)
                {
                    if (f != hasCoverImagePath)
                    {
                        byte[] imageArray = System.IO.File.ReadAllBytes(f);
                        var file = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);

                        photos.Add(file);
                    }
                }
            }
            else
            {
                string file = "images/utf_listing_item-02.jpg";
                photos.Add(file);
            }

            return photos;
        }

        private ActionResult HandlePaypalPayment(ListingBuyNowV2 listingBuyNowV2, Models.Order order)
        {
            //getting the apiContext
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    Models.Payment pay = db.Payments.Where(p => p.UserID == listingBuyNowV2.SellerID && p.Type == 2).FirstOrDefault();

                    if (pay == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
                    string email = pay.Email;

                    //TODO: we need to save the info into a session ? se we can use a get method to paypal callback
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Listings/BuyNow?id=" + order.OrderID + "&";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePaymentV2(apiContext, baseURI + "guid=" + guid, listingBuyNowV2, email);
                    var links
                    = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while
                    (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                        System.Diagnostics.Debug.WriteLine("Failed");

                        return View("CheckoutV2", new { });
                    }

                    var updateStatus = orderService.OrderPaypalPayment(order, listingBuyNowV2.SellerID);

                    if (updateStatus == false)
                    {
                        TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                        TempData["StatusDescription"] = "The purchase of the listing was unsuccessful. Please try again";

                        return RedirectToAction("Details", new { id = order.OrderID });
                    }

                    TempData["StatusCode"] = (int)HttpStatusCode.OK;
                    TempData["StatusDescription"] = "The purchase of the listing was successful";

                    return RedirectToAction("Details", "Orders", new { id = order.OrderID });
                }
            }
            catch (PaymentsException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Data);
                System.Diagnostics.Debug.WriteLine(ex.Details);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Request);
                System.Diagnostics.Debug.WriteLine(ex.Response);
                System.Diagnostics.Debug.WriteLine(ex.StatusCode);
                System.Diagnostics.Debug.WriteLine(ex.WebExceptionStatus); System.Diagnostics.Debug.WriteLine(ex.StackTrace);

                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Purchase for this listing failed. Please try again.";

                System.Diagnostics.Debug.WriteLine("Failed");

                System.Diagnostics.Debug.WriteLine("Failed");

                return View("CheckoutV2", new { });
            }
        }

        private void LoadCreateListingAndViewBag(CreateListing createListing)
        {
            var id = userService.GetCurrentID();
            User user = db.Users.Find(id);

            var allCategories = db.Categories.Where(c => c.MarkedAsDeleted == 0);
            var mainCategories = allCategories.Where(c => c.ParentCategoryID == 0);
            var subCategories = allCategories
                .Where(c => c.ParentCategoryID != 0)
                .GroupBy(c => (int)c.ParentCategoryID)
                .ToDictionary(g => g.Key, g => g.ToArray());

            var subcategoriesJsonBuilder = new StringBuilder("{");
            foreach (var kvp in subCategories)
            {
                subcategoriesJsonBuilder.Append($"'{kvp.Key}':[");
                foreach (var category in kvp.Value)
                {
                    subcategoriesJsonBuilder.Append("{'id':'" + category.CategoryID + "', 'name':'" + category.CategoryName + "'},");
                }
                subcategoriesJsonBuilder.Append("],");
            }
            subcategoriesJsonBuilder.Append("}");

            var postages = db.Postages.Where(p => (p.markedAsDeleted.HasValue && p.markedAsDeleted.Value == 0) || p.markedAsDeleted == null);

            ViewBag.CategoryID = new SelectList(mainCategories, "CategoryID", "CategoryName");
            ViewBag.SubcategoriesJson = subcategoriesJsonBuilder.ToString();

            ViewBag.SellerD = new SelectList(db.Users, "UserID", "Name");
            ViewBag.isSubscribed = user.isSubscribed;
            ViewBag.Payments = db.Payments.Where(p => p.UserID == id).ToList();
            ViewBag.MaxPhotosAllowed = MaxPhotosAllowed;

            createListing.PaymentList = PaymentTypeProvider.GetPaymentTypes();

            if (createListing.PostageList is null || !createListing.PostageList.Any())
            {
                createListing.PostageList = new List<PostageListingCreate>();
                foreach (var postage in postages)
                {
                    createListing.PostageList.Add(new PostageListingCreate()
                    {
                        PostageID = postage.PostageID,
                        Postage = postage
                    });
                }
            }

            ViewBag.UserAvailableCredit = this.creditService.GetUserAvailableCredit();
        }

        /*public ActionResult BuyNow(int id, int amount)
        {
            Listing listing = db.Listings.Find(id);

            if (listing != null)
            {
                ListingBuyNow _list = new ListingBuyNow
                {
                    ListingID = listing.ListingID,
                    SellerID = listing.SellerD,
                    ListingQTY = amount,
                    ListingPrice = listing.Price
                };

                return RedirectToAction("Buy", "Paypal", _list);
            }

            return RedirectToAction("Details", new { id });
        }*/

        private bool UploadFiles(HttpPostedFileBase[] files, int listingID)
        {
            if (ModelState.IsValid)
            {   //iterating through multiple file collection
                foreach (HttpPostedFileBase file in files)
                {
                    //Checking file is available to save.
                    if (file != null)
                    {
                        var InputFileName = Path.GetFileName(file.FileName);
                        var photoDirectoryPath = Server.MapPath("~/Images/" + listingID + "/");
                        if (!Directory.Exists(photoDirectoryPath))
                        {
                            Directory.CreateDirectory(photoDirectoryPath);
                        }
                        var serverSavePath = photoDirectoryPath + InputFileName.GetValidFileName();
                        file.SaveAs(serverSavePath);
                    }
                }
                return true;
            }
            return false;
        }
    }
}

public class RequireRequestValueAttribute : ActionMethodSelectorAttribute
{
    public RequireRequestValueAttribute(string valueName)
    {
        ValueName = valueName;
    }

    public string ValueName { get; private set; }

    public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
    {
        return (controllerContext.HttpContext.Request[ValueName] != null);
    }
}