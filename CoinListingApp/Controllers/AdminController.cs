﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;
using PagedList;

namespace CoinListingApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Route("Admin/UserDashboard/Reports/{id}")]
        public ActionResult Activate(ReportUser report)
        {
            if (report == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The report does not exist";

                return HttpNotFound();
            }

            User user = db.Users.Find(report.SellerID);

            if (user == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The user does not exist";

                return HttpNotFound();
            }

            user.isActive = 1;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            //Send notification to account
            notificationService.SendAccountNotification(user.UserID, "Activated");

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The user was successfully activated";

            return View(report);
        }

        public ActionResult ActivateListing(int? id, string message)
        {
            if (id == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The listing does not exist";

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The listing does not exist";

                return HttpNotFound();
            }

            listing.isActive = 1;
            listing.isArchived = 0;
            listing.ActiveDays = 0;
            listing.DeactivatedDate = null;
            listing.LastUpdatedDate = DateTime.Now;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            // Send email
            notificationService.SendListingUpdateNotification(listing.SellerD, id.Value, "Activated", message);

            // Send notification
            var statusCode = (int)HttpStatusCode.OK;
            TempData["StatusCode"] = statusCode;
            TempData["StatusDescription"] = "The listing was successfully activated";

            Session["StatusCode"] = statusCode;

            return View();
        }

        [HttpPost]
        //[Route("Admin/UserDashboard/Reports/{id}")]
        public ActionResult Deactivate(ReportUser report)
        {
            if (report == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The report does not exist";

                return HttpNotFound();
            }

            User user = db.Users.Find(report.SellerID);

            if (user == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The user does not exist";

                return HttpNotFound();
            }

            user.isActive = 0;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            //Send notification to account
            notificationService.SendAccountNotification(user.UserID, "Deactivated");

            // Send notification
            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The user was successfully deactivated";

            return View(report);
        }

        public ActionResult DeactivateListing(int? id, string message)
        {
            if (id == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The listing does not exist";

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Listing listing = db.Listings.Find(id);

            if (listing == null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "The listing does not exist";

                return HttpNotFound();
            }

            listing.isActive = 0;
            listing.ActiveDays = (int)(listing.Expires - DateTime.Now).TotalDays;
            listing.DeactivatedDate = DateTime.Now;
            listing.LastUpdatedDate = DateTime.Now;

            db.Entry(listing).State = EntityState.Modified;
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully deactivated";

            // Send email
            notificationService.SendListingUpdateNotification(listing.SellerD, id.Value, "Deactivated", message);

            // Send notification
            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The listing was successfully deactivated";

            return View();// View("Listings", db.Listings.ToList());
        }

        // GET: Admin
        public ActionResult Index()
        {
            AdminIndexViewModel adminIndexViewModel = new AdminIndexViewModel();

            if (OnlineVisitorsContainer.Visitors != null)
            {
                adminIndexViewModel.OnlineUsers = OnlineVisitorsContainer.Visitors.Values.OrderByDescending(x => x.SessionStarted).Count();
                adminIndexViewModel.AuthenticatedUsers = OnlineVisitorsContainer.Visitors.Values.Where(o => o.AuthUser != null).OrderByDescending(x => x.SessionStarted).Count();
                adminIndexViewModel.RegisertedUsers = db.Users.Include(u => u.Address).Include(u => u.Address1).Include(u => u.Credit).Include(u => u.File).Include(u => u.File1).Count();
            }

            adminIndexViewModel.LiveListings = db.Listings.Where(l => l.isActive == 1 && l.isArchived == 0).Count();

            return View(adminIndexViewModel);
        }

        [Route("Admin/ListingDetails/{id}")]
        public ActionResult ListingDetails(int? id)
        {
            var listings = db.Listings.Where(x => x.ListingID == id).First();
            return View(listings);
        }

        [Route("Admin/Listings")]
        public ActionResult Listings()
        {
            var listings = db.Listings.ToList();
            return View(listings);
        }

        [Route("Admin/PricingDashboard/ManageAds/{id}")]
        public ActionResult ManageAds(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemSetting systemSetting = db.SystemSettings.Find(id);
            if (systemSetting == null)
            {
                return HttpNotFound();
            }
            return View(systemSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Admin/PricingDashboard/ManageAds/{id}")]
        public ActionResult ManageAds(SystemSetting systemSetting, byte hasFreeAds)
        {
            if (ModelState.IsValid)
            {
                db.SystemSettings.Attach(systemSetting);
                db.Entry(systemSetting).Reload();
                systemSetting.hasFreeAds = hasFreeAds;
                db.Entry(systemSetting).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(systemSetting).State = EntityState.Detached;

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system settings were successfully updated";

                return RedirectToAction("PricingDashboard");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system settings could not be updated. Please try again";

            return View(systemSetting);
        }

        [Route("Admin/PricingDashboard/ManageCreditPrice/{id}")]
        public ActionResult ManageCreditPrice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemSetting systemSetting = db.SystemSettings.Find(id);
            if (systemSetting == null)
            {
                return HttpNotFound();
            }
            return View(systemSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Admin/PricingDashboard/ManageCreditPrice/{id}")]
        public ActionResult ManageCreditPrice(SystemSetting systemSetting, double creditPrice)
        {
            if (ModelState.IsValid)
            {
                db.SystemSettings.Attach(systemSetting);
                db.Entry(systemSetting).Reload();
                systemSetting.CreditPrice = creditPrice;
                db.Entry(systemSetting).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(systemSetting).State = EntityState.Detached;

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system settings were successfully updated";

                return RedirectToAction("PricingDashboard");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system settings could not be updated. Please try again";

            return View(systemSetting);
        }

        [Route("Admin/PricingDashboard/ManageDiscount/{id}")]
        public ActionResult ManageDiscount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemSetting systemSetting = db.SystemSettings.Find(id);
            if (systemSetting == null)
            {
                return HttpNotFound();
            }
            return View(systemSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Admin/PricingDashboard/ManageDiscount/{id}")]
        public ActionResult ManageDiscount(SystemSetting systemSetting, float discount)
        {
            if (ModelState.IsValid)
            {
                db.SystemSettings.Attach(systemSetting);
                db.Entry(systemSetting).Reload();
                systemSetting.Discount = discount;
                db.Entry(systemSetting).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(systemSetting).State = EntityState.Detached;

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system settings were successfully updated";

                return RedirectToAction("PricingDashboard");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system settings could not be updated. Please try again";

            return View(systemSetting);
        }

        [Route("Admin/SystemDashboard/ManageMessageOfTheDay/{id}")]
        public ActionResult ManageMessageOfTheDay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemSetting systemSetting = db.SystemSettings.Find(id);
            if (systemSetting == null)
            {
                return HttpNotFound();
            }
            return View(systemSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Admin/SystemDashboard/ManageMessageOfTheDay/{id}")]
        public ActionResult ManageMessageOfTheDay(SystemSetting systemSetting, string MessageOfTheDay)
        {
            if (ModelState.IsValid)
            {
                db.SystemSettings.Attach(systemSetting);
                db.Entry(systemSetting).Reload();
                systemSetting.MessageOfTheDay = MessageOfTheDay;
                db.Entry(systemSetting).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(systemSetting).State = EntityState.Detached;

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system settings were successfully updated";

                return RedirectToAction("SystemDashboard");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system settings could not be updated. Please try again";

            return View(systemSetting);
        }

        [Route("Admin/UserDashboard/Reports")]
        public ActionResult ManageReports()
        {
            List<ReportUser> reports = db.ReportUsers.ToList();

            return View(reports);
        }

        // GET: Admin/PricingDashboard
        public ActionResult PricingDashboard()
        {
            return View();
        }

        [Route("Admin/UserDashboard/Reports/{id}")]
        public ActionResult ReportDetails(int id)
        {
            ReportUser report = db.ReportUsers.Find(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(report);
        }

        // GET: Admin/SystemDashboard
        public ActionResult SystemDashboard()
        {
            return View();
        }

        // GET: Admin/UserDashboard
        public ActionResult UserDashboard()
        {
            return View();
        }

        [Route("Admin/UsersTransactions")]
        public ActionResult UsersTransactions(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<OrderTransactionDetails> OrderDataList = null;
            List<OrderTransactionDetails> TransactionDetailsObj = new List<OrderTransactionDetails>();
            TransactionDetailsObj = db.Orders.Include(o => o.OrderListings)
                                   .Include("OrderListings.Listing")
                                   .Include(o => o.User)
                                   .Include(o => o.Status)
                                   .Include(o => o.Tracking)
                                   .Select(x => new OrderTransactionDetails()
                                   {
                                       OrderDate = (DateTime)x.CreatedDate,
                                       OrderNumber = x.OrderID,
                                       TotalPrice = x.Price,
                                       BuyerName = x.User.Name + " " + x.User.Surname,
                                       Item = x.OrderListings.Where(ol => ol.OrderId == x.OrderID).Select(item => new CustomItem()
                                       {
                                           ItemName = item.Listing.Title,
                                           Price = item.Price,
                                           SellerName = item.Listing.User.Name + " " + item.Listing.User.Surname,
                                           PostagePrice = item.Listing.PostageListings.Where(ps => ps.Listing.ListingID == item.Listing.ListingID && ps.PostageID == item.Order.PostageId).Select(ss => ss.Price).FirstOrDefault(),
                                           ExtraCharge = item.Listing.PostageListings.Where(ps => ps.Listing.ListingID == item.Listing.ListingID && ps.PostageID == item.Order.PostageId).Select(ss => ss.EachAdditionalItemPrice).FirstOrDefault() ?? 0,
                                       }).ToList(),
                                       Status = x.Status.State
                                   }).OrderByDescending(c => c.OrderDate).ToList();

            OrderDataList = TransactionDetailsObj.ToPagedList(pageIndex, pageSize);
            return View(OrderDataList);
        }

        [Route("Admin/NewsletterUsers/{page}")]
        public ActionResult NewsletterUsers(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<User> IPagedUserList = null;
            List<User> Users = new List<User>();
            Users = db.Users.Include(u => u.Address).Include(u => u.Address1).Include(u => u.Credit).Include(u => u.File).Include(u => u.File1).Where(x => x.IsReceiveNewsletter).ToList();
            IPagedUserList = Users.ToPagedList(pageIndex, pageSize);
            return View(IPagedUserList);
        }

        [HttpPost]
        public ActionResult ExportUsersList()
        {
            var Users = db.Users.Include(u => u.Address).Include(u => u.Address1).Include(u => u.Credit).Include(u => u.File).Include(u => u.File1).Where(x => x.IsReceiveNewsletter);
            return this.BuildCsv(Users);
        }
        private FileResult BuildCsv(IQueryable<User> Users)
        {
            StringBuilder sb = new StringBuilder();
            string headers = $"Name, Surname, Email\r\n";

            sb.Append(headers);
            foreach (var user in Users)
            {
                sb.Append(UserTransactionToString(user));
                //Append new line character.
                sb.Append("\r\n");
            }
            var dateIntervalStr = $"{DateTime.Now.ToString("yyyy-MM-dd")}";
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"credit_transactions_history_{dateIntervalStr}.csv");
        }
        private string UserTransactionToString(User user)
        {
            return user.Name
              + "," + user.Surname
              + "," + user.Email;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult RemoveConfirm(int id)
        {
            User user = db.Users.Find(id)
;
            if (user != null)
            {
                user.IsReceiveNewsletter = false;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("NewsletterUsers/1");
        }
    }
}