﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using CoinListingApp.Models;

namespace CoinListingApp.Controllers
{
    public class CreditHistoriesController : Controller
    {
        private Entities db = new Entities();

        // GET: CreditHistories/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name");
            return View();
        }

        // POST: CreditHistories/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,TransactionDetail,DateOfPurchase,Amount,Price,UserID")] CreditHistory creditHistory)
        {
            if (ModelState.IsValid)
            {
                db.CreditHistories.Add(creditHistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", creditHistory.UserID);
            return View(creditHistory);
        }

        // GET: CreditHistories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditHistory creditHistory = db.CreditHistories.Find(id);
            if (creditHistory == null)
            {
                return HttpNotFound();
            }
            return View(creditHistory);
        }

        // POST: CreditHistories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CreditHistory creditHistory = db.CreditHistories.Find(id);
            db.CreditHistories.Remove(creditHistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: CreditHistories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditHistory creditHistory = db.CreditHistories.Find(id);
            if (creditHistory == null)
            {
                return HttpNotFound();
            }
            return View(creditHistory);
        }

        // GET: CreditHistories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditHistory creditHistory = db.CreditHistories.Find(id);
            if (creditHistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", creditHistory.UserID);
            return View(creditHistory);
        }

        // POST: CreditHistories/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TransactionDetail,DateOfPurchase,Amount,Price,UserID")] CreditHistory creditHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(creditHistory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", creditHistory.UserID);
            return View(creditHistory);
        }

        [HttpPost]
        public ActionResult Export(DateTime? dateFrom, DateTime? dateTo)
        {
            if (!dateFrom.HasValue || !dateTo.HasValue || dateFrom.Value > dateTo.Value)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "Please provide a valid \"date from\" and \"date to\" values";

                return RedirectToAction("Index");
            }

            var creditHistories = db.CreditHistories.Include(c => c.User);

            var filteredCreditHistories = creditHistories.Where(ch => ch.DateOfPurchase >= dateFrom.Value && ch.DateOfPurchase <= dateTo.Value);

            return this.BuildCsv(filteredCreditHistories, dateFrom.Value, dateTo.Value);
        }

        // GET: CreditHistories
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            var creditHistories = db.CreditHistories.Include(c => c.User);
            return View(creditHistories.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private FileResult BuildCsv(IQueryable<CreditHistory> creditHistories, DateTime dateFrom, DateTime dateTo)
        {
            StringBuilder sb = new StringBuilder();
            string headers = $"Date of purchase, Transaction Detail, Amount, Price, User\r\n";

            sb.Append(headers);

            foreach (var creditTransaction in creditHistories)
            {
                sb.Append(CreditTransactionToString(creditTransaction));

                //Append new line character.
                sb.Append("\r\n");
            }

            var dateIntervalStr = $"{dateFrom.ToString("yyyy-MM-dd")}_{dateTo.ToString("yyyy-MM-dd")}";
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"credit_transactions_history_{dateIntervalStr}.csv");
        }

        private string CreditTransactionToString(CreditHistory creditTransaction)
        {
            return creditTransaction.DateOfPurchase
              + "," + creditTransaction.TransactionDetail
              + "," + creditTransaction.Amount
              + "," + creditTransaction.Price
              + "," + creditTransaction.User.Name + " " + creditTransaction.User.Surname;
        }
    }
}