﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CoinListingApp.Models;
using CoinListingApp.Services;
using CoinListingApp.Services.Validation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CoinListingApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly Entities db = new Entities();
        private readonly NotificationService notificationService = new NotificationService();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var user2 = await UserManager.FindByIdAsync(userId);
            if (user2 == null || (await UserManager.IsEmailConfirmedAsync(user2.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return View("ForgotPasswordConfirmation");
            }

            //var codeR = await UserManager.GenerateEmailConfirmationTokenAsync(user2.Id);

            var result = await UserManager.ConfirmEmailAsync(user2.Id, code);

            if (result.Succeeded)
            {
                AspNetUser user = db.AspNetUsers.Find(user2.Id);
                var email = user.Email;

                User u = db.Users.Where(r => r.Email == email).FirstOrDefault();

                u.isActive = 1;
                db.Entry(u).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                notificationService.SendWelcomeMessageNotification(u.UserID);

                return View("ConfirmEmail");
            }
            else
            {
                return View("Error");
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ConfirmEmailManually(string userEmail)
        {
            AspNetUser aspNetUser = db.AspNetUsers.FirstOrDefault(u => u.Email == userEmail);

            if (aspNetUser is null)
            {
                TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                TempData["StatusDescription"] = "AspNetUser not found.";
            }
            else
            {
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(aspNetUser.Id).ConfigureAwait(false);

                var result = await UserManager.ConfirmEmailAsync(aspNetUser.Id, code).ConfigureAwait(false);

                if (result.Succeeded)
                {
                    User user = db.Users.Where(r => r.Email == aspNetUser.Email).FirstOrDefault();

                    user.isActive = 1;
                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    notificationService.SendWelcomeMessageNotification(user.UserID);

                    TempData["StatusCode"] = (int)HttpStatusCode.OK;
                    TempData["StatusDescription"] = $"User {user.Name} {user.Surname} ({user.Email}) confirmed.";
                }
                else
                {
                    TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
                    TempData["StatusDescription"] = $"Errors confirming the user email: {string.Join(",", result.Errors)}";
                }
            }

            return RedirectToAction("Index", "Users");
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });

                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("UpdateProfile", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var aspNetUser = await UserManager.FindByNameAsync(model.Email);
                if (aspNetUser == null || !(await UserManager.IsEmailConfirmedAsync(aspNetUser.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(aspNetUser.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account",
                    new { UserId = aspNetUser.Id, code = code },
                    protocol: Request.Url.Scheme);

                notificationService.SendPasswordChangeNotification(model.Email, callbackUrl);

                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout To enable password failures
            // to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            ////REMOVE
            //AspNetUser aspNetUser = db.AspNetUsers.FirstOrDefault(u => u.Email == model.Email);

            //var result2 = UserManager.ChangePassword(aspNetUser.Id, "Admin!2", "#CSAdmin!2");
            ////REMOVE

            System.Diagnostics.Debug.WriteLine(result);

            switch (result)
            {
                case SignInStatus.Success:
                    bool isVerified = IsVerfied(model.Email);
                    if (!IsAdmin(model.Email))
                    {
                        //Refuse access for unverified accounts
                        if (isVerified == false)
                        {
                            ModelState.AddModelError("", "You need to verify your email before you can log in");
                            return View(model);
                        }

                        //Refuse access for inactive accounts
                        if (IsActive(model.Email) == false)
                        {
                            ModelState.AddModelError("", "You can't log in. Your account does not exist");
                            return View(model);
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Admin");
                    }

                    //Add isVerified session token here
                    Session.Add("isVerified", isVerified);

                    SaveLogin(model.Email);

                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }

                    return RedirectToAction("UpdateProfile", "Manage");

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            //Remove isVerified session token here
            Session.Remove("isVerified");

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            //Remove isVerified session token here
            Session.Remove("isVerified");

            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    _ = UserManager.AddToRole(user.Id, "Buyer");

                    if (ModelState.IsValid)
                    {
                        _ = db.Users.Add(new User
                        {
                            Name = model.Name,
                            Surname = model.Surname,
                            Email = model.Email,
                            isActive = 0,
                            hasFreeAds = 0,
                            isVerified = 0,
                            SignUpDate = DateTime.Today,
                            LoginCount = 1,
                            LastLoggedIn = DateTime.Now,
                            AcceptEmails = 1,
                            ShowListingsRules = 1,
                            IsReceiveNewsletter = model.CheckNewsletter
                        });
                        db.SaveChanges();
                    }

                    /*await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);*/

                    /*********************************************/
                    // UNCOMMENT TO ACTIVATE CONFIRM EMAIL
                    /*********************************************/

                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action(
                       "ConfirmEmail", "Account",
                       new { userId = user.Id, code = code },
                       protocol: Request.Url.Scheme);

                    notificationService.SendEmailVerificationNotification(model.Email, model.Name, callbackUrl);

                    return View("DisplayVerification");

                    /*********************************************/
                    // UNCOMMENT TO ACTIVATE CONFIRM EMAIL
                    /*********************************************/

                    // For more information on how to enable account confirmation and password reset
                    // please visit https://go.microsoft.com/fwlink/?LinkID=320771 Send an email
                    // with this link string code = await
                    // UserManager.GenerateEmailConfirmationTokenAsync(user.Id); var callbackUrl =
                    // Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code },
                    // protocol: Request.Url.Scheme); await UserManager.SendEmailAsync(user.Id,
                    // "Confirm your account", "Please confirm your account by clicking <a href=\""
                    // + callbackUrl + "\">here</a>");

                    //return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ValidatePassword(string currentPassword)
        {
            var errorMessage = PasswordValidationService.GetErrorMessage(currentPassword);

            return Json(errorMessage);
        }

        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. If
            // a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time. You can configure the account
            // lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        private bool IsActive(string email)
        {
            User user = db.Users.Where(u => u.Email == email).FirstOrDefault();

            if (user == null)
            {
                return false;
            }

            return user.isActive != 0;
        }

        private bool IsAdmin(string email)
        {
            AspNetUser user = db.AspNetUsers.Where(u => u.Email == email).FirstOrDefault();
            if (user != null)
            {
                return UserManager.IsInRole(user.Id, "Admin");
            }
            return false;
        }

        private bool IsVerfied(string email)
        {
            AspNetUser user = db.AspNetUsers.Where(u => u.Email == email).FirstOrDefault();

            if (user == null)
            {
                return false;
            }

            return user.EmailConfirmed;
        }

        private void SaveLogin(string email)
        {
            User user = db.Users.Where(u => u.Email == email).FirstOrDefault();

            user.LoginCount += 1;
            user.LastLoggedIn = DateTime.Now;

            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            Session.Add("isVerifiedSeller", user.isVerified == 1 ? true : false);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Helpers
    }
}