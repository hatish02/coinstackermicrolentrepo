﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CoinListingApp.Models;

namespace CoinListingApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SystemNotificationsController : Controller
    {
        private Entities db = new Entities();

        // GET: SystemNotifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SystemNotifications/Create To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Subject,Content")] SystemNotificationViewModel systemNotification)
        {
            if (ModelState.IsValid)
            {
                SystemNotification sn = new SystemNotification
                {
                    Name = systemNotification.Name,
                    Subject = systemNotification.Subject,
                    Content = systemNotification.Content
                };

                db.SystemNotifications.Add(sn);
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system notification was successfully created";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system notification was not created. Please try again";

            return View(systemNotification);
        }

        // GET: SystemNotifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemNotification systemNotification = db.SystemNotifications.Find(id);
            if (systemNotification == null)
            {
                return HttpNotFound();
            }
            return View(systemNotification);
        }

        // POST: SystemNotifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SystemNotification systemNotification = db.SystemNotifications.Find(id);
            db.SystemNotifications.Remove(systemNotification);
            db.SaveChanges();

            TempData["StatusCode"] = (int)HttpStatusCode.OK;
            TempData["StatusDescription"] = "The system notification was successfully deleted";

            return RedirectToAction("Index");
        }

        // GET: SystemNotifications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemNotification systemNotification = db.SystemNotifications.Find(id);
            if (systemNotification == null)
            {
                return HttpNotFound();
            }
            return View(systemNotification);
        }

        // GET: SystemNotifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemNotification systemNotification = db.SystemNotifications.Find(id);
            if (systemNotification == null)
            {
                return HttpNotFound();
            }

            SystemNotificationViewModel sn = new SystemNotificationViewModel
            {
                ID = systemNotification.ID,
                Name = systemNotification.Name,
                Subject = systemNotification.Subject,
                Content = systemNotification.Content
            };

            return View(sn);
        }

        // POST: SystemNotifications/Edit/5 To protect from overposting attacks, enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Subject,Content")] SystemNotificationViewModel systemNotification)
        {
            if (ModelState.IsValid)
            {
                SystemNotification sn = db.SystemNotifications.Find(systemNotification.ID);

                sn.Subject = systemNotification.Subject;
                sn.Content = systemNotification.Content;

                db.Entry(sn).State = EntityState.Modified;
                db.SaveChanges();

                TempData["StatusCode"] = (int)HttpStatusCode.OK;
                TempData["StatusDescription"] = "The system notification was successfully updated";

                return RedirectToAction("Index");
            }

            TempData["StatusCode"] = (int)HttpStatusCode.BadRequest;
            TempData["StatusDescription"] = "The system notification was not updated. Please try again";

            return View(systemNotification);
        }

        // GET: SystemNotifications
        public ActionResult Index()
        {
            if (TempData["StatusCode"] != null)
            {
                TempData["StatusCode"] = (int)TempData["StatusCode"];
                TempData["StatusDescription"] = (string)TempData["StatusDescription"];
            }

            return View(db.SystemNotifications.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}