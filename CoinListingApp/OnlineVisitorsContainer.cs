﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace CoinListingApp
{
    public static class Extensions
    {
        public static MvcHtmlString DisplayWithBreaksFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var model = html.Encode(metadata.Model).Replace("\r\n", "<br />\r\n");

            if (String.IsNullOrEmpty(model))
                return MvcHtmlString.Empty;

            return MvcHtmlString.Create(model);
        }
    }

    /// <summary>
    /// Online visitors list
    /// </summary>
    public static class OnlineVisitorsContainer
    {
        public static readonly ConcurrentDictionary<string, WebsiteVisitor> Visitors = new ConcurrentDictionary<string, WebsiteVisitor>();
    }
}