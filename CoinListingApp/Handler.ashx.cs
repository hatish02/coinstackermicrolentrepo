﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CoinListingApp
{
    /// <summary>
    /// Summary description for Handler
    /// </summary>
    public class Handler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "multipart/form-data";

            string ListingID = context.Request.Form["ListingID"].ToString();

            string dirFullPath = HttpContext.Current.Server.MapPath("~/images/" + ListingID + "/");
            string[] files;
            int numFiles;
            files = System.IO.Directory.GetFiles(dirFullPath);
            numFiles = files.Length;
            numFiles = numFiles + 1;

            string str_zip = "";

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                //  int fileSizeInBytes = file.ContentLength;
                string fileName = file.FileName;
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    string pathToSave = HttpContext.Current.Server.MapPath("~/images/" + ListingID + "/") + fileName;
                    file.SaveAs(pathToSave);

                    
                }
            }

            //context.Response.Redirect("/users/EditListing/"+ ListingID);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}